package main

import calendar.google.CalendarRole

import java.nio.file.{Path, Paths}

case class CalendarConfiguration(
    role: CalendarRole,
    credentialsFile: Path,
    prefix: Option[String]
)

object CalendarConfiguration {
  import net.ceedubs.ficus.Ficus._
  import net.ceedubs.ficus.readers.ValueReader

  implicit val pathValueReader: ValueReader[Path] =
    ValueReader[String].map(str => Paths.get(str))

  implicit val calendarConfigurationValueReader
      : ValueReader[CalendarConfiguration] = ValueReader.relative { config =>
    CalendarConfiguration(
      role = config.as[CalendarRole]("role"),
      credentialsFile = config.as[Path]("credentialsFile"),
      prefix = config.as[Option[String]]("prefix")
    )
  }
}
