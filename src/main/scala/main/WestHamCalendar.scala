package main

import badge.{BadgeService, BadgeServiceImpl}
import calendar.docker.{
  ContainerManager,
  DockerConfiguration,
  DockerContainerManager
}
import calendar.google.*
import calendar.ical.*
import calendar.ical.CalendarMediaType.*
import client.{HttpClientPageLoader, PageLoader}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging
import db.*
import main.CalendarConfiguration.*
import net.ceedubs.ficus.Ficus.*
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.server.Directives.*
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.settings.ParserSettings
import org.apache.pekko.http.scaladsl.{Http, ServerBuilder}
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.{Done, NotUsed}
import tickets.{TicketsFactory, TicketsFactoryImpl}
import uk.co.unclealex.mongodb.dao.MongoDatabaseFactory
import uk.co.unclealex.pog.WebApp.*
import uk.co.unclealex.pog.security.{
  Security,
  SecurityConfiguration,
  UserInfoValidator
}
import uk.co.unclealex.pog.{RouteProvider, WebApp}
import uk.co.unclealex.push.{BrowserPushConfiguration, BrowserPushService}
import update.*
import update.selenium.{ContainerManagingBrowser, SeleniumBrowser}
import user.ValidUsersSupplier
import web.controllers.*

import java.net.URI
import java.nio.file.Path
import java.time.{Clock, ZoneId}
import java.util.concurrent.Executors
import java.util.logging.LogManager
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}

@main
def WestHamCalendar(): Unit =

  LogManager.getLogManager.reset()

  val config = ConfigFactory.load()

  given actorSystem: ActorSystem = ActorSystem()
  given ec: ExecutionContext = actorSystem.dispatcher

  val locationUriFactory = new LocationUriFactoryImpl
  val clock = Clock.systemDefaultZone().withZone(ZoneId.of("Europe/London"))

  val (mongoClient, eventualDatabase) =
    MongoDatabaseFactory(config.getString("mongodb.url"))

  def serverCustomiser(serverBuilder: ServerBuilder): ServerBuilder =
    val parserSettings =
      ParserSettings.forServer.withCustomMediaTypes(`text/calendar`)
    serverBuilder.adaptSettings(_.withParserSettings(parserSettings))

  webApp
    .withEventualCloseables(mongoClient)
    .withServerCustomiser(serverCustomiser)
    .serve: security =>

      val calendarWriter: CalendarWriter = new IcalCalendarWriter(
        clock = clock,
        locationUriFactory = locationUriFactory
      )
      val ticketsFactory: TicketsFactory = new TicketsFactoryImpl

      val persistedGameDao: PersistedGameDao = new MongoDbPersistedGameDao(
        eventualDatabase = eventualDatabase,
        clock = clock
      )
      val pointsDao: PointsDao =
        new MongoDbPointsDao(eventualDatabase = eventualDatabase, clock = clock)
      val calendarFactory: CalendarFactory = new CalendarFactoryImpl(
        persistedGameDao = persistedGameDao,
        pointsDao = pointsDao,
        ticketsFactory = ticketsFactory,
        clock = clock
      )

      val secretPayloadValidator: SecretPayloadValidator = {
        val requiredSecretPayload: String =
          config.as[String]("secretPayload")
        (secretPayload: String) => requiredSecretPayload == secretPayload
      }

      val calendarService: CalendarService = {

        val validUsers: () => Source[String, NotUsed] =
          security.userInfoValidator match
            case UserInfoValidator.FromEmailSource(sourceEmailBuilder) =>
              sourceEmailBuilder
            case _ => () => Source.empty[String]

        val calendarIdDao: CalendarIdDao = new MongoDbCalendarIdDao(
          eventualDatabase = eventualDatabase,
          clock = clock
        )
        val calendarConfiguration =
          config.as[CalendarConfiguration]("calendars")
        val calendarNameSupplier = new CalendarNameSupplierImpl(
          calendarConfiguration.prefix
        )
        val calendarExecutionContext: ExecutionContext =
          ExecutionContext.fromExecutor(Executors.newCachedThreadPool())

        CalendarServiceImpl(
          calendarIdDao = calendarIdDao,
          credentialsPath = calendarConfiguration.credentialsFile,
          validUsersSupplier = ValidUsersSupplier(validUsers),
          clock = clock,
          role = calendarConfiguration.role,
          calendarNameSupplier = calendarNameSupplier,
          executionContext = calendarExecutionContext
        )
      }
      val fixtureUpdatesProcessor: FixtureUpdatesProcessor =
        new FixtureUpdatesProcessorImpl(persistedGameDao = persistedGameDao)
      val ticketUpdatesProcessor: TicketUpdatesProcessor =
        new TicketUpdatesProcessorImpl(persistedGameDao = persistedGameDao)

      val browserPushService: BrowserPushService = {
        val privateKey: String = config.as[String]("push.keys.private")
        val publicKey: String = config.as[String](s"push.keys.public")
        val domain: String = config.as[String](s"push.domain")
        val browserPushConfiguration = BrowserPushConfiguration(
          publicKey = publicKey,
          privateKey = privateKey,
          domain = domain
        )
        BrowserPushService(
          eventualDatabase = eventualDatabase,
          browserPushConfiguration = browserPushConfiguration,
          clock = clock
        )
      }
      val notificationService: NotificationService =
        new BrowserPushNotificationService(
          browserPushService = browserPushService,
          ticketsFactory = ticketsFactory,
          pointsDao = pointsDao
        )

      val updateGoogleCalendarsService = {
        val gameEventCreator: GameEventCreator = new GameEventCreatorImpl(
          persistedGameDao = persistedGameDao,
          calendarService = calendarService,
          clock = clock
        )
        val gameEventDateTimeUpdater = new GameEventDateTimeUpdaterImpl(
          persistedGameDao = persistedGameDao,
          calendarService = calendarService
        )
        val ticketsEventCreator: TicketsEventCreator =
          new TicketsEventCreatorImpl(
            persistedGameDao = persistedGameDao,
            calendarService = calendarService,
            clock = clock,
            ticketsFactory = ticketsFactory
          )
        new UpdateGoogleCalendarsServiceImpl(
          gameEventCreator = gameEventCreator,
          gameEventDateTimeUpdater = gameEventDateTimeUpdater,
          ticketsEventCreator = ticketsEventCreator,
          pointsDao = pointsDao
        )
      }
      val updatesProcessor: UpdatesProcessor = new UpdatesProcessorImpl(
        fixtureUpdatesProcessor = fixtureUpdatesProcessor,
        ticketUpdatesProcessor = ticketUpdatesProcessor,
        notificationService = notificationService,
        updateGoogleCalendarsService = updateGoogleCalendarsService
      )
      val manualGameService: ManualGameService = new ManualGameServiceImpl(
        persistedGameDao = persistedGameDao,
        updatesProcessor = updatesProcessor,
        calendarService = calendarService,
        clock = clock
      )

      val gameEventCreator: GameEventCreator =
        new GameEventCreatorImpl(
          persistedGameDao = persistedGameDao,
          calendarService = calendarService,
          clock = clock
        )
      val gameEventMover: GameEventMover = new GameEventMoverImpl(
        calendarService = calendarService
      )
      val ticketsEventCreator: TicketsEventCreator =
        new TicketsEventCreatorImpl(
          persistedGameDao = persistedGameDao,
          calendarService = calendarService,
          clock = clock,
          ticketsFactory = ticketsFactory
        )

      val ticketsEventUpdater: TicketsEventUpdater =
        new TicketsEventUpdaterImpl(
          ticketsEventCreator = ticketsEventCreator,
          calendarService = calendarService,
          ticketsFactory = ticketsFactory
        )
      val ticketsCalendarUpdater: TicketsCalendarUpdater =
        new TicketsCalendarUpdaterImpl(
          persistedGameDao = persistedGameDao,
          ticketsEventUpdater = ticketsEventUpdater,
          ticketsFactory = ticketsFactory,
          clock = clock
        )

      val calendarIdDao: CalendarIdDao =
        new MongoDbCalendarIdDao(
          eventualDatabase = eventualDatabase,
          clock = clock
        )

      val apiController: RouteProvider = new ApiController(
        persistedGameDao = persistedGameDao,
        pointsDao = pointsDao,
        ticketsFactory = ticketsFactory,
        ticketsCalendarUpdater = ticketsCalendarUpdater,
        gameEventMover = gameEventMover,
        manualGameService = manualGameService,
        calendarIdDao = calendarIdDao,
        security = security
      )
      val badgeController: RouteProvider = {
        val badgeService: BadgeService = new BadgeServiceImpl(
          persistedGameDao = persistedGameDao,
          badgeDirectory = config.as[Path]("badgeDirectory")
        )
        new BadgeController(
          badgeService = badgeService
        )
      }
      val calendarController: RouteProvider = new CalendarController(
        calendarFactory = calendarFactory,
        calendarWriter = calendarWriter,
        secretPayloadValidator = secretPayloadValidator
      )
      val googleCalendarController: RouteProvider = {
        val calendarSeeder: CalendarSeeder = new CalendarSeederImpl(
          persistedGameDao = persistedGameDao,
          pointsDao = pointsDao,
          calendarService = calendarService,
          gameEventCreator = gameEventCreator,
          ticketsEventCreator = ticketsEventCreator,
          ticketsFactory = ticketsFactory
        )
        new GoogleCalendarController(
          calendarSeeder = calendarSeeder,
          calendarService = calendarService
        )
      }
      val pushController: RouteProvider = new PushController(
        browserPushService = browserPushService,
        security = security
      )

      val updateController: UpdateController =
        UpdateController(
          config = config,
          clock = clock,
          updatesProcessor = updatesProcessor
        )

      val versionController: RouteProvider = new VersionController()

      import security.directives.*

      concat(
        updateController,
        googleCalendarController,
        secure:
          concat(
            apiController,
            badgeController,
            calendarController,
            pushController,
            versionController
          )
      )
