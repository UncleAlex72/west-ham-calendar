package user

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source

/** A trait used to supply all the valid users of the application.
  */
trait ValidUsersSupplier {

  def validUsers(): Source[String, NotUsed]
}

object ValidUsersSupplier {
  def apply(validUsers: () => Source[String, NotUsed]): ValidUsersSupplier = {
    () =>
      validUsers()
  }
}
