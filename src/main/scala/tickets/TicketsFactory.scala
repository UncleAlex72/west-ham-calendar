package tickets

import db.PersistedTicket

import java.time.Instant
import scala.collection.SortedSet

trait TicketsFactory {

  def saleDate(
      persistedTickets: Seq[PersistedTicket],
      points: Int
  ): Option[Instant]
}
