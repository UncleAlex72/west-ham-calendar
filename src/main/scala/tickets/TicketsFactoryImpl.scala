package tickets

import db.PersistedTicket

import java.time.Instant
import scala.collection.SortedSet

class TicketsFactoryImpl extends TicketsFactory {

  override def saleDate(
      persistedTickets: Seq[PersistedTicket],
      points: Int
  ): Option[Instant] = {
    persistedTickets.filter(_.points <= points).sorted.lastOption.map(_.at)
  }
}
