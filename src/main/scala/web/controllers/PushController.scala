package web.controllers

import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.model.{HttpEntity, StatusCodes}
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import io.circe.Codec
import io.circe.generic.semiauto.*
import uk.co.unclealex.pog.RouteProvider
import uk.co.unclealex.pog.security.{Security, UserInfo}
import uk.co.unclealex.push.{BrowserPushService, PushSubscription}

import scala.concurrent.ExecutionContext

class PushController(
    val browserPushService: BrowserPushService,
    val security: Security
)(using
    actorSystem: ActorSystem,
    ec: ExecutionContext
) extends RouteProvider
    with Routes
    with Directives
    with FailFastCirceSupport:

  import security.directives.*

  val route: Route = pathPrefix(push):
    concat(
      path("subscribe"):
        post:
          subscribe
      ,
      path("key"):
        get:
          publicKey
    )

  /** Subscribe to a push service for notifications.
    * @return
    */
  private def subscribe: Route =
    requireUser: userInfo =>
      entity(as[NewPushSubscription]): newPushSubscription =>
        val pushSubscription =
          PushSubscription(
            _id = None,
            endpoint = newPushSubscription.endpoint,
            keys = newPushSubscription.keys,
            user = PushSubscription.User(
              user = Some(userInfo.email),
              userAgent = Some(newPushSubscription.userAgent)
            ),
            lastUpdated = None,
            dateCreated = None
          )
        complete(
          status = StatusCodes.Created,
          browserPushService
            .subscribe(pushSubscription)
            .run()
            .map(_ => HttpEntity.Empty)
        )

  private def publicKey: Route =
    requireUser0:
      complete(browserPushService.publicKey())

  private case class NewPushSubscription(
      endpoint: String,
      keys: PushSubscription.Keys,
      userAgent: String
  ) derives Codec
