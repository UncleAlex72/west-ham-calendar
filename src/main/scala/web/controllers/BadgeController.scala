package web.controllers

import badge.*
import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import com.typesafe.scalalogging.StrictLogging
import io.circe.syntax.*
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.marshalling.{
  Marshal,
  Marshaller,
  Marshalling,
  ToResponseMarshaller
}
import org.apache.pekko.http.scaladsl.model.*
import org.apache.pekko.http.scaladsl.model.StatusCodes.*
import org.apache.pekko.http.scaladsl.server.directives.SecurityDirectives
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import org.apache.pekko.http.scaladsl.util.FastFuture
import uk.co.unclealex.pog.*
import uk.co.unclealex.pog.security.Security

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal

class BadgeController(
    val badgeService: BadgeService
)(implicit ac: ActorSystem, ec: ExecutionContext)
    extends StrictLogging
    with RouteProvider
    with Routes
    with Directives
    with FailFastCirceSupport
    with Sources:

  val route: Route = {
    pathPrefix(badge) {
      concat(
        path(Segment) { opponents =>
          concat(
            post {
              create(opponents)
            },
            put {
              update(opponents)
            }
          )
        },
        pathEnd {
          get {
            listMissing
          }
        }
      )
    }
  }

  implicit val badgeResponseMarshaller
      : ToResponseMarshaller[Either[BadgeResponse, Unit]] =
    Marshaller { _ => badgeResponse =>
      val (status: StatusCode, message: String) = badgeResponse match
        case Left(BadgeAlreadyExists) =>
          Unauthorized -> "A badge already exists"
        case Left(NoSuchOpponents) => NotFound -> "No such opponents"
        case Left(InvalidBadgeUrl) => BadRequest -> "Cannot parse the badge URL"
        case Left(InvalidBadgeType(mimeType)) =>
          BadRequest -> s"$mimeType is not a valid badge mime type"
        case Left(BadgeNotReadable(statusCode, statusLine)) =>
          BadGateway ->
            s"Got an invalid response for the badge $statusCode - $statusLine"
        case Right(_) => OK -> "Success"

      Marshal(message.asJson)
        .to[MessageEntity]
        .map: entity =>
          HttpResponse(status = status, entity = entity)
          Marshalling.Opaque(() =>
            HttpResponse(status = status, entity = entity)
          ) :: Nil
    }

  private def serveResponse(
      responseBuilder: (String, String) => Future[Either[BadgeResponse, Unit]],
      opponents: String
  ): Route =
    entity(as[String]): url =>
      complete(responseBuilder(opponents, url))

  def update(opponents: String): Route =
    serveResponse(badgeService.updateBadge, opponents)

  def create(opponents: String): Route =
    serveResponse(badgeService.createNewBadge, opponents)

  def listMissing: Route =
    complete(badgeService.listMissing().map(_.asJson))
