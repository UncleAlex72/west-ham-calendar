package web.controllers

import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import org.apache.pekko.http.scaladsl.server.Directives
import org.apache.pekko.http.scaladsl.server.Route
import io.circe.JsonObject
import uk.co.unclealex.pog.RouteProvider
import io.circe.syntax.*
import version.Version

class VersionController
    extends RouteProvider
    with FailFastCirceSupport
    with Directives
    with Routes {

  override val route: Route = {
    get {
      path("version.json") {
        complete(Map("version" -> Version.version).asJson)
      }
    }
  }
}
