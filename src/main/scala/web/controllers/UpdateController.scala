package web.controllers

import calendar.docker.{
  ContainerManager,
  DockerConfiguration,
  DockerContainerManager
}
import client.{HttpClientPageLoader, PageLoader}
import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import db.{Location, PersistedTicket}
import io.circe.syntax.*
import net.ceedubs.ficus.Ficus.*
import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.javadsl.unmarshalling.StringUnmarshallers
import org.apache.pekko.http.scaladsl.model.StatusCodes.BadRequest
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import org.apache.pekko.stream.scaladsl.{Flow, Framing, Source}
import org.apache.pekko.util.ByteString
import uk.co.unclealex.pog.RouteProvider
import update.*
import update.selenium.{ContainerManagingBrowser, SeleniumBrowser}

import java.net.URI
import java.nio.charset.StandardCharsets
import java.time.format.DateTimeFormatter
import java.time.{Clock, Instant, ZonedDateTime}
import java.util.concurrent.atomic.AtomicBoolean
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{DurationInt, FiniteDuration}

class UpdateController(
    allUpdatesProcessor: AllUpdatesProcessor,
    clock: Clock
)(using ac: ActorSystem, ec: ExecutionContext)
    extends RouteProvider
    with Directives
    with FailFastCirceSupport
    with StrictLogging:

  private val locked: AtomicBoolean = new AtomicBoolean(false)

  val route: Route =
    path("update"):
      post:
        parameters("season".as[Int].optional): maybeSeason =>
          if (!locked.getAndSet(true))
            logger.info("Looking for updates")
            val source: Source[String, NotUsed] =
              allUpdatesProcessor
                .update(maybeSeason)
                .via(asMessages)
                .via(withKeepAlive)
                .recover:
                  case ex =>
                    logger.error(
                      "An unexpected error occurred during updating",
                      ex
                    )
                    locked.set(false)
                    ex.getMessage
                .concatLazy(
                  Source.lazySource: () =>
                    logger.info("Finished looking for updates")
                    locked.set(false)
                    Source.empty
                )
            complete(source)
          else complete(BadRequest, "An update is already in progress".asJson)

  private def asMessages: Flow[NotableUpdate, String, NotUsed] =
    Flow[NotableUpdate].map:
      case NewGameNotableUpdate(game) =>
        val opponents = game.opponents
        val location = game.location match
          case Location.HOME => "home"
          case Location.AWAY => "away"
        val when = instantToString(game.at.get)
        s"A new $location game against $opponents will take place at $when"
      case DateChangedNotableUpdate(game, previousDatePlayed) =>
        val originalWhen = instantToString(previousDatePlayed)
        val newWhen = instantToString(game.at.get)
        s"The game against ${game.opponents} at $originalWhen will now be played at $newWhen"
      case TicketsOnSaleNotableUpdate(game) =>
        val persistedTicket: PersistedTicket = game.tickets.max
        val when = instantToString(game.at.get)
        val ticketSellingWhen = instantToString(persistedTicket.at)
        s"Tickets for the game against ${game.opponents} at $when will start selling at $ticketSellingWhen"

  private def instantToString(instant: Instant): String =
    val zonedDateTime = ZonedDateTime.ofInstant(instant, clock.getZone)
    DateTimeFormatter.RFC_1123_DATE_TIME.format(zonedDateTime)

  private def withKeepAlive: Flow[String, String, NotUsed] =
    Flow[String].keepAlive(10.seconds, () => ".")

object UpdateController:

  def apply(config: Config, clock: Clock, updatesProcessor: UpdatesProcessor)(
      using
      actorSystem: ActorSystem,
      ec: ExecutionContext
  ): UpdateController =
    val maybeBrowserDockerConfiguration: Option[Config] =
      config.as[Option[Config]]("browser.docker")

    val browserConfig: Config =
      config.as[Config]("browser")
    val webDriverUrl = browserConfig.as[String]("webDriverUrl")
    val delegate = new SeleniumBrowser(webDriverUrl)
    val maybeRestartDelay =
      for browserDockerConfiguration <- maybeBrowserDockerConfiguration
      yield browserDockerConfiguration.as[FiniteDuration]("restartDelay")
    val dockerConfiguration = config.as[DockerConfiguration]("docker")
    val containerManager: ContainerManager = new DockerContainerManager(
      dockerConfiguration = dockerConfiguration
    )
    val browser: Browser = new ContainerManagingBrowser(
      containerManager = containerManager,
      browserUrl = webDriverUrl,
      delegate = delegate
    )

    val fixtureUpdateSupplier: FixtureUpdatesSupplier =
      new BrowserFixtureUpdatesSupplier(browser = browser)
    val pageLoader: PageLoader = new HttpClientPageLoader
    val ticketUpdatesSupplier: TicketUpdatesSupplier =
      new HtmlTicketUpdatesSupplier(
        pageLoader = pageLoader,
        ticketsUri = new URI("https://www.whufc.com")
      )

    val allUpdatesProcessor: AllUpdatesProcessor = new AllUpdatesProcessorImpl(
      fixtureUpdatesSupplier = fixtureUpdateSupplier,
      ticketUpdatesSupplier = ticketUpdatesSupplier,
      updatesProcessor = updatesProcessor
    )
    new UpdateController(
      allUpdatesProcessor = allUpdatesProcessor,
      clock = clock
    )
