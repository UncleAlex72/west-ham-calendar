package web.controllers

import calendar.google.*
import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import db.*
import io.circe.*
import io.circe.syntax.*
import org.apache.commons.codec.binary.Base64
import org.apache.pekko.*
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.model.{HttpEntity, StatusCodes}
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import org.apache.pekko.stream.scaladsl.{Sink, Source}
import tickets.TicketsFactory
import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.pog.Sources.*
import uk.co.unclealex.pog.directives.*
import uk.co.unclealex.pog.security.{Security, UserInfo}
import uk.co.unclealex.pog.{PekkoStringLikeSupport, RouteProvider, Sources}
import uk.co.unclealex.stringlike.StringLikeJsonSupport
import update.{ManualGame, ManualGameService}
import web.*

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class ApiController(
    val persistedGameDao: PersistedGameDao,
    val pointsDao: PointsDao,
    val calendarIdDao: CalendarIdDao,
    val ticketsFactory: TicketsFactory,
    val ticketsCalendarUpdater: TicketsCalendarUpdater,
    val gameEventMover: GameEventMover,
    val manualGameService: ManualGameService,
    val security: Security
)(using
    val actorSystem: ActorSystem,
    val ec: ExecutionContext
) extends RouteProvider
    with Routes
    with SourceDirectives
    with Directives
    with PekkoStringLikeSupport
    with StringLikeJsonSupport
    with FailFastCirceSupport:

  import security.directives.*

  val route: Route =
    concat(
      (get & path("opponents")):
        opponents
      ,
      (get & path("seasons")):
        seasons
      ,
      (get & path("season" / IntNumber)):
        season
      ,
      (get & path("calendar" / Segment.as[CalendarType])): calendarType =>
        calendarOf(calendarType),
      path(game / Segment.as[ID]): gameId =>
        concat(
          get { getGame(gameId) },
          patch { updateAttended(gameId) },
          delete { deleteGame(gameId) },
          put { alterGame(gameId) }
        ),
      path(game):
        concat(
          get:
            parameters("opponents"): opponents =>
              findGamesByOpponents(opponents)
          ,
          post:
            newGame
        )
      ,
      path(points):
        concat(
          get:
            getPoints
          ,
          (post | put):
            updatePoints()
        )
      ,
      (get & path("venue")):
        parameters("opponents"): opponents =>
          findVenueByOpponents(opponents)
    )

  private def opponents: Route =
    complete:
      persistedGameDao.listOpponents()

  private def seasons: Route = {
    complete:
      persistedGameDao
        .listSeasons()
        .runFold(Seq.empty[Int])(_ :+ _)
        .map(Seasons(_)): Future[Seasons]
  }

  private def season(season: Int): Route = optionalUser: maybeUserInfo =>
    val monthsSource: Source[Seq[Month], NotUsed] =
      gamesSource(persistedGameDao.findBySeason(season), maybeUserInfo)
        .fold(Seq.empty[Game])(_ :+ _)
        .map(games => Month(games))
    completeSourceAsSingle:
      monthsSource.map(_.asJson)

  private def calendarOf(calendarType: CalendarType): Route = requireUser0:
    completeSourceAsSingle:
      calendarIdDao
        .findByType(calendarType)
        .map: calendarId =>
          Json.obj(
            "calendarType" -> calendarType.asJson,
            "href" -> s"https://calendar.google.com/calendar?cid=${Base64
                .encodeBase64URLSafeString(calendarId.calendarId.getBytes)}".asJson
          )

  private def gamesSource(
      persistedGameSource: Source[PersistedGame, NotUsed],
      maybeUserInfo: Option[UserInfo]
  ): Source[Game, NotUsed] =
    pointsSource(maybeUserInfo).flatMapConcat: maybePoints =>
      val gameBuilder =
        Game(ticketsFactory, maybeUserInfo.isDefined, maybePoints)
      persistedGameSource.flatMapConcat: persistedGame =>
        gameBuilder(persistedGame) match
          case Some(game) => Source.single(game)
          case None       => Source.empty

  private def pointsSource(
      maybeUserInfo: Option[UserInfo]
  ): Source[Option[Int], NotUsed] =
    maybeUserInfo match
      case Some(_) => pointsDao.points().map(Some(_))
      case None    => Source.single(None)

  private def getGame(gameId: ID): Route = optionalUser: maybeUserInfo =>
    completeSourceAsSingle:
      gamesSource(persistedGameDao.findById(gameId), maybeUserInfo)

  private def updateAttended(gameId: ID): Route =
    requireUser0:
      entity(as[Attended]): attended =>
        val gameSource: Source[Game, NotUsed] = for
          persistedGame <- persistedGameDao.updateAttended(
            gameId,
            attended.attended
          )
          _ <- Source.single(persistedGame).via(gameEventMover.flow())
          points <- pointsDao.points()
          game <- Source(
            Game(ticketsFactory, includeAttended = true, Some(points))(
              persistedGame
            ).toSeq
          )
        yield game
        completeSourceAsSingle:
          gameSource

  private def deleteGame(gameId: ID): Route =
    requireUser0:
      val eventualMaybeDone: Future[Option[Done]] =
        manualGameService.removeGame(gameId).runWith(Sink.headOption[Done])
      onSuccess(eventualMaybeDone):
        case Some(_) => complete(StatusCodes.NoContent, HttpEntity.Empty)
        case None    => reject()

  private def alterGame(gameId: ID): Route = requireUser: userInfo =>
    entity(as[ManualGame]): manualGame =>
      completeSourceAsSingle:
        gamesSource(
          manualGameService.alterGame(gameId, manualGame),
          Some(userInfo)
        )

  private def getPoints: Route =
    requireUser: userInfo =>
      completeSourceAsSingle(pointsSource(Some(userInfo)))

  private def updatePoints(): Route =
    requireUser0:
      entity(as[Int]): points =>
        completeSourceAsSingle:
          for
            _ <- pointsDao.updatePoints(points)
            _ <- ticketsCalendarUpdater.updateFutureTicketSellingDates(points)
          yield points.asJson

  private def newGame: Route =
    requireUser: userInfo =>
      entity(as[ManualGame]): manualGame =>
        completeSourceAsSingle(
          gamesSource(manualGameService.newGame(manualGame), Some(userInfo))
        )

  private def findGamesByOpponents(opponents: String): Route =
    optionalUser: maybeUserInfo =>
      complete:
        gamesSource(persistedGameDao.findByOpponents(opponents), maybeUserInfo)

  private def findVenueByOpponents(opponents: String): Route =
    val venueSearchSource = for
      persistedGame <- persistedGameDao.findByOpponents(opponents)
      if persistedGame.location == Location.AWAY
      venue <- Sources.fromOption(persistedGame.venue)
    yield venue
    completeSourceAsSingle:
      venueSearchSource.headOption.map:
        case Some(venue) => venue.asJson
        case None        => Json.Null
