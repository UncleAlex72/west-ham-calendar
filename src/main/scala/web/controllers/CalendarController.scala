package web.controllers

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import org.apache.pekko.stream.scaladsl.Sink
import calendar.ical.{
  CalendarFactory,
  CalendarMediaType,
  CalendarType,
  CalendarWriter
}
import com.typesafe.scalalogging.StrictLogging
import uk.co.unclealex.pog.{RouteProvider, Sources}

import scala.concurrent.Future

class CalendarController(
    calendarFactory: CalendarFactory,
    calendarWriter: CalendarWriter,
    secretPayloadValidator: SecretPayloadValidator
)(implicit ac: ActorSystem)
    extends RouteProvider
    with StrictLogging
    with Directives
    with Routes
    with CalendarMediaType
    with Sources {

  implicit val calendarTypeMarshaller: ToEntityMarshaller[CalendarType] = {
    implicitly[ToEntityMarshaller[Future[String]]].wrap(`text/calendar`) {
      calendarType =>
        calendarFactory
          .create(calendarType)
          .map(calendarWriter.write)
          .runWith(Sink.head[String])
    }
  }

  val route: Route = {
    get {
      pathPrefix(calendars / Segment)
        .filter(secretPayloadValidator.validate) { _ =>
          concat(
            path("attended.ics") {
              complete(CalendarType.Attended(true))
            },
            path("unattended.ics") {
              complete(CalendarType.Attended(false))
            },
            path("tickets.ics") {
              complete(CalendarType.Tickets)
            }
          )
        }
    }
  }
}
