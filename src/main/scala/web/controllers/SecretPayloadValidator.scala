package web.controllers

trait SecretPayloadValidator {

  def validate(secretPayload: String): Boolean
}
