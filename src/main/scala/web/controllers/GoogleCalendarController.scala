package web.controllers

import calendar.google.{
  CalendarSeeder,
  CalendarSeederImpl,
  CalendarService,
  GameEventCreator,
  TicketsEventCreator
}
import com.github.pjfanning.pekkohttpcirce.FailFastCirceSupport
import com.typesafe.scalalogging.StrictLogging
import db.{PersistedGameDao, PointsDao}
import io.circe.syntax.*
import org.apache.pekko.Done
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.server.{Directives, Route}
import org.apache.pekko.stream.scaladsl.{Sink, Source}
import tickets.TicketsFactory
import uk.co.unclealex.pog.RouteProvider

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

class GoogleCalendarController(
    val calendarSeeder: CalendarSeeder,
    val calendarService: CalendarService
)(using ac: ActorSystem, ec: ExecutionContext)
    extends RouteProvider
    with StrictLogging
    with Directives
    with Routes
    with FailFastCirceSupport:

  val route: Route =
    post:
      concat(
        path("seed"):
          seed
        ,
        path("invite"):
          sendInvites
      )

  private def seed: Route =
    val source = calendarSeeder
      .generate()
      .map(_ => ".")
      .merge(Source.tick(0.seconds, 10.seconds, "."), eagerComplete = true)
    complete(source)

  private def sendInvites: Route =
    parameters("user".repeated): users =>
      def userFilter(user: String): Boolean =
        users.foldLeft(false):
          case (result, filter) => result || user.contains(filter)
      complete:
        calendarService
          .sendInvites(userFilter)
          .runWith(Sink.fold(Done.done())((_, _) => Done.done()))
          .map(_ => "done".asJson)

object GoogleCalendarController:

  def apply(
      persistedGameDao: PersistedGameDao,
      pointsDao: PointsDao,
      calendarService: CalendarService,
      gameEventCreator: GameEventCreator,
      ticketsEventCreator: TicketsEventCreator,
      ticketsFactory: TicketsFactory
  )(using ac: ActorSystem, ec: ExecutionContext): GoogleCalendarController =
    val calendarSeeder: CalendarSeeder = new CalendarSeederImpl(
      persistedGameDao = persistedGameDao,
      pointsDao = pointsDao,
      calendarService = calendarService,
      gameEventCreator = gameEventCreator,
      ticketsEventCreator = ticketsEventCreator,
      ticketsFactory = ticketsFactory
    )
    new GoogleCalendarController(
      calendarSeeder = calendarSeeder,
      calendarService = calendarService
    )
