package web.controllers

trait Routes {

  val calendar = "calendar"
  val api = "api"
  val calendars = "calendars"
  val game = "game"
  val points = "points"
  val push = "push"
  val badge = "badge"
}
