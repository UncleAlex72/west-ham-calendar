package web

import db.{GameResult, PersistedGame}
import io.circe.Codec
import tickets.TicketsFactory
import io.circe.generic.semiauto._
import java.time.Instant

case class Attended(attended: Boolean)
object Attended {
  implicit val attendedCodec: Codec[Attended] = deriveCodec[Attended]
}

case class Tickets(points: Int, sellingDate: Instant)
object Tickets {
  implicit val ticketsCodec: Codec[Tickets] = deriveCodec[Tickets]
}

case class Game(
    id: String,
    location: String,
    season: Int,
    opponents: String,
    competition: String,
    at: Instant,
    day: Int,
    month: Int,
    year: Int,
    attended: Option[Boolean] = None,
    venue: Option[String] = None,
    result: Option[GameResult] = None,
    attendance: Option[Int] = None,
    tickets: Option[Tickets] = None,
    dateCreated: Option[Instant] = None,
    lastUpdated: Option[Instant] = None
)

object Game {
  def apply(
      ticketsFactory: TicketsFactory,
      includeAttended: Boolean,
      maybePoints: Option[Int]
  ): PersistedGame => Option[Game] = { persistedGame =>
    val tickets: Option[Tickets] = maybePoints.flatMap { points =>
      ticketsFactory.saleDate(persistedGame.tickets, points).map {
        sellingDate =>
          Tickets(sellingDate = sellingDate, points = points)
      }
    }
    for {
      id <- persistedGame._id
      at <- persistedGame.at
      day <- persistedGame.day
      month <- persistedGame.month
      year <- persistedGame.year
    } yield {

      Game(
        id = id.id,
        location = persistedGame.location.entryName,
        season = persistedGame.season,
        opponents = persistedGame.opponents,
        competition = persistedGame.competition.name,
        at = at,
        day = day,
        month = month,
        year = year,
        attended = if (includeAttended) Some(persistedGame.attended) else None,
        venue = persistedGame.venue,
        result = persistedGame.result,
        attendance = persistedGame.attendance,
        tickets = tickets,
        dateCreated = persistedGame.dateCreated,
        lastUpdated = persistedGame.lastUpdated
      )
    }
  }

  implicit val gameCodec: Codec[Game] = deriveCodec[Game]
}
