package web

import io.circe.Codec
import io.circe.generic.semiauto._

case class Seasons(latest: Option[Int], seasons: Seq[Int])

object Seasons {
  def apply(seasons: Seq[Int]): Seasons = {
    val sortedSeasons: Seq[Int] = seasons.sortBy(s => -s)
    Seasons(latest = sortedSeasons.headOption, seasons = sortedSeasons)
  }

  implicit val seasonsCodec: Codec[Seasons] = deriveCodec[Seasons]
}
