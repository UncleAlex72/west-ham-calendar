package web

import io.circe.generic.semiauto._
import io.circe.Codec

case class Month(
    month: Int,
    year: Int,
    gameIds: Seq[String]
)

object Month {
  def apply(games: Seq[Game]): Seq[Month] = {
    val gamesByMonth = games.groupBy { game => (game.year, game.month) }
    gamesByMonth.toSeq.sortBy(_._1).map { case ((year, month), games) =>
      Month(month = month, year = year, gameIds = games.sortBy(_.at).map(_.id))
    }
  }

  implicit val monthCodec: Codec[Month] = deriveCodec[Month]
}
