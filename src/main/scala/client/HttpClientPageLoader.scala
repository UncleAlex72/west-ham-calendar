package client

import org.apache.pekko.NotUsed
import uk.co.unclealex.pog.NotUsedMethods.*
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.{Http, HttpExt}
import org.apache.pekko.http.scaladsl.model.HttpMethods.GET
import org.apache.pekko.http.scaladsl.model.*
import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal
import org.apache.pekko.stream.scaladsl.Source
import com.typesafe.scalalogging.StrictLogging
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import scala.concurrent.ExecutionContext

/** A PageLoader that uses Http()
  */
class HttpClientPageLoader(using
    actorSystem: ActorSystem,
    ec: ExecutionContext
) extends PageLoader
    with StrictLogging:

  /** Load a page and treat it as text.
    *
    * @return
    *   The page contents as text.
    */
  private def bodyText(uri: String): Source[String, NotUsed] =
    Source
      .futureSource:
        Http()
          .singleRequest(HttpRequest(method = GET, uri = Uri(uri)))
          .map: response =>
            Source.futureSource:
              val status = response.status
              if (status.isSuccess())
                val entity = response.entity
                Unmarshal(entity).to[String].map(str => Source.single(str))
              else
                logger.warn(
                  s"Received ${status.intValue()}: ${status.reason()} from $uri"
                )
                response.discardEntityBytes().future().map(_ => Source.empty)
      .notUsed

  /** Load a page and treat it as xml.
    *
    * @return
    *   The page contents as xml.
    */
  override def bodyXml(uri: String): Source[Document, NotUsed] =
    bodyText(uri).map(body => Jsoup.parse(body))
