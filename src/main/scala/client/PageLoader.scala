package client

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import org.jsoup.nodes.Document

/** A trait to load pages from West Ham United web pages.
  */
trait PageLoader {

  /** Load a page and treat it as xml.
    *
    * @param path
    *   The path of the URL on the West Ham Page.
    * @param ec
    * @return
    *   The page contents as xml.
    */
  def bodyXml(uri: String): Source[Document, NotUsed]

}
