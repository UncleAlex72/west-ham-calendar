package badge

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.{Http, HttpExt}
import org.apache.pekko.http.scaladsl.model.HttpMethods.GET
import org.apache.pekko.http.scaladsl.model.{HttpRequest, HttpResponse, Uri}
import org.apache.pekko.stream.scaladsl.Sink
import org.apache.pekko.util.ByteString
import db.PersistedGameDao
import org.apache.batik.transcoder.image.PNGTranscoder
import org.apache.batik.transcoder.{TranscoderInput, TranscoderOutput}
import uk.co.unclealex.futures.Futures.*

import java.io.ByteArrayInputStream
import java.net.URL
import java.nio.file.{Files, Path}
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Success, Try, Using}

class BadgeServiceImpl(
    val persistedGameDao: PersistedGameDao,
    val badgeDirectory: Path
)(using actorSystem: ActorSystem, ec: ExecutionContext)
    extends BadgeService:

  override def createNewBadge(
      opponents: String,
      url: String
  ): Future[Either[BadgeResponse, Unit]] =
    downloadAndSaveBadge(opponents, url, Files.exists(_)).value

  override def updateBadge(
      opponents: String,
      url: String
  ): Future[Either[BadgeResponse, Unit]] =
    downloadAndSaveBadge(opponents, url, _ => false).value

  private def downloadAndSaveBadge(
      opponents: String,
      _url: String,
      failIfTruePredicate: Path => Boolean
  ): FutureEither[BadgeResponse, Unit] =
    for
      _ <- fe(checkOpponentsExist(opponents))
      url <- fe(validateUrl(_url))
      outputPath <- fe(generateAndValidatePath(opponents, failIfTruePredicate))
      wsResponse <- fe(downloadPicture(url))
      pictureType <- fe(extractPictureType(wsResponse))
      _ <- fe(processPicture(wsResponse, pictureType, outputPath))
    yield {}

  private def checkOpponentsExist(
      opponents: String
  ): Future[Either[BadgeResponse, Unit]] =
    persistedGameDao
      .listOpponents()
      .filter(_ == opponents)
      .runWith(Sink.headOption[String])
      .map {
        case Some(_) => Right({})
        case None    => Left(NoSuchOpponents)
      }

  private def validateUrl(_url: String): Either[BadgeResponse, URL] =
    Try(new URL(_url)) match
      case Success(url) if url.getProtocol.startsWith("http") =>
        Right(url)
      case _ => Left(InvalidBadgeUrl)

  private def generateAndValidatePath(
      opponents: String,
      failIfTruePredicate: Path => Boolean
  ): Either[BadgeResponse, Path] =
    val path: Path = badgeDirectory.resolve(s"$opponents.png")
    if (failIfTruePredicate(path))
      Left(BadgeAlreadyExists)
    else
      Right(path)

  private def downloadPicture(
      url: URL
  ): Future[Either[BadgeResponse, HttpResponse]] =
    val request = HttpRequest(method = GET, uri = Uri(url.toString))
    Http()
      .singleRequest(request)
      .map: response =>
        val status = response.status
        if (status.isSuccess())
          Right(response)
        else
          Left(BadgeNotReadable(status.intValue(), status.reason()))

  private def extractPictureType(
      response: HttpResponse
  ): Either[BadgeResponse, PictureType] =
    val contentType = response.entity.contentType.value
    PictureType.find(contentType).toRight(InvalidBadgeType(contentType))

  private def processPicture(
      response: HttpResponse,
      pictureType: PictureType,
      outputPath: Path
  ): Future[Either[BadgeResponse, Unit]] =
    response.entity.dataBytes
      .runFold(ByteString.empty)(_.concat(_))
      .map: bytes =>
        Right(pictureType.download(bytes, outputPath).get)

  enum PictureType(
      val token: String,
      _download: (ByteString, Path) => Try[Unit]
  ):

    def download(byteString: ByteString, outputPath: Path): Try[Unit] =
      _download(byteString, outputPath)

    case SVG
        extends PictureType(
          "svg",
          (byteString: ByteString, outputPath: Path) =>
            Using.Manager: use =>
              val transcoderInput = new TranscoderInput(
                use(new ByteArrayInputStream(byteString.toArray))
              )
              val transcoderOutput =
                new TranscoderOutput(use(Files.newOutputStream(outputPath)))
              val pngTranscoder = new PNGTranscoder()
              pngTranscoder.transcode(transcoderInput, transcoderOutput)
        )

    case PNG
        extends PictureType(
          "png",
          (byteString: ByteString, outputPath: Path) =>
            Using.Manager: use =>
              val in = use(new ByteArrayInputStream(byteString.toArray))
              Files.copy(in, outputPath)
        )

  private object PictureType:
    def find(mimeType: String): Option[PictureType] =
      PictureType.values.find(pt => mimeType.contains(pt.token))

  override def listMissing(): Future[Seq[String]] =
    val existingOpponentsSource =
      persistedGameDao.listOpponents().filter { opponents =>
        val path = badgeDirectory.resolve(s"$opponents.png")
        Files.notExists(path)
      }
    existingOpponentsSource.runWith(Sink.seq)
