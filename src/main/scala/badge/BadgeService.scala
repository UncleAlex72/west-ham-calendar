package badge

import scala.concurrent.Future

trait BadgeService:

  def createNewBadge(
      opponents: String,
      url: String
  ): Future[Either[BadgeResponse, Unit]]
  def updateBadge(
      opponents: String,
      url: String
  ): Future[Either[BadgeResponse, Unit]]

  def listMissing(): Future[Seq[String]]

sealed trait BadgeResponse

case object BadgeAlreadyExists extends BadgeResponse
case object NoSuchOpponents extends BadgeResponse
case object InvalidBadgeUrl extends BadgeResponse
case class BadgeNotReadable(statusCode: Int, statusLine: String)
    extends BadgeResponse
case class InvalidBadgeType(mimeType: String) extends BadgeResponse
