package retry

import cats.effect.IO

import scala.concurrent.duration.FiniteDuration

object Retry {

  sealed trait Failed
  case object Failed extends Failed

  private val NeverFail: PartialFunction[Throwable, Failed] = {
    new PartialFunction[Throwable, Failed] {
      override def isDefinedAt(x: Throwable): Boolean = false

      override def apply(v1: Throwable): Failed = Failed
    }
  }

  def retry[A](
      maxRetries: Int,
      delay: FiniteDuration,
      task: IO[A],
      failOn: PartialFunction[Throwable, Failed] = NeverFail
  ): IO[A] = {
    task.recoverWith { case ex =>
      if (maxRetries == 0 || failOn.isDefinedAt(ex)) {
        IO.raiseError(ex)
      } else {
        IO.sleep(delay) >> IO.defer(retry(maxRetries - 1, delay, task, failOn))
      }
    }
  }
}
