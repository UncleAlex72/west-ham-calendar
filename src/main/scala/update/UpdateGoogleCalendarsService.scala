package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow

trait UpdateGoogleCalendarsService {

  def updateGoogleCalendars(): Flow[NotableUpdate, NotableUpdate, NotUsed]
}
