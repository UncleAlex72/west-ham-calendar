package update

import db.PersistedGame

case class GamesAndUpdates(
    originalGame: PersistedGame,
    updatedGame: PersistedGame,
    updates: Set[GameUpdateCommand]
) {
  def appyUpdate(update: GameUpdateCommand): GamesAndUpdates = {
    update.update(updatedGame) match {
      case Some(newGame) =>
        copy(updatedGame = newGame, updates = updates + update)
      case None => this
    }
  }
}
object GamesAndUpdates {
  def apply(originalGame: PersistedGame): GamesAndUpdates =
    GamesAndUpdates(originalGame, originalGame, Set.empty)
}
