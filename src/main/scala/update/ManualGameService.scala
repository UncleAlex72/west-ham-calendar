package update

import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.stream.scaladsl.Source
import db.PersistedGame
import uk.co.unclealex.mongodb.bson.ID

trait ManualGameService {

  def removeGame(id: ID): Source[Done, NotUsed]

  def newGame(manualGame: ManualGame): Source[PersistedGame, NotUsed]

  def alterGame(
      id: ID,
      manualGame: ManualGame
  ): Source[PersistedGame, NotUsed]
}
