package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import db.GameKey

trait FixtureUpdatesSupplier {

  def fixtureUpdates(
      maybeSeason: Option[Int]
  ): Source[(GameKey, Set[GameUpdateCommand]), NotUsed]
}
