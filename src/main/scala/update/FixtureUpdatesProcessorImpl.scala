package update

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import com.typesafe.scalalogging.StrictLogging
import db.{GameKey, Location, PersistedGame, PersistedGameDao}
import update.GameUpdateCommand.DatePlayedUpdateCommand
import uk.co.unclealex.pog.Sources.*

class FixtureUpdatesProcessorImpl(
    persistedGameDao: PersistedGameDao
) extends StrictLogging
    with FixtureUpdatesProcessor:

  override def update()
      : Flow[(GameKey, Set[GameUpdateCommand]), NotableUpdate, NotUsed] =
    val gameAndUpdatesFlow
        : Flow[(GameKey, Set[GameUpdateCommand]), GameAndUpdates, NotUsed] =
      Flow[(GameKey, Set[GameUpdateCommand])].flatMapConcat {
        case (gameKey, updates) =>
          persistedGameDao
            .findByBusinessKey(gameKey)
            .headOption
            .map: maybeGame =>
              val game = maybeGame match
                case Some(game) =>
                  logger.debug(s"Found game $game for key $gameKey")
                  game
                case None =>
                  logger.debug(s"Creating a new game for key $gameKey")
                  val attended = gameKey.location == Location.HOME
                  PersistedGame.gameKey(gameKey).copy(attended = attended)
              GameAndUpdates(game, updates)

      }
    val notableUpdateFlow: Flow[GamesAndUpdates, NotableUpdate, NotUsed] =
      Flow[GamesAndUpdates].flatMapConcat {
        case GamesAndUpdates(originalGame, persistedGame, updates) =>
          val gameIsNew = originalGame._id.isEmpty
          if (gameIsNew)
            // Notify if there is a new game.
            Source.single(NewGameNotableUpdate(persistedGame))
          else
            Source:
              updates.flatMap {
                case _: DatePlayedUpdateCommand =>
                  // Notify
                  originalGame.at.map(
                    DateChangedNotableUpdate(persistedGame, _)
                  )
                case _ => None
              }
      }
    gameAndUpdatesFlow
      .via(UpdateFlows.filterUpdatesFlow())
      .via(UpdateFlows.persistingFlow(persistedGameDao))
      .via(notableUpdateFlow)
