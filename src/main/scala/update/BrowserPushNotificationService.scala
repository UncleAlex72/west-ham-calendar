package update

import _root_.tickets.TicketsFactory
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import com.typesafe.scalalogging.StrictLogging
import db.{PersistedGame, PointsDao}
import io.circe.generic.semiauto._
import io.circe.syntax._
import io.circe.{Encoder, JsonObject, Printer}
import uk.co.unclealex.push.{BrowserPushService, PushResponse}

import java.time.Instant

class BrowserPushNotificationService(
    val browserPushService: BrowserPushService,
    val ticketsFactory: TicketsFactory,
    val pointsDao: PointsDao
) extends NotificationService
    with StrictLogging {

  override def sendNotifications()
      : Flow[NotableUpdate, (NotableUpdate, String, PushResponse), NotUsed] = {
    Flow[NotableUpdate].flatMapConcat { notableUpdate =>
      val responseSource = Source.single(notableUpdate).via(parse()).via(send())
      responseSource.zipWith(Source.repeat(notableUpdate)) {
        case ((message, pushResponse), notableUpdate) =>
          (notableUpdate, message, pushResponse)
      }
    }
  }

  def parse(): Flow[NotableUpdate, Notification, NotUsed] = {
    Flow[NotableUpdate].flatMapConcat {
      case NewGameNotableUpdate(persistedGame) =>
        Source(NewGameNotification(persistedGame).toSeq)
      case DateChangedNotableUpdate(persistedGame, previousDatePlayed) =>
        Source(
          DateChangedNotification(
            persistedGame,
            previousDatePlayed
          ).toSeq
        )
      case TicketsOnSaleNotableUpdate(persistedGame) =>
        pointsDao.points().flatMapConcat { points =>
          val maybeNotification = for {
            ticketsDate <- ticketsFactory
              .saleDate(persistedGame.tickets, points)
            notification <- TicketsOnSaleNotification(
              persistedGame,
              ticketsDate
            )
          } yield {
            notification
          }
          Source(maybeNotification.toSeq)
        }
    }
  }

  def send(): Flow[Notification, (String, PushResponse), NotUsed] = {
    Flow[Notification].flatMapConcat { notification =>
      val message = notification.asJson.printWith(Printer.noSpaces)
      browserPushService.notifyAll(message).map(response => message -> response)
    }
  }
}

private object GameKeyDecomposer {
  def apply(persistedGame: PersistedGame): (String, String, String, Int) = {
    val gameKey = persistedGame.gameKey
    (
      gameKey.competition.name,
      gameKey.location.entryName,
      gameKey.opponents,
      gameKey.season
    )
  }
}

sealed trait Notification {
  val competition: String
  val location: String
  val opponents: String
  val season: Int

}

case class NewGameNotification(
    override val competition: String,
    override val location: String,
    override val opponents: String,
    override val season: Int,
    when: Instant
) extends Notification

object NewGameNotification {
  def apply(
      persistedGame: PersistedGame
  ): Option[Notification] = {
    persistedGame.at.map { at =>
      val (competition, location, opponents, season) =
        GameKeyDecomposer(persistedGame)
      NewGameNotification(
        competition = competition,
        location = location,
        opponents = opponents,
        season = season,
        when = at
      )
    }
  }
}

case class DateChangedNotification(
    override val competition: String,
    override val location: String,
    override val opponents: String,
    override val season: Int,
    previousDateTimePlayed: Instant,
    newDateTimePlayed: Instant
) extends Notification
object DateChangedNotification {
  def apply(
      persistedGame: PersistedGame,
      previousDateTimePlayed: Instant
  ): Option[Notification] = {
    persistedGame.at.map { at =>
      val (competition, location, opponents, season) =
        GameKeyDecomposer(persistedGame)
      DateChangedNotification(
        competition = competition,
        location = location,
        opponents = opponents,
        season = season,
        previousDateTimePlayed = previousDateTimePlayed,
        newDateTimePlayed = at
      )
    }
  }
}

case class TicketsOnSaleNotification(
    override val competition: String,
    override val location: String,
    override val opponents: String,
    override val season: Int,
    dateTimePlayed: Instant,
    ticketsAvailableAt: Instant
) extends Notification

object TicketsOnSaleNotification {
  def apply(
      persistedGame: PersistedGame,
      ticketsAvailableAt: Instant
  ): Option[Notification] = {
    persistedGame.at.map { at =>
      val (competition, location, opponents, season) =
        GameKeyDecomposer(persistedGame)
      TicketsOnSaleNotification(
        competition = competition,
        location = location,
        opponents = opponents,
        season = season,
        dateTimePlayed = at,
        ticketsAvailableAt = ticketsAvailableAt
      )
    }
  }
}

object Notification {

  implicit val notificationEncoder: Encoder.AsObject[Notification] = {
    (notification: Notification) =>
      {
        val (notificationType, obj): (String, JsonObject) = notification match {
          case n: NewGameNotification =>
            ("new", deriveEncoder[NewGameNotification].encodeObject(n))
          case n: TicketsOnSaleNotification =>
            (
              "tickets",
              deriveEncoder[TicketsOnSaleNotification].encodeObject(n)
            )
          case n: DateChangedNotification =>
            ("change", deriveEncoder[DateChangedNotification].encodeObject(n))
        }
        obj.+:("notificationType", notificationType.asJson)
      }
  }
}
