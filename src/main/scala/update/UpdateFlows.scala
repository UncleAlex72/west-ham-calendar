package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import com.typesafe.scalalogging.StrictLogging
import db.{PersistedGame, PersistedGameDao}

object UpdateFlows extends StrictLogging {

  def filterUpdatesFlow(): Flow[GameAndUpdates, GamesAndUpdates, NotUsed] =
    Flow[GameAndUpdates].map { case GameAndUpdates(game, updates) =>
      updates.foldLeft(GamesAndUpdates(game))(_.appyUpdate(_))
    }

  def persistingFlow(
      persistedGameDao: PersistedGameDao
  ): Flow[GamesAndUpdates, GamesAndUpdates, NotUsed] =
    Flow[GamesAndUpdates].flatMapConcat {
      case GamesAndUpdates(originalGame, updatedGame, updates) =>
        persistedGameDao
          .store(updatedGame)
          .map { persistedGame =>
            GamesAndUpdates(originalGame, persistedGame, updates)
          }
          .recover { case ex =>
            logger.error(s"Could not store game $updatedGame", ex)
            GamesAndUpdates(originalGame, updatedGame, updates)
          }
    }

}
