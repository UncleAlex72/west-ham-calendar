package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source

trait AllUpdatesProcessor {

  def update(maybeSeason: Option[Int]): Source[NotableUpdate, NotUsed]

}
