package update

import db.PersistedGame

case class GameAndUpdates(
    game: PersistedGame,
    updates: Set[GameUpdateCommand]
)
