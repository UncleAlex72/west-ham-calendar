package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import uk.co.unclealex.push.PushResponse

trait NotificationService {

  def sendNotifications()
      : Flow[NotableUpdate, (NotableUpdate, String, PushResponse), NotUsed]
}
