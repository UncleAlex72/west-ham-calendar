package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import com.typesafe.scalalogging.StrictLogging
import db.PersistedGameDao
import uk.co.unclealex.pog.Sources.*
import java.time.LocalDate

class TicketUpdatesProcessorImpl(persistedGameDao: PersistedGameDao)
    extends TicketUpdatesProcessor
    with StrictLogging:
  override def update()
      : Flow[(LocalDate, Set[GameUpdateCommand]), NotableUpdate, NotUsed] =
    val gameAndTicketsFlow =
      Flow[(LocalDate, Set[GameUpdateCommand])].flatMapConcat {
        case (gameDate, updates) =>
          persistedGameDao
            .findByDatePlayed(gameDate)
            .headOption
            .flatMapConcat {
              case Some(game) =>
                logger.info(
                  s"Found tickets for a game against ${game.opponents} on $gameDate"
                )
                Source.single(GameAndUpdates(game, updates))
              case None =>
                logger.warn(s"Could not find a game being played on $gameDate")
                Source.empty
            }
      }
    val notificationsFlow = Flow[GamesAndUpdates].flatMapConcat {
      case GamesAndUpdates(_, game, updates) =>
        if (updates.nonEmpty)
          Source.single(TicketsOnSaleNotableUpdate(game))
        else
          Source.empty
    }
    gameAndTicketsFlow
      .via(UpdateFlows.filterUpdatesFlow())
      .via(UpdateFlows.persistingFlow(persistedGameDao))
      .via(notificationsFlow)
