package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import db.GameKey

import java.time.LocalDate

trait TicketUpdatesSupplier {

  def ticketUpdates(): Source[(LocalDate, Set[GameUpdateCommand]), NotUsed]
}
