package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import cats.data.Validated.{Invalid, Valid}
import cats.effect.unsafe.implicits.global
import com.typesafe.scalalogging.StrictLogging
import db.GameKey

import scala.collection.immutable.SortedMultiDict
class BrowserFixtureUpdatesSupplier(
    val browser: Browser
) extends FixtureUpdatesSupplier
    with StrictLogging {

  override def fixtureUpdates(
      maybeSeason: Option[Int]
  ): Source[(GameKey, Set[GameUpdateCommand]), NotUsed] = {
    Source
      .lazyFuture { () =>
        {
          logger.info("Searching for games")
          browser.browse(maybeSeason).unsafeToFuture()
        }
      }
      .mapConcat { matches =>
        logger.info(s"Found ${matches.size} games")
        matches
      }
      .mapConcat { `match` =>
        `match`.toUpdates match {
          case Valid(updates) =>
            SortedMultiDict
              .newBuilder[GameKey, GameUpdateCommand]
              .addAll(updates)
              .result()
              .sets
              .toSeq
          case Invalid(messages) =>
            logger.error(
              ("Could not extract updates" :: messages.toChain.toList)
                .mkString("\n")
            )
            Seq.empty
        }
      }
  }
}
