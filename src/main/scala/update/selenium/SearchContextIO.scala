package update.selenium

import cats.effect.IO
import org.openqa.selenium.{By, SearchContext}

import scala.concurrent.duration.FiniteDuration
import scala.jdk.CollectionConverters.CollectionHasAsScala

trait SearchContextIO {

  /** Find all elements within the current context using the given mechanism.
    *
    * @param by
    *   The locating mechanism to use
    * @return
    *   A list of all {@link WebElement} s, or an empty list if nothing matches
    * @see
    *   org.openqa.selenium.By
    */
  def findElements(by: By): IO[Seq[WebElementIO]]

  /** Find the first {@link WebElement} using the given method.
    *
    * @param by
    *   The locating mechanism
    * @return
    *   The first matching element on the current context
    * @throws NoSuchElementException
    *   If no matching elements are found
    */
  def findElement(by: By): IO[WebElementIO]

}

object SearchContextIO {

  def apply(
      searchContext: SearchContext,
      defaultTimeout: FiniteDuration
  ): SearchContextIO = {
    def wrap[A](thunk: SearchContext => A): IO[A] =
      IO.interruptible(thunk(searchContext)).timeout(defaultTimeout)
    new SearchContextIO {

      override def findElements(by: By): IO[Seq[WebElementIO]] = wrap(
        _.findElements(by).asScala.toSeq.map(WebElementIO(_, defaultTimeout))
      )

      override def findElement(by: By): IO[WebElementIO] =
        wrap(_.findElement(by)).map(WebElementIO(_, defaultTimeout))
    }
  }
}
