package update.selenium

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import calendar.docker.ContainerManager
import cats.effect.{IO, Resource}
import com.typesafe.scalalogging.StrictLogging
import retry.Retry
import update.{Browser, Match}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

class ContainerManagingBrowser(
    val containerManager: ContainerManager,
    val browserUrl: String,
    val delegate: Browser
)(implicit val ec: ExecutionContext, actorSystem: ActorSystem)
    extends Browser
    with StrictLogging {

  private def waitForServices: IO[Unit] = {
    val httpRequestTask = IO.fromFuture {
      IO {
        val request: HttpRequest =
          HttpRequest(method = HttpMethods.HEAD, uri = Uri(browserUrl))
        Http().singleRequest(request).map(_ => {})
      }
    }
    Retry.retry(30, 1.second, httpRequestTask)
  }

  override def browse(maybeSeason: Option[Int]): IO[Seq[Match]] = {
    val containerResource: Resource[IO, Unit] =
      Resource.make(containerManager.start)(_ => containerManager.stop)
    containerResource.use { _ =>
      waitForServices >> delegate.browse(maybeSeason)
    }
  }
}
