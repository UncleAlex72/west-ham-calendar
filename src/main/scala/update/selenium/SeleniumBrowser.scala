package update.selenium

import cats.effect.IO
import cats.effect.std.Queue
import cats.effect.unsafe.implicits.global
import com.typesafe.scalalogging.StrictLogging
import io.circe.parser
import org.openqa.selenium.By
import org.openqa.selenium.devtools.v116.network.Network
import update.{Browser, Match, Matches}

import java.time.Month._
import java.util.Optional
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success, Try}

class SeleniumBrowser(val webDriverUrl: String)
    extends Browser
    with StrictLogging {

  private val months: Seq[Int] = Seq(
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER,
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY
  ).map(_.getValue)
  sealed trait QueueElement

  private object QueueElement {
    import update.Matches._

    case class Parsed(matches: Seq[Match]) extends QueueElement
    case object Unparseable extends QueueElement
    case object Complete extends QueueElement

    def apply(body: String): QueueElement = {
      val tryMatches: Try[Matches] = for {
        json <- parser.parse(body).toTry
        matches <- json.as[Matches].toTry
      } yield {
        matches
      }
      tryMatches match {
        case Success(matches) => Parsed(matches.`match`)
        case Failure(ex) =>
          logger.error(s"An API response could not be parsed", ex)
          Unparseable
      }

    }
  }

  private def browseWithDriver(
      driver: WebDriverIO,
      devTools: DevToolsIO,
      maybeSeason: Option[Int]
  ): IO[Seq[Match]] = {
    def seasonIsSelectable: String => Boolean = {
      maybeSeason match {
        case Some(season) =>
          val seasonAsString = season.toString
          _.startsWith(seasonAsString)
        case None => _ => true
      }
    }

    val getUrlsTask: IO[(String, Seq[String])] = for {
      _ <- driver.get("https://www.whufc.com/")
      fixturesListElement <- driver.findElement(
        new By.ByXPath("//a[@title='Fixtures & Results']")
      )
      href <- fixturesListElement.getAttribute("href")
      _ <- driver.get(href)
      baseUrl <- driver.getCurrentUrl
      seasonsSelectionElement <- driver.findElement(new By.ById("edit-season"))
      optionElements <- seasonsSelectionElement.findElements(
        new By.ByTagName("option")
      )
      seasons <- optionElements.reverse.foldLeft(IO.pure(Seq.empty[String])) {
        case (acc, webElement) =>
          for {
            seq <- acc
            season <- webElement.getAttribute("value")
          } yield {
            seq :+ season
          }
      }
    } yield {
      val fixtureUrls = for {
        season <- seasons if seasonIsSelectable(season)
        month <- months
      } yield {
        s"$baseUrl?season=$season&month=$month"
      }
      (baseUrl, fixtureUrls)
    }

    val initialiseDevToolsTask: IO[Unit] = {
      devTools.createSession >>
        devTools.send(
          Network.enable(
            Optional.of(1024 * 1024 * 64),
            Optional.empty(),
            Optional.empty()
          )
        ) >> IO.unit
    }

    def createNetworkListenerTask(
        baseUrl: String,
        queue: Queue[IO, QueueElement]
    ): IO[Unit] = {
      devTools.addListener(Network.responseReceived) { responseReceived =>
        val url = responseReceived.getResponse.getUrl
        val requestId = responseReceived.getRequestId
        IO.whenA(url.startsWith(baseUrl)) {
          IO { logger.info(s"Loaded page $url") }
        } >> IO.whenA(url.contains("performfeeds")) {
          (for {
            _ <- IO { logger.info(s"Downloading $url") }
            _ <- IO.sleep(100.milliseconds)
            response <- devTools.send(Network.getResponseBody(requestId))
            body = response.getBody
            _ <- queue.offer(QueueElement(body))
            _ <- IO { logger.info(s"Downloaded $url") }
          } yield {}).recover { case ex: Throwable =>
            logger.error(s"Could not download a response body for $url", ex)
          }
        }
      }
    }

    def loadPageTask(url: String): IO[Unit] = {
      val task =
        IO { logger.info(s"Opening page $url") } >> driver.get(url) >> IO.sleep(
          2.seconds
        )
      task.recoverWith { case ex =>
        logger.error(s"Could not open page $url", ex)
        IO.raiseError(ex)
      }
    }

    def loadPagesTask(urls: Seq[String]): IO[Unit] = {
      val task = urls.foldLeft(IO.unit) { case (acc, url) =>
        acc >> loadPageTask(url)
      }
      task.recover { case ex =>
        logger.error(s"Attempting to download all fixtures failed", ex)
      }
    }

    def drainQueueTask(
        queue: Queue[IO, QueueElement],
        buffer: Seq[Match] = Seq.empty
    ): IO[Seq[Match]] = {
      queue.tryTake.flatMap {
        case Some(QueueElement.Complete) => IO.pure(buffer)
        case Some(QueueElement.Unparseable) =>
          IO.defer(drainQueueTask(queue, buffer))
        case Some(QueueElement.Parsed(matches)) =>
          drainQueueTask(queue, buffer ++ matches)
        case _ =>
          IO.raiseError(
            new IllegalStateException("Could not drain the results queue")
          )
      }
    }

    for {
      baseUrlAndFixtureUrls <- getUrlsTask
      (baseUrl, fixtureUrls) = baseUrlAndFixtureUrls
      _ <- initialiseDevToolsTask
      queue <- Queue.bounded[IO, QueueElement](1024)
      _ <- createNetworkListenerTask(baseUrl, queue)
      _ <- loadPagesTask(fixtureUrls)
      _ <- devTools.clearListeners
      _ <- queue.offer(QueueElement.Complete)
      matches <- drainQueueTask(queue)
    } yield {
      matches
    }
  }

  def browse(maybeSeason: Option[Int]): IO[Seq[Match]] = {
    (for {
      webDriverIO <- WebDriverIO(
        webDriverUrl = webDriverUrl,
        defaultTimeout = 15.seconds
      )
      devToolsIO <- webDriverIO.getDevTools
    } yield {
      (webDriverIO, devToolsIO)
    }).use { case (webDriverIO, devToolsIO) =>
      browseWithDriver(webDriverIO, devToolsIO, maybeSeason)
    }
  }
}
