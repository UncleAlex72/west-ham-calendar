package update.selenium

import cats.effect.{IO, Resource}
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.devtools.HasDevTools
import org.openqa.selenium.remote.{Augmenter, RemoteWebDriver}
import org.openqa.selenium.{By, WebDriver}

import java.net.URL
import scala.concurrent.duration.FiniteDuration
import scala.jdk.CollectionConverters._

class WebDriverIO(driver: WebDriver, val defaultTimeout: FiniteDuration) {

  private def wrap[A](thunk: WebDriver => A): IO[A] = {
    IO.interruptible(thunk(driver)).timeout(defaultTimeout)
  }
  def get(url: String): IO[Unit] = {
    wrap(_.get(url))
  }

  def findElement(by: By): IO[WebElementIO] = {
    wrap(_.findElement(by)).map(WebElementIO(_, defaultTimeout))
  }

  def findElements(by: By): IO[Seq[WebElementIO]] = {
    wrap(_.findElements(by).asScala.toSeq.map(WebElementIO(_, defaultTimeout)))
  }

  def getCurrentUrl: IO[String] = {
    wrap(_.getCurrentUrl)
  }

  def getDevTools: Resource[IO, DevToolsIO] = {
    val acquire: IO[DevToolsIO] =
      wrap(_.asInstanceOf[HasDevTools].getDevTools).map(devTools =>
        new DevToolsIO(
          devTools = devTools,
          defaultTimeout = defaultTimeout
        )
      )
    def release(devToolsIO: DevToolsIO): IO[Unit] = devToolsIO.close
    Resource.make(acquire)(release)
  }

  def close: IO[Unit] = {
    wrap(_.close())
  }
}

object WebDriverIO {

  def apply(
      webDriverUrl: String,
      defaultTimeout: FiniteDuration
  ): Resource[IO, WebDriverIO] = {
    val acquire: IO[WebDriverIO] = IO {
      val options: ChromeOptions = new ChromeOptions
      val remoteDriver =
        new RemoteWebDriver(new URL(webDriverUrl), options)
      val driver = new Augmenter().augment(remoteDriver)
      new WebDriverIO(driver = driver, defaultTimeout = defaultTimeout)
    }
    def release(webDriverIO: WebDriverIO): IO[Unit] = webDriverIO.close
    Resource.make(acquire)(release)
  }
}
