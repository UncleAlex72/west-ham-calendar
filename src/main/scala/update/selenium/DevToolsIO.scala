package update.selenium

import cats.effect.unsafe.IORuntime
import cats.effect.{IO, Resource}
import org.openqa.selenium.devtools.{Command, DevTools, Event}

import java.util.function.Consumer
import scala.concurrent.duration.FiniteDuration

class DevToolsIO(devTools: DevTools, defaultTimeout: FiniteDuration) {

  private def wrap[A](thunk: DevTools => A): IO[A] = {
    IO.interruptible(thunk(devTools)).timeout(defaultTimeout)
  }

  def createSession: IO[Unit] = {
    wrap(_.createSession())
  }

  def send[A](command: Command[A]): IO[A] = {
    wrap(_.send(command))
  }

  def addListener[A](
      event: Event[A]
  )(block: A => IO[Unit])(implicit IORuntime: IORuntime): IO[Unit] = {
    val consumer: Consumer[A] = (a: A) => {
      block(a).unsafeRunSync()
    }
    wrap(_.addListener(event, consumer))
  }

  def clearListeners: IO[Unit] = {
    wrap(_.clearListeners())
  }

  def close: IO[Unit] = {
    wrap(_.close())
  }
}
