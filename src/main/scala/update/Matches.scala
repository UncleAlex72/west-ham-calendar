package update

import cats.data.Validated._
import cats.data._
import cats.implicits._
import db._
import io.circe._
import io.circe.generic.semiauto._
import update.GameUpdateCommand.{
  AttendanceUpdateCommand,
  DatePlayedUpdateCommand,
  ResultUpdateCommand,
  VenueUpdateCommand
}

import java.time.{LocalDate, OffsetDateTime}
import scala.util.{Failure, Success, Try}

case class Venue(longName: String, shortName: String)

object Venue {
  implicit val venueDecoder: Decoder[Venue] = deriveDecoder[Venue]

}

case class CompetitionJson(name: String, competitionCode: String)

object CompetitionJson {
  implicit val competitionDecoder: Decoder[CompetitionJson] =
    deriveDecoder[CompetitionJson]
}

case class Stage(name: String)

object Stage {
  implicit val stageDecoder: Decoder[Stage] =
    deriveDecoder[Stage]

}
case class Contestant(
    name: String,
    shortName: String,
    officialName: String,
    code: String,
    position: String
)

object Contestant {
  implicit val contestantEncoder: Encoder[Contestant] =
    deriveEncoder[Contestant]
  implicit val contestantDecoder: Decoder[Contestant] =
    deriveDecoder[Contestant]

}

case class TournamentCalendar(startDate: String) {

  def toSeason: Either[String, Int] = {
    val sd = startDate.replace("Z", "")
    Try(LocalDate.parse(sd)) match {
      case Success(localDate) =>
        Right(localDate.getYear)
      case Failure(exception) =>
        Left(
          s"Cannot parse season start date $sd due to ${exception.getMessage}"
        )
    }
  }
}

object TournamentCalendar {
  implicit val tournamentCalendarDecoder: Decoder[TournamentCalendar] =
    deriveDecoder[TournamentCalendar]
}

case class MatchInfo(
    date: String,
    time: String,
    competition: CompetitionJson,
    stage: Stage,
    tournamentCalendar: TournamentCalendar,
    contestant: Seq[Contestant],
    venue: Venue
) {

  private val WHU: String = "WHU"
  private val HOME: String = "home"
  private val AWAY: String = "away"

  def toSeason: Either[String, Int] = tournamentCalendar.toSeason

  def toCompetition(
      dateTime: OffsetDateTime
  ): ValidatedNec[String, Competition] = {
    Competition
      .findForCodeAndStage(
        code = competition.competitionCode,
        stage = stage.name
      )
      .toValidNec(
        s"${competition.competitionCode} and stage ${stage.name} is not a valid competition code at $dateTime"
      )
  }

  def toDatePlayedUpdateCommand(
      season: Int
  ): ValidatedNec[String, (GameKey, DatePlayedUpdateCommand)] = {
    val dateTime: String = date.replace('Z', 'T') + time
    Try(OffsetDateTime.parse(dateTime)) match {
      case Success(dt) =>
        val validatedCompetition: ValidatedNec[String, Competition] =
          toCompetition(dt)
        val validatedLocation: ValidatedNec[String, Location] = {
          contestant.find(_.code == WHU).map(_.position) match {
            case None       => s"Cannot find a location $dt".invalidNec
            case Some(HOME) => Location.HOME.validNec
            case Some(AWAY) => Location.AWAY.validNec
            case Some(other) =>
              s"Cannot parse position $other at £dt".invalidNec
          }
        }
        val validatedOpponents: ValidatedNec[String, String] =
          contestant
            .find(_.code != WHU)
            .map(_.name)
            .toValidNec(s"Cannot find opponents at $dt")
        (validatedCompetition, validatedLocation, validatedOpponents).mapN {
          (competition, location, opponents) =>
            val gameKey = GameKey(
              competition = competition,
              location = location,
              opponents = opponents,
              season = season
            )
            gameKey -> DatePlayedUpdateCommand(newValue = dt)
        }
      case Failure(exception) =>
        s"Cannot parse $dateTime - ${exception.getMessage}".invalidNec
    }
  }

  def toUpdates(gameKey: GameKey): Seq[(GameKey, GameUpdateCommand)] = {
    Seq(gameKey -> VenueUpdateCommand(venue.longName))
  }
}

object MatchInfo {
  implicit val matchInfoDecoder: Decoder[MatchInfo] = deriveDecoder[MatchInfo]

}
case class ScoreJson(home: Int, away: Int) {
  def toScore(): Score = Score(home = home, away = away)
}

object ScoreJson {
  implicit val scoreDecoder: Decoder[ScoreJson] = deriveDecoder[ScoreJson]

}
case class Scores(
    ht: Option[ScoreJson],
    ft: Option[ScoreJson],
    pen: Option[ScoreJson],
    total: Option[ScoreJson]
)
object Scores {
  implicit val scoresDecoder: Decoder[Scores] = deriveDecoder[Scores]

}

case class MatchDetails(scores: Option[Scores])

object MatchDetails {
  implicit val matchDetailsDecoder: Decoder[MatchDetails] =
    deriveDecoder[MatchDetails]

}
case class MatchDetailsExtra(attendance: Option[String])

object MatchDetailsExtra {
  implicit val matchDetailsExtraDecoder: Decoder[MatchDetailsExtra] =
    deriveDecoder[MatchDetailsExtra]

}

case class LiveData(
    matchDetails: Option[MatchDetails],
    matchDetailsExtra: Option[MatchDetailsExtra]
) {
  def toUpdates(gameKey: GameKey): Seq[(GameKey, GameUpdateCommand)] = {
    val maybeResultUpdateCommand = for {
      details <- matchDetails
      scores <- details.scores
      ft <- scores.ft
    } yield {
      val gameResult: GameResult = GameResult(
        score = ft.toScore(),
        shootoutScore = scores.pen.map(_.toScore())
      )
      gameKey -> ResultUpdateCommand(newValue = gameResult)
    }
    val maybeAttendanceUpdateCommand = for {
      extra <- matchDetailsExtra
      attendanceStr <- extra.attendance
      attendance <- Try(Integer.parseInt(attendanceStr)).toOption
    } yield {
      gameKey -> AttendanceUpdateCommand(attendance)
    }
    maybeResultUpdateCommand.toSeq ++ maybeAttendanceUpdateCommand
  }
}

object LiveData {
  implicit val liveDataDecoder: Decoder[LiveData] = deriveDecoder[LiveData]

}

case class Match(matchInfo: MatchInfo, liveData: Option[LiveData]) {
  def toSeason: Either[String, Int] = matchInfo.toSeason
  def toUpdates: ValidatedNec[String, Seq[(GameKey, GameUpdateCommand)]] = {
    toSeason match {
      case Right(season) =>
        matchInfo.toDatePlayedUpdateCommand(season).map {
          case (gameKey, datePlayedUpdateCommand) =>
            Seq(gameKey -> datePlayedUpdateCommand) ++
              matchInfo.toUpdates(gameKey) ++
              liveData.map(_.toUpdates(gameKey)).getOrElse(Seq.empty)
        }
      case Left(err) => err.invalidNec
    }
  }
}

object Match {
  implicit val matchDecoder: Decoder[Match] = deriveDecoder[Match]
}

case class Matches(`match`: Seq[Match]) {
  def ++(matches: Matches): Matches =
    Matches(`match` = this.`match` ++ matches.`match`)
}

object Matches {
  def empty: Matches = Matches(Seq.empty)

  implicit val matchesDecoder: Decoder[Matches] = {
    case class Matches_(`match`: Option[Seq[Match]])
    deriveDecoder[Matches_].map(m => Matches(m.`match`.toSeq.flatten))
  }
}
