package update

import cats.effect.IO

trait Browser {

  def browse(maybeSeason: Option[Int]): IO[Seq[Match]]
}
