package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import com.typesafe.scalalogging.StrictLogging
import db.{GameKey, PersistedGame}

import java.time.LocalDate

class UpdatesProcessorImpl(
    val fixtureUpdatesProcessor: FixtureUpdatesProcessor,
    val ticketUpdatesProcessor: TicketUpdatesProcessor,
    val notificationService: NotificationService,
    val updateGoogleCalendarsService: UpdateGoogleCalendarsService
) extends UpdatesProcessor
    with StrictLogging {

  override def update(
      fixtureUpdatesSource: Source[(GameKey, Set[GameUpdateCommand]), NotUsed],
      ticketUpdatesSourceBuilder: () => Source[
        (LocalDate, Set[GameUpdateCommand]),
        NotUsed
      ]
  ): Source[NotableUpdate, NotUsed] = {
    case class State(
        previous: Option[NotableUpdate] = None,
        current: Option[NotableUpdate] = None
    ) {
      def next(update: NotableUpdate): State = {
        val newCurrent: Option[NotableUpdate] =
          if (previous.contains(update)) None else Some(update)
        State(Some(update), newCurrent)
      }
    }
    fixtureUpdatesSource
      .via(fixtureUpdatesProcessor.update())
      .concatLazy(
        Source
          .lazySource(ticketUpdatesSourceBuilder)
          .via(ticketUpdatesProcessor.update())
      )
      .log("before-gcal")
      .via(updateGoogleCalendarsService.updateGoogleCalendars())
      .log("after-gcal")
      .via(notificationService.sendNotifications())
      .map(_._1)
      .fold(State())(_.next(_))
      .map(_.current)
      .flatMapConcat(maybeUpdate => Source(maybeUpdate.toSeq))
  }
}
