package update
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.{Done, NotUsed}
import calendar.google.{CalendarService, CalendarType}
import db.Competition.FRIENDLY
import db.{GameKey, Location, PersistedGame, PersistedGameDao}
import uk.co.unclealex.mongodb.bson.ID
import update.GameUpdateCommand.{
  AttendedUpdateCommand,
  DatePlayedUpdateCommand,
  TicketsUpdateCommand,
  VenueUpdateCommand
}
import uk.co.unclealex.pog.Sources.lastOption
import java.time.*
import scala.math.Ordered.orderingToOrdered

class ManualGameServiceImpl(
    val persistedGameDao: PersistedGameDao,
    val updatesProcessor: UpdatesProcessor,
    val calendarService: CalendarService,
    val clock: Clock
) extends ManualGameService {

  val zoneId: ZoneId = clock.getZone

  override def newGame(
      manualGame: ManualGame
  ): Source[PersistedGame, NotUsed] = {
    val (gameKey, at, datePlayed, venue) = decomposeManualGame(manualGame)
    val gameUpdateCommands: Set[GameUpdateCommand] = Set(
      DatePlayedUpdateCommand(at),
      VenueUpdateCommand(venue),
      AttendedUpdateCommand(manualGame.attended)
    )
    val fixtureUpdateCommands: Seq[(GameKey, Set[GameUpdateCommand])] = Seq(
      gameKey -> gameUpdateCommands
    )
    val ticketUpdateCommands: Option[(LocalDate, Set[GameUpdateCommand])] = {
      manualGame.tickets.map(ticketSellingDate =>
        datePlayed -> Set(
          TicketsUpdateCommand(
            0 -> ticketSellingDate.atZone(zoneId).toOffsetDateTime
          )
        )
      )
    }
    updatesProcessor
      .update(
        Source(fixtureUpdateCommands),
        () => Source(ticketUpdateCommands.toSeq)
      )
      .lastOption
      .flatMapConcat { _ =>
        persistedGameDao.findByBusinessKey(gameKey)
      }
  }

  private def decomposeManualGame(
      manualGame: ManualGame
  ): (GameKey, OffsetDateTime, LocalDate, String) = {
    val at = manualGame.at.atZone(zoneId)
    val datePlayed = at.toLocalDate
    val year = datePlayed.getYear
    val season = if (datePlayed.getMonth >= Month.JUNE) year else year + 1
    val (location, venue) = manualGame.venue match {
      case Some(venue) => (Location.AWAY, venue)
      case None        => (Location.HOME, "London Stadium")
    }
    val gameKey: GameKey = GameKey(
      competition = FRIENDLY,
      location = location,
      opponents = manualGame.opponents,
      season = season
    )
    (gameKey, at.toOffsetDateTime, datePlayed, venue)
  }

  override def alterGame(
      id: ID,
      manualGame: ManualGame
  ): Source[PersistedGame, NotUsed] = {
    removeGame(id).flatMapConcat { _ =>
      newGame(manualGame)
    }
  }

  override def removeGame(id: ID): Source[Done, NotUsed] = {
    def removeCalendarEvent(
        calendarType: CalendarType,
        maybeId: Option[String]
    ): Source[Done, NotUsed] = {
      for {
        id <- Source(maybeId.toSeq)
        _ <- calendarService.remove(calendarType, id)
      } yield {
        Done.done()
      }
    }

    val deletedPersistedGameSource: Source[PersistedGame, NotUsed] = for {
      persistedGame <- persistedGameDao.findById(id)
      if persistedGame.competition == FRIENDLY
      id <- Source(persistedGame._id.toSeq)
      _ <- persistedGameDao.delete(id)
    } yield {
      persistedGame
    }
    deletedPersistedGameSource.flatMapConcat { persistedGame =>
      val calendarType: CalendarType = if (persistedGame.attended) {
        CalendarType.ATTENDED
      } else {
        CalendarType.NON_ATTENDED
      }
      removeCalendarEvent(calendarType, persistedGame.gameCalendarEventId)
        .concat(
          removeCalendarEvent(
            CalendarType.TICKETS,
            persistedGame.ticketsCalendarEventId
          )
        )
    }
  }
}
