package update

import cats.Order
import db.PersistedGame

import java.time.Instant

sealed trait NotableUpdate {

  type N <: NotableUpdate

  val game: PersistedGame

  def updateGame(newGame: PersistedGame): N
}

case class NewGameNotableUpdate(override val game: PersistedGame)
    extends NotableUpdate {

  type N = NewGameNotableUpdate

  override def updateGame(newGame: PersistedGame): N = {
    this.copy(game = newGame)
  }
}

case class DateChangedNotableUpdate(
    override val game: PersistedGame,
    previousDatePlayed: Instant
) extends NotableUpdate {

  type N = DateChangedNotableUpdate

  override def updateGame(newGame: PersistedGame): N = {
    this.copy(game = newGame)
  }
}

case class TicketsOnSaleNotableUpdate(override val game: PersistedGame)
    extends NotableUpdate {

  type N = TicketsOnSaleNotableUpdate

  override def updateGame(newGame: PersistedGame): N = {
    this.copy(game = newGame)
  }
}

object NotableUpdate {

  implicit val notableUpdateOrdering: Ordering[NotableUpdate] = Ordering.by {
    notableUpdate =>
      val typeOrdering: Int = notableUpdate match {
        case NewGameNotableUpdate(_)        => 1
        case DateChangedNotableUpdate(_, _) => 2
        case TicketsOnSaleNotableUpdate(_)  => 3
      }
      (notableUpdate.game.at, typeOrdering)
  }

  implicit val notableUpdateOrder: Order[NotableUpdate] =
    (x: NotableUpdate, y: NotableUpdate) => {
      notableUpdateOrdering.compare(x, y)
    }

}
