package update
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import db.{GameKey, PersistedGame}
import update.GameUpdateCommand.DatePlayedUpdateCommand

trait FixtureUpdatesProcessor:

  def update(): Flow[(GameKey, Set[GameUpdateCommand]), NotableUpdate, NotUsed]
