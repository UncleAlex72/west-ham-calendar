/** Copyright 2010-2012 Alex Jones
  *
  * Licensed to the Apache Software Foundation (ASF) under one or more
  * contributor license agreements. See the NOTICE file distributed with work
  * for additional information regarding copyright ownership. The ASF licenses
  * file to you under the Apache License, Version 2.0 (the "License"); you may
  * not use file except in compliance with the License. You may obtain a copy of
  * the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations
  * under the License.
  */
package update

import java.time.{OffsetDateTime}
import com.typesafe.scalalogging.StrictLogging
import db.{GameKey, GameResult, PersistedGame, PersistedTicket}
import enumeratum.{EnumEntry, _}

sealed trait GameUpdateCommand {

  val name: String = this.getClass.getName

  // For testing only
  val value: Any

  /** Update a game. No check is made to see if the correct game is being
    * updated.
    *
    * @param game
    *   The game to update.
    * @return
    *   The updated game if it was updated or none otherwise.
    */
  def update(game: PersistedGame): Option[PersistedGame]
}

sealed abstract class UpdateType(val description: String) extends EnumEntry
object UpdateType extends Enum[UpdateType] {

  val values: IndexedSeq[UpdateType] = findValues

  // The type for date played updates.
  case object DATE_PLAYED extends UpdateType("date played")
  // The type for result updates.
  case object RESULT extends UpdateType("result")
  // The type for attendence updates.
  case object ATTENDENCE extends UpdateType("attendence")
  // The type for venue updates.
  case object VENUE extends UpdateType("venue")
  // The type for attended updates
  case object ATTENDED extends UpdateType("attended")
  // The type for changing the Bondholder ticket selling dates.
  case object PRIORITY_POINT_TICKETS
      extends UpdateType("priority ticket selling date")
  // The type for changing the season ticket holder ticket selling dates.

  implicit val ordering: Ordering[UpdateType] = Ordering.by(values.indexOf)
}

object GameUpdateCommand {
  import update.UpdateType._

  sealed abstract class BaseGameUpdateCommand[V](
      /** The type of update (used for ordering).
        */
      val updateType: UpdateType,
      /** The new date played value.
        */
      val newValue: V
  ) extends GameUpdateCommand
      with StrictLogging {

    override val value: V = newValue

    override def update(game: PersistedGame): Option[PersistedGame] = {
      val updatedGame: PersistedGame = setNewValue(game)
      if (game == updatedGame) {
        None
      } else {
        logger info s"Updating the ${updateType.description} to $newValue for game ${game.gameKey}"
        Some(updatedGame)
      }
    }

    /** Alter a game.
      *
      * @param game
      *   The game to alter.
      */
    protected def setNewValue(game: PersistedGame): PersistedGame
  }

  /** A {@link GameUpdateCommand} that updates a game's date played value.
    *
    * @param newValue
    *   The new date played value.
    * @return
    *   A {@link GameUpdateCommand} that updates a game's date played value.
    */
  case class DatePlayedUpdateCommand(override val newValue: OffsetDateTime)
      extends BaseGameUpdateCommand[OffsetDateTime](DATE_PLAYED, newValue) {

    override def setNewValue(game: PersistedGame): PersistedGame = {
      game.copy(
        at = Some(newValue.toInstant),
        day = Some(newValue.getDayOfMonth),
        month = Some(newValue.getMonthValue),
        year = Some(newValue.getYear)
      )
    }

    override def equals(obj: Any): Boolean = obj match
      case DatePlayedUpdateCommand(otherValue) =>
        newValue.toInstant == otherValue.toInstant
      case _ => false
  }

  /** Create a {@link GameUpdateCommand} that updates a game's result value.
    *
    * @param gameKey
    *   The locator to use to locate the game.
    * @param newValue
    *   The new result.
    * @return
    *   A {@link GameUpdateCommand} that updates a game's result value.
    */
  case class ResultUpdateCommand(override val newValue: GameResult)
      extends BaseGameUpdateCommand[GameResult](RESULT, newValue) {

    override def setNewValue(game: PersistedGame): PersistedGame =
      game.copy(result = Some(newValue))
  }

  /** Create a {@link GameUpdateCommand} that updates a game's attendance value.
    *
    * @param gameKey
    *   The locator to use to locate the game.
    * @param newValue
    *   The new attendance.
    * @return
    *   A {@link GameUpdateCommand} that updates a game's attendance value.
    */
  case class AttendanceUpdateCommand(override val newValue: Int)
      extends BaseGameUpdateCommand[Int](ATTENDENCE, newValue) {

    override def setNewValue(game: PersistedGame): PersistedGame =
      game.copy(attendance = Some(newValue))

  }

  /** Create a {@link GameUpdateCommand} that updates a game's attended value.
    *
    * @param gameKey
    *   The locator to use to locate the game.
    * @param newValue
    *   The new attended value.
    * @return
    *   A {@link GameUpdateCommand} that updates a game's attended value.
    */
  case class AttendedUpdateCommand(override val newValue: Boolean)
      extends BaseGameUpdateCommand[Boolean](ATTENDED, newValue) {

    override def setNewValue(game: PersistedGame): PersistedGame =
      game.copy(attended = newValue)

  }

  /** Create a {@link GameUpdateCommand} that updates a game's venue.
    *
    * @param gameKey
    *   The locator to use to locate the game.
    * @param newValue
    *   The new venue.
    * @return
    *   A {@link GameUpdateCommand} that updates a game's venue.
    */
  case class VenueUpdateCommand(override val newValue: String)
      extends BaseGameUpdateCommand[String](VENUE, newValue) {

    override def setNewValue(game: PersistedGame): PersistedGame =
      game.copy(venue = Some(newValue))

  }

  /** Create a {@link GameUpdateCommand} that updates a game's tickets
    * availability date.
    *
    * @param gameKey
    *   The locator to use to locate the game.
    * @param newValue
    *   The new date and points needed for ticket availabilty.
    * @return
    *   A {@link GameUpdateCommand} that updates a game's bondholder
    *   availability tickets date.
    */
  case class TicketsUpdateCommand(override val newValue: (Int, OffsetDateTime))
      extends BaseGameUpdateCommand[(Int, OffsetDateTime)](
        PRIORITY_POINT_TICKETS,
        newValue
      ) {

    override def setNewValue(game: PersistedGame): PersistedGame = {
      val (newPoints, newSellingDate) = newValue
      val index: Int =
        game.tickets.indexWhere(tickets => tickets.points == newPoints)
      val persistedTicket = PersistedTicket(newPoints, newSellingDate.toInstant)
      val tickets: Seq[PersistedTicket] = if (index < 0) {
        (game.tickets :+ persistedTicket).sorted
      } else {
        game.tickets.updated(index, persistedTicket)
      }
      game.copy(tickets = tickets)
    }
  }
}
