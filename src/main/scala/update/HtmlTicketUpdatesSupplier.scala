package update
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import client.PageLoader
import com.typesafe.scalalogging.StrictLogging
import org.jsoup.nodes.{Document, Node, TextNode}
import update.GameUpdateCommand.TicketsUpdateCommand

import java.net.URI
import java.time.format.{DateTimeFormatter, DateTimeFormatterBuilder}
import java.time.{LocalDate, LocalDateTime, ZoneId, ZonedDateTime}
import scala.annotation.tailrec
import scala.jdk.CollectionConverters._
import scala.util.Try

class HtmlTicketUpdatesSupplier(
    val pageLoader: PageLoader,
    val ticketsUri: URI
) extends TicketUpdatesSupplier
    with StrictLogging {

  override def ticketUpdates()
      : Source[(LocalDate, Set[GameUpdateCommand]), NotUsed] = {
    Source.lazySource { () =>
      {
        logger.info("Searching for tickets")
        loadPages().via(findTicketsUpdates()).via(processTicketsUpdates())
      }
    }
  }.mapMaterializedValue(_ => NotUsed)

  def loadPages(): Source[Document, NotUsed] = {
    val ticketsPage = ticketsUri.resolve("/tickets/away-matches")
    val pageLoadFlow: Flow[String, Document, NotUsed] =
      Flow[String].flatMapConcat { page =>
        logger.info(s"Loading page $page")
        pageLoader.bodyXml(page)
      }
    val childLinksExtractor: Flow[Document, String, NotUsed] =
      Flow[Document].flatMapConcat { body =>
        val links = for {
          a <- body.select("a").asScala if a.text().trim == "INFO"
        } yield {
          ticketsUri.resolve(a.attributes().get("href")).toString
        }
        Source(links.toSeq)
      }
    Source
      .single {
        logger.info(s"Looking for tickets at $ticketsPage")
        ticketsPage.toString
      }
      .via(pageLoadFlow)
      .via(childLinksExtractor.log("tickets.html-ticket-links"))
      .via(pageLoadFlow)

  }
  def findTicketsUpdates()
      : Flow[Document, (LocalDate, Map[Int, ZonedDateTime]), NotUsed] = {
    Flow[Document]
      .flatMapConcat { body =>
        val zoneId = ZoneId.of("Europe/London")
        // Sunday 24 April 2022, 2pm BST
        val gameDateFormatter: DateTimeFormatter =
          DateTimeFormatter.ofPattern("EEEE d MMMM yyyy")
        // 11am, Tuesday 29 March
        val ticketDateTimeFormatter: DateTimeFormatter =
          new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern("ha, EEEE d MMMM yyyy")
            .toFormatter

        val gameDateParser: String => Try[LocalDate] = { str =>
          Try(LocalDate.parse(str, gameDateFormatter))
        }
        val ticketTimeParser: Int => String => Try[LocalDateTime] = {
          year => str =>
            Try(LocalDateTime.parse(s"$str $year", ticketDateTimeFormatter))
              .recoverWith { case _ =>
                Try(
                  LocalDateTime
                    .parse(s"$str ${year - 1}", ticketDateTimeFormatter)
                )
              }
        }
        val pointsRegex =
          raw".*Season Ticket Holders with (\d+)(?:(?:\s*-\s*|\s*to\s*)\d+)?\+? Points.*".r
        case class State(
            maybeGame: Option[LocalDate] = None,
            currentPoints: Option[Int] = None,
            ticketDates: Map[Int, ZonedDateTime] = Map.empty
        ) {
          def next(line: String): State = {
            maybeGame match {
              case Some(game) =>
                (line, currentPoints) match {
                  case (pointsRegex(points), _) =>
                    logger.info(s"Found points $points")
                    copy(currentPoints = Some(points.toInt))
                  case (_, Some(points)) =>
                    logger.debug(s"Parsing for selling dates in [$line]")
                    val year = game.getYear
                    Searcher(ticketTimeParser(year))(line) match {
                      case Some(localDateTime) =>
                        val ticketSellingDateTime =
                          ZonedDateTime.of(localDateTime, zoneId)
                        logger.info(
                          s"Found tickets selling at $ticketSellingDateTime"
                        )
                        copy(
                          currentPoints = None,
                          ticketDates =
                            ticketDates + (points -> ticketSellingDateTime)
                        )
                      case _ => this
                    }
                  case _ => this
                }
              case None =>
                Searcher(gameDateParser)(line) match {
                  case Some(game) =>
                    logger.info(s"Found date $game")
                    copy(maybeGame = Some(game))
                  case _ => this
                }
            }
          }
        }
        val texts = allTexts(body)
        val state = texts.foldLeft(State())(_.next(_))
        val maybeResult = state.maybeGame.map(game => game -> state.ticketDates)
        Source(maybeResult.toSeq)
      }
      .log("tickets.html-ticket-dates")
  }

  def allTexts(document: Document): Seq[String] = {
    @tailrec
    def inner_texts(stack: List[Node], texts: Seq[String]): Seq[String] =
      stack match {
        case Nil => texts
        case (text: TextNode) :: tail =>
          inner_texts(
            tail,
            texts ++ Option(text.text.replace('\u00A0', ' ').trim)
              .filter(_.nonEmpty)
          )
        case node :: ls =>
          inner_texts(node.childNodes().asScala.toList ++ ls, texts)
      }

    inner_texts(List(document), Seq.empty)
  }

  object Searcher {
    def apply[E](parser: String => Try[E]): String => Option[E] = { str =>
      val values: LazyList[E] = for {
        start <- (0 to str.length).to(LazyList)
        end <- (start to str.length).to(LazyList)
        substr = str.substring(start, end)
        value <- parser(substr).toOption.to(LazyList)
      } yield {
        value
      }
      values.headOption

    }

  }

  def processTicketsUpdates(): Flow[
    (LocalDate, Map[Int, ZonedDateTime]),
    (LocalDate, Set[GameUpdateCommand]),
    NotUsed
  ] = {
    Flow[(LocalDate, Map[Int, ZonedDateTime])].map { case (gameDate, tickets) =>
      val updates: Set[GameUpdateCommand] = tickets.map {
        case (points, sellingAt) =>
          TicketsUpdateCommand(points -> sellingAt.toOffsetDateTime)
      }.toSet
      (gameDate, updates)
    }
  }
}
