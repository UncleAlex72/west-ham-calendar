package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow

import java.time.LocalDate

trait TicketUpdatesProcessor {
  def update()
      : Flow[(LocalDate, Set[GameUpdateCommand]), NotableUpdate, NotUsed]
}
