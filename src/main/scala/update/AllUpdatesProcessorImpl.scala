package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import com.typesafe.scalalogging.StrictLogging
import db.GameKey

import java.time.LocalDate

class AllUpdatesProcessorImpl(
    val fixtureUpdatesSupplier: FixtureUpdatesSupplier,
    val ticketUpdatesSupplier: TicketUpdatesSupplier,
    val updatesProcessor: UpdatesProcessor
) extends AllUpdatesProcessor
    with StrictLogging {

  override def update(
      maybeSeason: Option[Int]
  ): Source[NotableUpdate, NotUsed] = {
    updatesProcessor.update(fixtures(maybeSeason), tickets)
  }

  private def tickets()
      : Source[(LocalDate, Set[GameUpdateCommand]), NotUsed] = {
    ticketUpdatesSupplier
      .ticketUpdates()
      .recoverWithRetries(
        0,
        { case ex: Exception =>
          logger.error("An error occurred downloading ticket data", ex)
          Source.empty
        }
      )
  }

  private def fixtures(
      maybeSeason: Option[Int]
  ): Source[(GameKey, Set[GameUpdateCommand]), NotUsed] = {
    fixtureUpdatesSupplier
      .fixtureUpdates(maybeSeason)
      .recoverWithRetries(
        0,
        { case ex: Exception =>
          logger.error("An error occurred downloading fixtures data", ex)
          Source.empty
        }
      )
  }
}
