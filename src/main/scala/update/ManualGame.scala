package update

import io.circe.Codec
import io.circe.generic.semiauto._

import java.time.Instant

case class ManualGame(
    opponents: String,
    at: Instant,
    attended: Boolean,
    venue: Option[String] = None, // No location means a home game
    tickets: Option[Instant]
)

object ManualGame {
  implicit val newGameCodec: Codec[ManualGame] = deriveCodec[ManualGame]
}
