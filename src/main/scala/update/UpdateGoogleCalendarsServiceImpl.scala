package update
import org.apache.pekko.NotUsed
import uk.co.unclealex.pog.Sources.*
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import calendar.google.{
  GameEventCreator,
  GameEventDateTimeUpdater,
  TicketsEventCreator
}
import db.{PersistedGame, PointsDao}

class UpdateGoogleCalendarsServiceImpl(
    val gameEventCreator: GameEventCreator,
    val gameEventDateTimeUpdater: GameEventDateTimeUpdater,
    val ticketsEventCreator: TicketsEventCreator,
    val pointsDao: PointsDao
) extends UpdateGoogleCalendarsService:

  override def updateGoogleCalendars()
      : Flow[NotableUpdate, NotableUpdate, NotUsed] =
    Flow[NotableUpdate].flatMapConcat: notableUpdate =>
      val gameFlow: Flow[PersistedGame, PersistedGame, NotUsed] =
        notableUpdate match
          case _: TicketsOnSaleNotableUpdate =>
            Flow[PersistedGame].flatMapConcat: game =>
              for
                points <- pointsDao.points()
                updatedGame <- Source
                  .single(game)
                  .via(ticketsEventCreator.flow(points))
              yield updatedGame
          case _: DateChangedNotableUpdate => gameEventDateTimeUpdater.flow()
          case _: NewGameNotableUpdate     => gameEventCreator.flow()
      Source
        .single(notableUpdate.game)
        .via(gameFlow)
        .map(notableUpdate.updateGame)
