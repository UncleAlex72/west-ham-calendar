package update

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import db.GameKey

import java.time.LocalDate

trait UpdatesProcessor {

  def update(
      fixtureUpdatesSource: Source[(GameKey, Set[GameUpdateCommand]), NotUsed],
      ticketUpdatesSourceBuilder: () => Source[
        (LocalDate, Set[GameUpdateCommand]),
        NotUsed
      ]
  ): Source[NotableUpdate, NotUsed]
}
