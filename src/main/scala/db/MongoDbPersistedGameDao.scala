package db

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import org.apache.pekko.{Done, NotUsed}
import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.dao.Database.Database
import uk.co.unclealex.mongodb.dao.{Index, MongoDbDao}
import uk.co.unclealex.mongodb.flows.Distinct

import java.time.{Clock, Instant, LocalDate}
import scala.concurrent.{ExecutionContext, Future}

class MongoDbPersistedGameDao(
    val eventualDatabase: Future[Database],
    val clock: Clock
)(using actorSystem: ActorSystem, ec: ExecutionContext)
    extends MongoDbDao[PersistedGame](eventualDatabase, clock, "game")
    with PersistedGameDao {

  override val indicies: Seq[Index[PersistedGame]] = Seq(
    index
      .on("day")
      .on("month")
      .on("year")
      .ascending
      .named("dateIndex"),
    index.on("season").ascending.named("seasonIndex"),
    index.on("at").ascending.named("datePlayedIndex").unique,
    index
      .on("location")
      .on("season")
      .on("opponents")
      .on(
        "competition"
      )
      .ascending
      .named("gameKeyIndex")
      .unique
  )

  /** Find a game by its [[Competition]], [[Location]], opponents and season.
    * Together, these are guaranteed to uniquely define a [[PersistedGame]].
    *
    * @param gameKey
    *   The [[GameKey]] to search for.
    * @return
    *   The uniquely defined [[PersistedGame]] if it exists or null otherwise.
    */
  override def findByBusinessKey(
      gameKey: GameKey
  ): Source[PersistedGame, NotUsed] = {
    findOne(
      "competition" === gameKey.competition.entryName &&
        "location" === gameKey.location.entryName &&
        "opponents" === gameKey.opponents &&
        "season" === gameKey.season
    ).log("mongodb.persisted-game-dao-find-by-business-key")
  }

  override def findAll(): Source[PersistedGame, NotUsed] =
    super[MongoDbDao].findAll()

  override def findByDatePlayed(
      datePlayed: Instant
  ): Source[PersistedGame, NotUsed] = {
    findOne("at" === datePlayed)
  }

  override def findByDatePlayed(
      localDate: LocalDate
  ): Source[PersistedGame, NotUsed] = {
    findOne(
      "day" === localDate.getDayOfMonth &&
        "month" === localDate.getMonthValue &&
        "year" === localDate.getYear
    ).log("mongodb.persisted-game-dao-find-by-date-played")
  }

  override def store(v: PersistedGame): Source[PersistedGame, NotUsed] = {
    super.store(v).log("mongodb.store")
  }

  override def findByAttended(
      attended: Boolean
  ): Source[PersistedGame, NotUsed] = {
    findWhere("attended" === attended).log("mongodb.find-by-attended")
  }

  override def findWithTickets(): Source[PersistedGame, NotUsed] = {
    findWhere("tickets".doesExist && "tickets".isNotEmpty)
      .log("mongodb.find-with-tickets")
  }

  override def findBySeason(season: Int): Source[PersistedGame, NotUsed] = {
    findWhere("season" === season, "season".asc)
  }

  override def updateAttended(
      gameId: ID,
      attended: Boolean
  ): Source[PersistedGame, NotUsed] = {
    val change: PersistedGame => PersistedGame = _.copy(attended = attended)
    update("_id" === gameId, change)
  }

  override def findByOpponents(
      opponents: String
  ): Source[PersistedGame, NotUsed] = {
    findWhere("opponents" === opponents, "at".desc)
  }

  override def listSeasons(): Source[Int, NotUsed] = {
    findAll(sort = "season".asc).map(_.season).via(Distinct.flow)
  }

  override def listOpponents(): Source[String, NotUsed] = {
    findAll(sort = "opponents".asc).map(_.opponents).via(Distinct.flow)
  }

  override def updateGameCalendarId(
      persistedGame: PersistedGame,
      id: String
  ): Source[PersistedGame, NotUsed] = {
    store(persistedGame.copy(gameCalendarEventId = Some(id)))
  }

  override def updateTicketsCalendarId(
      persistedGame: PersistedGame,
      id: String
  ): Source[PersistedGame, NotUsed] = {
    store(persistedGame.copy(ticketsCalendarEventId = Some(id)))
  }

  override def delete(id: ID): Source[Done, NotUsed] = {
    removeById(id).map(_ => Done.done())
  }
}

object MongoDbPersistedGameDao {
  def apply(database: Database, clock: Clock)(implicit
      actorSystem: ActorSystem,
      ec: ExecutionContext
  ): MongoDbPersistedGameDao = {
    new MongoDbPersistedGameDao(Future.successful(database), clock)
  }
}
