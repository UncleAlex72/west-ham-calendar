package db

import io.circe.Codec
import uk.co.unclealex.mongodb.bson.BsonDocumentCodec

case class Score(home: Int, away: Int) derives Codec, BsonDocumentCodec:
  val format: String = s"$home - $away"

/** Created by alex on 18/02/16.
  */
case class GameResult(score: Score, shootoutScore: Option[Score] = None)
    derives BsonDocumentCodec,
      Codec:
  val serialise: String =
    (Seq(score) ++ shootoutScore).map(s => s"${s.home}-${s.away}").mkString(",")
  val format: String =
    score.format + shootoutScore.map(s => s" (${s.format})").getOrElse("")
