package db

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source

trait PointsDao {

  def points(): Source[Int, NotUsed]

  def updatePoints(points: Int): Source[Unit, NotUsed]
}
