package db

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import calendar.google.CalendarType
import uk.co.unclealex.mongodb.dao.Database.Database
import uk.co.unclealex.mongodb.dao.{Index, MongoDbDao, Sort}

import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

class MongoDbCalendarIdDao(
    val eventualDatabase: Future[Database],
    val clock: Clock
)(implicit actorSystem: ActorSystem, ec: ExecutionContext)
    extends MongoDbDao[CalendarId](eventualDatabase, clock, "calendars")
    with CalendarIdDao {

  override val indicies: Seq[Index[CalendarId]] = Seq(
    index.on("calendarType").ascending.named("calendarTypeIndex")
  )

  override val defaultSort: Option[Sort] = "calendarType".asc

  override def findAll(): Source[CalendarId, NotUsed] =
    super[MongoDbDao].findAll()

  override def findByType(
      calendarType: CalendarType
  ): Source[CalendarId, NotUsed] = {
    findOne("calendarType" === calendarType.entryName)
  }
}
