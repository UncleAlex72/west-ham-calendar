/** Copyright 2010-2012 Alex Jones
  *
  * Licensed to the Apache Software Foundation (ASF) under one or more
  * contributor license agreements. See the NOTICE file distributed with work
  * for additional information regarding copyright ownership. The ASF licenses
  * file to you under the Apache License, Version 2.0 (the "License"); you may
  * not use file except in compliance with the License. You may obtain a copy of
  * the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations
  * under the License.
  */
package db

import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.stream.scaladsl.Source
import uk.co.unclealex.mongodb.bson.ID

import java.time.{Instant, LocalDate}

/** The data access object for [[PersistedGame]]s.
  *
  * @author
  *   alex
  */
trait PersistedGameDao {

  def delete(id: ID): Source[Done, NotUsed]

  def updateGameCalendarId(
      persistedGame: PersistedGame,
      id: String
  ): Source[PersistedGame, NotUsed]

  def updateTicketsCalendarId(
      persistedGame: PersistedGame,
      id: String
  ): Source[PersistedGame, NotUsed]

  def listOpponents(): Source[String, NotUsed]

  def findById(_id: ID): Source[PersistedGame, NotUsed]

  def findAll(): Source[PersistedGame, NotUsed]

  def findBySeason(season: Int): Source[PersistedGame, NotUsed]

  def updateAttended(
      gameId: ID,
      attended: Boolean
  ): Source[PersistedGame, NotUsed]

  def store(game: PersistedGame): Source[PersistedGame, NotUsed]

  def findByBusinessKey(gameKey: GameKey): Source[PersistedGame, NotUsed]

  def findByDatePlayed(datePlayed: Instant): Source[PersistedGame, NotUsed]
  def findByDatePlayed(localDate: LocalDate): Source[PersistedGame, NotUsed]

  def listSeasons(): Source[Int, NotUsed]

  def findByOpponents(opponents: String): Source[PersistedGame, NotUsed]

  def findByAttended(attended: Boolean): Source[PersistedGame, NotUsed]
  def findWithTickets(): Source[PersistedGame, NotUsed]
}
