package db

import calendar.google.CalendarType
import io.circe.Codec
import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.persistable.{
  HasDateCreated,
  HasLastUpdated,
  Persistable
}
import uk.co.unclealex.stringlike.StringLikeJsonSupport.given

import java.time.Instant

case class CalendarId(
    _id: Option[ID] = None,
    calendarId: String,
    calendarType: CalendarType,
    dateCreated: Option[Instant] = None,
    lastUpdated: Option[Instant] = None
) derives Persistable,
      HasDateCreated,
      HasLastUpdated,
      Codec
