/** Copyright 2012 Alex Jones
  *
  * Licensed to the Apache Software Foundation extends CompetitionJson(0, ASF)
  * under one or more contributor license agreements. See the NOTICE file
  * distributed with work for additional information regarding copyright
  * ownership. The ASF licenses file to you under the Apache License, Version
  * 2.0 extends CompetitionJson(0, the "License"); you may not use file except
  * in compliance with the License. You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations
  * under the License.
  */
package db

import com.typesafe.scalalogging.StrictLogging
import enumeratum.EnumEntry.Uppercase
import enumeratum._
import uk.co.unclealex.stringlike.StringLike

import scala.collection.immutable

/** The different competitions that West Ham have taken part in.
  *
  * @author
  *   alex
  */
sealed trait Competition extends Uppercase:

  /** The English name of this competition.
    */
  val name: String

  /** The code of this competition.
    */
  val code: String

  /** A flag that determines whether this competition is a primary English
    * league competition or not.
    */
  def isLeague: Boolean

  def matches(code: String, stage: String): Boolean

sealed abstract class AbstractCompetition(
    val name: String,
    val isLeague: Boolean,
    code: String,
    stageMatcher: String => Boolean = _ => true
) extends Competition:
  override def matches(code: String, stage: String): Boolean = {
    code == this.code && stageMatcher(stage)
  }

/** League based competitions.
  */
sealed abstract class LeagueCompetition(
    override val name: String,
    override val code: String
) extends AbstractCompetition(name, true, code)

/** Cup based competitions.
  */
sealed abstract class CupCompetition(
    override val name: String,
    override val code: String
) extends AbstractCompetition(name, false, code)

sealed abstract class EuropeanCompetition(
    override val name: String,
    override val code: String,
    val stageMatcher: String => Boolean
) extends AbstractCompetition(name, false, code, stageMatcher)

object Competition extends Enum[Competition]:

  val values: immutable.IndexedSeq[Competition] = findValues

  private def isGroupStage(stage: String): Boolean =
    stage.toLowerCase.contains("group")

  private def isNotGroupStage(stage: String): Boolean =
    !isGroupStage(stage)

  /** The FA Premiership.
    */
  case object PREM extends LeagueCompetition("Premiership", "EPL")

  /** The League Cup.
    */
  case object LGCP extends CupCompetition("League Cup", "LEC")

  /** The FA Cup.
    */
  case object FACP extends CupCompetition("FA Cup", "FAC")

  /** The Championship.
    */
  case object FLC extends LeagueCompetition("Championship", "FLC")

  /** The Championship play-offs.
    */
  case object FLCPO extends CupCompetition("Play-Offs", "EFL")

  /** Friendly fixtureUpdates.
    */
  case object FRIENDLY extends CupCompetition("Friendly", "FRE")

  /** The EUROPA League.
    */
  case object EUROPA_GROUP
      extends EuropeanCompetition(
        "UEFA Europa League (Group Stage)",
        "UEL",
        isGroupStage
      )

  case object EUROPA_KNOCKOUT
      extends EuropeanCompetition(
        "UEFA Europa League (Knockout Stage)",
        "UEL",
        isNotGroupStage
      )

  /** The EUROPA League.
    */
  case object EUROPA_CONFERENCE_GROUP
      extends EuropeanCompetition(
        "UEFA Europa Conference League (Group Stage)",
        "UCF",
        isGroupStage
      )

  case object EUROPA_CONFERENCE_KNOCKOUT
      extends EuropeanCompetition(
        "UEFA Europa Conference League (Knockout Stage)",
        "UCF",
        isNotGroupStage
      )

  def findForCodeAndStage(code: String, stage: String): Option[Competition] = {
    values.find(competition => competition.matches(code = code, stage = stage))
  }

  given competitionIsStringLike: StringLike[Competition] =
    StringLike.fromOption[Competition](
      _.entryName,
      token => Competition.values.find(_.entryName == token)
    )
