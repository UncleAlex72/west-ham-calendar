/** Copyright 2010-2012 Alex Jones
  *
  * Licensed to the Apache Software Foundation (ASF) under one or more
  * contributor license agreements. See the NOTICE file distributed with work
  * for additional information regarding copyright ownership. The ASF licenses
  * file to you under the Apache License, Version 2.0 (the "License"); you may
  * not use file except in compliance with the License. You may obtain a copy of
  * the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations
  * under the License.
  */
package db

import uk.co.unclealex.mongodb.bson.{BsonCodec, BsonDocumentCodec, ID}
import uk.co.unclealex.mongodb.persistable.{
  HasDateCreated,
  HasLastUpdated,
  Persistable
}

import java.time.Instant

/** Entity class storing rows of table Game
  *
  * @param opponents
  *   Database column opponents SqlType(varchar), Length(128,true)
  * @param attended
  *   Database column attended SqlType(bool), Default(None)
  * @param result
  *   Database column result SqlType(varchar), Length(128,true), Default(None)
  * @param competition
  *   Database column competition SqlType(varchar), Length(128,true)
  * @param season
  *   Database column season SqlType(int4)
  * @param attendance
  *   Database column attendence SqlType(int4), Default(None)
  * @param at
  *   Database column at SqlType(timestamp), Default(None)
  * @param forcedYear
  *   used to indicate coronavirus games in June & July 2020
  * @param _id
  *   Database column id SqlType(int8), PrimaryKey
  * @param location
  *   Database column location SqlType(varchar), Length(128,true)
  * @param matchReport
  *   Database column report SqlType(text), Default(None)
  * @param dateCreated
  *   Database column datecreated SqlType(timestamp)
  * @param lastUpdated
  *   Database column lastupdated SqlType(timestamp)
  */
case class PersistedGame(
    _id: Option[ID] = None,
    location: Location,
    season: Int,
    opponents: String,
    competition: Competition,
    at: Option[Instant] = None,
    day: Option[Int] = None,
    month: Option[Int] = None,
    year: Option[Int] = None,
    attended: Boolean = false,
    venue: Option[String] = None,
    result: Option[GameResult] = None,
    attendance: Option[Int] = None,
    tickets: Seq[PersistedTicket] = Seq.empty[PersistedTicket],
    gameCalendarEventId: Option[String] = None,
    ticketsCalendarEventId: Option[String] = None,
    dateCreated: Option[Instant] = None,
    lastUpdated: Option[Instant] = None
) derives Persistable,
      HasDateCreated,
      HasLastUpdated:

  /** Get the unique business key for this game.
    */
  def gameKey: GameKey = GameKey(competition, location, opponents, season)

case class PersistedTicket(points: Int, at: Instant) derives BsonDocumentCodec

object PersistedTicket:

  given persistedTicketsOrdering: Ordering[PersistedTicket] =
    Ordering.by(_.points)

object PersistedGame:

  def gameKey(gameKey: GameKey): PersistedGame =
    new PersistedGame(
      competition = gameKey.competition,
      location = gameKey.location,
      opponents = gameKey.opponents,
      season = gameKey.season
    )

  def gameKey(
      competition: Competition,
      location: Location,
      opponents: String,
      season: Int
  ): PersistedGame =
    gameKey(GameKey(competition, location, opponents, season))
