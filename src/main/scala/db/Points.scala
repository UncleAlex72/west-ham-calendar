package db

import io.circe.Codec
import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.persistable.{
  HasDateCreated,
  HasLastUpdated,
  Persistable,
  Upsertable
}

import java.time.Instant

case class Points(
    _id: Option[ID] = None,
    points: Int,
    lastUpdated: Option[Instant] = None,
    dateCreated: Option[Instant] = None
) derives Codec,
      Persistable,
      HasLastUpdated,
      HasDateCreated

object Points:

  given Upsertable[Points] = Upsertable(
    Points(0)
  )

  def apply(points: Int): Points = Points(None, points, None, None)
