package db

import io.circe.Codec
import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import uk.co.unclealex.mongodb.*
import uk.co.unclealex.mongodb.dao.Database.Database
import uk.co.unclealex.mongodb.dao.{MongoDbDao, Query}
import uk.co.unclealex.pog.Sources.*

import java.time.{Clock, Instant}
import scala.concurrent.{ExecutionContext, Future}

class MongoDbPointsDao(
    val eventualDatabase: Future[Database],
    val clock: Clock
)(implicit ec: ExecutionContext, actorSystem: ActorSystem)
    extends MongoDbDao[Points](eventualDatabase, clock, "points")
    with PointsDao {

  override def points(): Source[Int, NotUsed] = {
    findAll().map(_.points).headOrElse(0).log("mongodb.find-points")
  }

  override def updatePoints(points: Int): Source[Unit, NotUsed] = {
    upsert(Query.all, _.copy(points = points)).map(_ => ())
  }
}

object MongoDbPointsDao:
  def apply(mongoDatabase: Database, clock: Clock)(using
      actorSystem: ActorSystem,
      ec: ExecutionContext
  ): MongoDbPointsDao =
    new MongoDbPointsDao(Future.successful(mongoDatabase), clock)
