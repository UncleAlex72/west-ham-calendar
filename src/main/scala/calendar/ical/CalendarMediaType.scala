package calendar.ical

import org.apache.pekko.http.scaladsl.model.{
  HttpCharset,
  HttpCharsets,
  MediaType
}

import java.nio.charset.StandardCharsets

trait CalendarMediaType {

  val `text/calendar`: MediaType.WithFixedCharset = MediaType
    .textWithFixedCharset("calendar", HttpCharsets.`UTF-8`, "ics", "ical")
}

object CalendarMediaType extends CalendarMediaType
