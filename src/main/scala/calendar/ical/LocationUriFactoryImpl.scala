package calendar.ical

import org.apache.pekko.http.scaladsl.model.Uri

import java.net.URI

class LocationUriFactoryImpl() extends LocationUriFactory {
  override def locationUri(venue: String): URI = {
    new URI(
      Uri("https://www.google.co.uk/maps/")
        .withQuery(Uri.Query("q" -> venue))
        .toString()
    )
  }
}
