package calendar.ical

sealed trait CalendarType

object CalendarType {
  case class Attended(attended: Boolean) extends CalendarType
  case object Tickets extends CalendarType
}
