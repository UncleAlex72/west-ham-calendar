/** Copyright 2013 Alex Jones
  *
  * Licensed to the Apache Software Foundation (ASF) under one or more
  * contributor license agreements. See the NOTICE file distributed with this
  * work for additional information regarding copyright ownership. The ASF
  * licenses this file to you under the Apache License, Version 2.0 (the
  * "License"); you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations
  * under the License.
  *
  * @author
  *   unclealex72
  */

package calendar.ical

import calendar.Defaults
import db.Location.{AWAY, HOME}

import java.io.StringWriter
import java.time.{Clock, ZonedDateTime => jtZonedDateTime}
import net.fortuna.ical4j.data.CalendarOutputter
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.Status._
import net.fortuna.ical4j.model.property.Transp._
import net.fortuna.ical4j.model.property.{
  Attach,
  CalScale,
  Created,
  Description,
  DtEnd,
  DtStamp,
  DtStart,
  LastModified,
  Method,
  ProdId,
  Sequence,
  Summary,
  Uid,
  Version,
  XProperty,
  Location => ILocation
}
import net.fortuna.ical4j.model.{
  Property,
  PropertyList,
  Calendar => ICalendar,
  DateTime => IDateTime
}

import scala.language.implicitConversions

/*
 * A calendar writer that creates iCal calendars that are compatible with at least Google and Mozilla Thunderbird
 * @author alex
 *
 */
class IcalCalendarWriter(
    clock: Clock,
    locationUriFactory: LocationUriFactory
) extends CalendarWriter {

  /** The calendar outputter that actually outputs the calendar.
    */
  val outputter = new CalendarOutputter(false)

  def write(calendar: Calendar): String = {
    val ical = new ICalendar
    List(
      new ProdId("-//unclealex.co.uk//West Ham Calendar 6.0//EN"),
      Version.VERSION_2_0,
      Method.PUBLISH,
      new XProperty("X-WR-CALNAME", calendar.title),
      new XProperty("X-WR-CALDESC", calendar.title),
      new XProperty("X-WR-TIMEZONE", clock.getZone.getId),
      CalScale.GREGORIAN
    ) foreach ical.getProperties().add
    val vEvents: Seq[VEvent] = calendar.events.map(toVEvent)
    vEvents.foreach(ical.getComponents().add)
    val writer = new StringWriter()
    outputter.output(ical, writer)
    writer.getBuffer.toString
  }

  given singleConversion
      : Conversion[Event => Property, Event => Iterable[Property]] = fn =>
    event => Seq(fn(event))
  given optionConversion
      : Conversion[Event => Option[Property], Event => Iterable[Property]] =
    fn => event => fn(event).toSeq

  /** Output a single event.
    */
  def toVEvent(event: Event): VEvent = {
    val properties: Seq[Event => Iterable[Property]] =
      Seq(
        DTSTART,
        DTEND,
        DTSTAMP,
        UID,
        CREATED,
        DESCRIPTION,
        LAST_MODIFIED,
        LOCATION,
        SEQUENCE,
        STATUS,
        SUMMARY,
        LOCATION_URL,
        TRANSP
      )
    fluent(new VEvent(new PropertyList))(ve =>
      properties.foreach(f => f(event).foreach(ve.getProperties.add))
    )
  }

  /** Create a DTSTART property
    */
  def DTSTART: Event => Property = event => new DtStart(event.zonedDateTime)

  /** Create a DTEND property
    */
  def DTEND: Event => Property = event =>
    new DtEnd(event.zonedDateTime.plus(event.duration))

  /** Create a DTSAMP property
    */
  def DTSTAMP: Event => Property = _ => new DtStamp(jtZonedDateTime.now(clock))

  /** Create a UID property
    */
  def UID: Event => Property = event =>
    new Uid(s"${event.id}@calendar.unclealex.co.uk")

  /** Create a CREATED property
    */
  def CREATED: Event => Property = event => new Created(event.dateCreated)

  /** Create a DESCRIPTION property
    */
  def DESCRIPTION: Event => Option[Property] = { event =>
    val descriptionLine: ((String, Option[Any])) => Option[String] = { parts =>
      parts._2.map(value => s"${parts._1}: $value")
    }
    val descriptions: Seq[Option[String]] = Seq(
      "Result" -> event.result,
      "Attendence" -> event.attendance
    ) map descriptionLine
    if (descriptions.isEmpty) None
    else Some(new Description(descriptions.flatten.mkString("\n")))
  }

  /** Create a LAST-MODIFIED property
    */
  def LAST_MODIFIED: Event => Property = event =>
    new LastModified(event.lastUpdated)

  /** Create a LOCATION property
    */
  def LOCATION: Event => Option[Property] = event =>
    event.geoLocation map { gl => new ILocation(gl) }

  /** Create a SEQUENCE property
    */
  def SEQUENCE: Event => Property = _ => new Sequence(0)

  /** Create a STATUS property
    */
  def STATUS: Event => Property = _ => VEVENT_CONFIRMED

  /** Create a TRANSP property
    */
  def TRANSP: Event => Property = event =>
    if (event.busy) OPAQUE else TRANSPARENT

  /** Create a SUMMARY property
    */
  def SUMMARY: Event => Property = { event =>
    new Summary(
      Defaults.summary(event.opponents, event.location, event.competition)
    )
  }

  def LOCATION_URL: Event => Option[Property] = event => {
    event.geoLocation.map { venue =>
      new Attach(locationUriFactory.locationUri(venue))
    }
  }

  /** A small utility that allows a mutable object to be created, mutated and
    * then returned.
    */
  def fluent[E](value: E)(block: E => Any): E = {
    block(value)
    value
  }

  // Implicits

  /** An implicit used to create a Ical ZonedDateTime from a Joda ZonedDateTime
    */
  implicit def ical(zonedDateTime: jtZonedDateTime): IDateTime = {
    fluent(new IDateTime(zonedDateTime.toInstant.toEpochMilli)) { dt =>
      dt.setUtc(true)
    }
  }

  /** An implicit to convert Scala BigDecimals into Java BigDecimals
    */
  implicit def bigDecimal: BigDecimal => java.math.BigDecimal = bd =>
    new java.math.BigDecimal(bd.toString)

  /** Implicits to allow the property generating functions to either return an
    * optional or single property instead of a sequence of properties.
    */
  // noinspection ScalaUnnecessaryParentheses
  implicit def singleProperty
      : (Event => Property) => (Event => Iterable[Property]) = f =>
    e => Some(f(e))
  // noinspection ScalaUnnecessaryParentheses
  implicit def optionalProperty
      : (Event => Option[Property]) => (Event => Iterable[Property]) = f =>
    e => f(e)

}
