package calendar.ical

import java.net.URI

trait LocationUriFactory {

  def locationUri(venue: String): URI
}
