/** Copyright 2013 Alex Jones
  *
  * Licensed to the Apache Software Foundation (ASF) under one or more
  * contributor license agreements. See the NOTICE file distributed with this
  * work for additional information regarding copyright ownership. The ASF
  * licenses this file to you under the Apache License, Version 2.0 (the
  * "License"); you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations
  * under the License.
  *
  * @author
  *   unclealex72
  */

package calendar.ical

import calendar.Defaults
import calendar.ical.CalendarType.{Attended, Tickets}
import db.{PersistedGame, PersistedGameDao, PointsDao}
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Source
import tickets.TicketsFactory
import uk.co.unclealex.pog.Sources

import java.time.*

/** The default implementation of the Calendar factory.
  *
  * @author
  *   alex
  */
class CalendarFactoryImpl(
    val persistedGameDao: PersistedGameDao,
    val pointsDao: PointsDao,
    val ticketsFactory: TicketsFactory,
    val clock: Clock
) extends CalendarFactory:

  private val zoneId: ZoneId = clock.getZone

  override def create(calendarType: CalendarType): Source[Calendar, NotUsed] =
    val events: Source[Event, NotUsed] =
      gameDecorator(calendarType).flatMapConcat: decoratedGameFactory =>
        games(calendarType).flatMapConcat: persistedGame =>
          val maybeEvent = decoratedGameFactory(persistedGame).flatMap:
            decoratedGame => createEvent(decoratedGame, calendarType)
          Sources.fromOption(maybeEvent)
    val nonEmptyEvents: Source[Seq[Event], NotUsed] =
      events.fold(Seq.empty[Event])(_ :+ _).filterNot(_.isEmpty)
    nonEmptyEvents.map: nonEmptyEvents =>
      val title = createTitle(calendarType)
      Calendar("whufc_" + title.replace(' ', '_'), title, nonEmptyEvents.sorted)

  private def createEvent(
      decoratedGame: DecoratedGame,
      calendarType: CalendarType
  ): Option[Event] =
    val game = decoratedGame.persistedGame
    val at = decoratedGame.at
    val duration = decoratedGame.duration
    for
      id <- game._id
      instantCreated <- game.dateCreated
    yield
      val dateCreated: ZonedDateTime = instantCreated.atZone(zoneId)
      val lastUpdated: ZonedDateTime =
        game.lastUpdated.map(_.atZone(zoneId)).getOrElse(dateCreated)
      new Event(
        id = id.id,
        gameId = id.id,
        competition = game.competition,
        location = game.location,
        geoLocation = game.venue,
        opponents = game.opponents,
        zonedDateTime = at.atZone(zoneId),
        duration = duration,
        result = game.result.map(_.format),
        attendance = game.attendance,
        busy = busy(calendarType),
        dateCreated = dateCreated,
        lastUpdated = lastUpdated
      )

  def busy(calendarType: CalendarType): Boolean =
    calendarType match
      case Attended(false) => false
      case _               => true

  def games(calendarType: CalendarType): Source[PersistedGame, NotUsed] =
    calendarType match
      case Attended(attended) => persistedGameDao.findByAttended(attended)
      case Tickets            => persistedGameDao.findWithTickets()

  private def createTitle(calendarType: CalendarType): String =
    calendarType match
      case Attended(attended) =>
        val adjective = if (attended) "attended" else "non-attended"
        s"Game dates for all $adjective games"
      case Tickets => "Ticket selling dates for all games"

  private def gameDecorator(
      calendarType: CalendarType
  ): Source[DecoratedGameFactory, NotUsed] =
    calendarType match
      case Attended(_) =>
        Source.single(persistedGame =>
          persistedGame.at.map(at =>
            DecoratedGame(
              persistedGame = persistedGame,
              at = at,
              duration = Defaults.MATCH_DURATION
            )
          )
        )
      case Tickets =>
        pointsDao
          .points()
          .map: points =>
            persistedGame =>
              val maybeTicketSellingDate =
                ticketsFactory.saleDate(persistedGame.tickets, points)
              maybeTicketSellingDate.map(ticketSellingDate =>
                DecoratedGame(
                  persistedGame = persistedGame,
                  at = ticketSellingDate,
                  duration = Defaults.TICKETS_DURATION
                )
              )

  private case class DecoratedGame(
      persistedGame: PersistedGame,
      at: Instant,
      duration: Duration
  )

  private type DecoratedGameFactory = PersistedGame => Option[DecoratedGame]
