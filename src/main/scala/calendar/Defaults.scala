package calendar

import db.{Competition, Location}
import db.Location.{AWAY, HOME}

import java.time.Duration
import java.time.temporal.ChronoUnit

object Defaults {

  def summary(
      opponents: String,
      location: Location,
      competition: Competition
  ): String = {
    // noinspection ScalaUnnecessaryParentheses
    val swapOnAway: ((String, String)) => (String, String) = { p =>
      location match {
        case HOME => p
        case AWAY => p.swap
      }
    }
    val teams: (String, String) = swapOnAway("West Ham", opponents)
    s"${teams._1} vs ${teams._2} (${competition.name})"
  }

  val MATCH_DURATION: Duration = Duration.of(2, ChronoUnit.HOURS)
  val TICKETS_DURATION: Duration = Duration.of(1, ChronoUnit.HOURS)
}
