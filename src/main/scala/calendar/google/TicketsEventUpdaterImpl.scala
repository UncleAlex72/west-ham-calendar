package calendar.google

import org.apache.pekko.NotUsed
import uk.co.unclealex.pog.Sources.*
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import calendar.Defaults
import db.PersistedGame
import tickets.TicketsFactory

class TicketsEventUpdaterImpl(
    ticketsFactory: TicketsFactory,
    ticketsEventCreator: TicketsEventCreator,
    calendarService: CalendarService
) extends TicketsEventUpdater {

  override def flow(
      points: Int
  ): Flow[PersistedGame, PersistedGame, NotUsed] = {
    Flow[PersistedGame].flatMapConcat { persistedGame =>
      persistedGame.ticketsCalendarEventId match {
        case Some(eventId) =>
          ticketsFactory.saleDate(persistedGame.tickets, points) match {
            case Some(saleDate) =>
              calendarService
                .changeWhen(
                  calendarType = CalendarType.TICKETS,
                  eventId = eventId,
                  to = saleDate,
                  duration = Defaults.TICKETS_DURATION
                )
                .headOption
                .map(_ => persistedGame)
            case None =>
              Source.single(persistedGame)
          }
        case None =>
          Source.single(persistedGame).via(ticketsEventCreator.flow(points))
      }
    }
  }
}
