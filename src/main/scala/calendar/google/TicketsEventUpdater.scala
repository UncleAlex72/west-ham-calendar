package calendar.google

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import db.PersistedGame

trait TicketsEventUpdater {

  def flow(points: Int): Flow[PersistedGame, PersistedGame, NotUsed]
}
