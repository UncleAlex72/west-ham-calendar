package calendar.google

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import db.PersistedGame

trait GameEventCreator {

  def flow(): Flow[PersistedGame, PersistedGame, NotUsed]
}
