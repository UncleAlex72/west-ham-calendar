package calendar.google

import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.stream.scaladsl.Source
import com.google.api.services.calendar.model.Event
import db.CalendarId

import java.time.{Duration, Instant}

trait CalendarService {

  /** Remove all events from a calendar.
    *
    * @param calendarType
    *   The calendar to clear.
    * @return
    *   The id if the calendar was created or none if the calendar was cleared.
    */
  def clearOrCreate(calendarType: CalendarType): Source[Option[String], NotUsed]

  /** Create an event in a calendar.
    *
    * @param description
    *   The event's description.
    * @param location
    *   The location of the event.
    * @param at
    *   When the event is.
    * @param duration
    *   The duration of the event.
    * @param calendarType
    *   The calendar in which to store the event.
    * @return
    *   The ID of the newly created event.
    */
  def create(
      description: String,
      location: Option[String],
      at: Instant,
      duration: Duration,
      calendarType: CalendarType
  ): Source[String, NotUsed]

  /** Move an event from one calendar to another.
    *
    * @param eventId
    *   The ID of the event.
    * @param source
    *   The source calendar type.
    * @param target
    *   The target calendar type.
    * @return
    *   Done.
    */
  def move(
      eventId: String,
      source: CalendarType,
      target: CalendarType
  ): Source[Done, NotUsed]

  def list(calendarType: CalendarType): Source[Event, NotUsed]

  /** Change the time of an event.
    *
    * @param eventId
    *   The ID of the event.
    * @param calendarType
    *   The calendar containing the event.
    * @param to
    *   The new time of the event.
    * @param duration
    *   The new duration of the event.
    * @return
    *   Done.
    */
  def changeWhen(
      eventId: String,
      calendarType: CalendarType,
      to: Instant,
      duration: Duration
  ): Source[Done, NotUsed]

  def sendInvites(
      userFilter: String => Boolean
  ): Source[(String, String), NotUsed]

  def sendInvite(calendarType: CalendarType)(
      userFilter: String => Boolean
  ): Source[(String, String), NotUsed]

  def remove(calendarType: CalendarType, eventId: String): Source[Done, NotUsed]
}
