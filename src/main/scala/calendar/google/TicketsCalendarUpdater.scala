package calendar.google

import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.stream.scaladsl.Source

trait TicketsCalendarUpdater {

  def updateFutureTicketSellingDates(points: Int): Source[Done, NotUsed]
}
