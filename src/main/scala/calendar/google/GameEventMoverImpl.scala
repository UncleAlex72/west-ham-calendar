package calendar.google
import org.apache.pekko.NotUsed
import uk.co.unclealex.pog.Sources.*
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import db.PersistedGame

class GameEventMoverImpl(val calendarService: CalendarService)
    extends GameEventMover {

  override def flow(): Flow[PersistedGame, PersistedGame, NotUsed] = {
    Flow[PersistedGame].flatMapConcat { persistedGame =>
      val (sourceCalendarType, targetCalendarType) =
        if (persistedGame.attended) {
          (CalendarType.NON_ATTENDED, CalendarType.ATTENDED)
        } else {
          (CalendarType.ATTENDED, CalendarType.NON_ATTENDED)
        }
      val update = for {
        eventId <- Source(persistedGame.gameCalendarEventId.toSeq)
        response <- calendarService.move(
          eventId = eventId,
          source = sourceCalendarType,
          target = targetCalendarType
        )
      } yield {
        response
      }
      update.headOption.map(_ => persistedGame)
    }
  }
}
