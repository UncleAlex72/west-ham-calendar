package calendar.google

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import calendar.Defaults
import db.{PersistedGame, PersistedGameDao}
import tickets.TicketsFactory

import java.time.Clock

class TicketsEventCreatorImpl(
    override val persistedGameDao: PersistedGameDao,
    override val calendarService: CalendarService,
    override val clock: Clock,
    val ticketsFactory: TicketsFactory
) extends AbstractEventCreator(persistedGameDao, calendarService, clock)
    with TicketsEventCreator {

  override def flow(
      points: Int
  ): Flow[PersistedGame, PersistedGame, NotUsed] = {
    createFlow(
      calendarProvider = _ => CalendarType.TICKETS,
      dateTimeProvider =
        persistedGame => ticketsFactory.saleDate(persistedGame.tickets, points),
      locationProvider = _ => None,
      duration = Defaults.TICKETS_DURATION,
      persistGame = _.updateTicketsCalendarId
    )
  }
}
