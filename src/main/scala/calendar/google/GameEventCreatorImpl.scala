package calendar.google

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Flow
import calendar.Defaults
import db.{PersistedGame, PersistedGameDao}

import java.time.Clock

class GameEventCreatorImpl(
    override val persistedGameDao: PersistedGameDao,
    override val calendarService: CalendarService,
    override val clock: Clock
) extends AbstractEventCreator(persistedGameDao, calendarService, clock)
    with GameEventCreator {

  override def flow(): Flow[PersistedGame, PersistedGame, NotUsed] = {
    createFlow(
      calendarProvider = persistedGame =>
        if (persistedGame.attended) CalendarType.ATTENDED
        else CalendarType.NON_ATTENDED,
      dateTimeProvider = _.at,
      locationProvider = _.venue,
      duration = Defaults.MATCH_DURATION,
      persistGame = _.updateGameCalendarId
    )
  }
}
