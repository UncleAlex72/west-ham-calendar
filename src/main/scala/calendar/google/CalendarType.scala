package calendar.google

import enumeratum.EnumEntry.Uppercase
import enumeratum._
import uk.co.unclealex.stringlike.StringLike

sealed trait CalendarType extends EnumEntry with Uppercase

object CalendarType extends Enum[CalendarType]:

  case object ATTENDED extends CalendarType
  case object NON_ATTENDED extends CalendarType
  case object TICKETS extends CalendarType

  override def values: IndexedSeq[CalendarType] = findValues

  given StringLike[CalendarType] =
    StringLike.fromValues[CalendarType](values, _.entryName)
