package calendar.google

class CalendarNameSupplierImpl(val maybePrefix: Option[String])
    extends CalendarNameSupplier {

  def calendarName(calendarType: CalendarType): String = {
    val defaultCalendarName = calendarType match {
      case CalendarType.ATTENDED     => "Attended West Ham Games"
      case CalendarType.NON_ATTENDED => "Non-attended West Ham Games"
      case CalendarType.TICKETS      => "West Ham Ticket Selling Times"
    }
    val maybeFullPrefix = maybePrefix.map(prefix => s"[$prefix]")
    (maybeFullPrefix.toSeq :+ defaultCalendarName).mkString(" ")
  }
}
