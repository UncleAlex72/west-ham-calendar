package calendar.google

import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.stream.scaladsl.Source

trait CalendarSeeder {

  def generate(): Source[Done, NotUsed]
}
