package calendar.google

import calendar.Defaults
import db.{PersistedGame, PersistedGameDao}
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import uk.co.unclealex.pog.Sources
import uk.co.unclealex.pog.Sources.*

class GameEventDateTimeUpdaterImpl(
    val persistedGameDao: PersistedGameDao,
    val calendarService: CalendarService
) extends GameEventDateTimeUpdater:

  override def flow(): Flow[PersistedGame, PersistedGame, NotUsed] =
    Flow[PersistedGame].flatMapConcat: game =>
      val calendarType =
        if (game.attended) CalendarType.ATTENDED else CalendarType.NON_ATTENDED
      for
        eventId <- Sources.fromOption(game.gameCalendarEventId)
        at <- Sources.fromOption(game.at)
        _ <-
          calendarService.changeWhen(
            eventId = eventId,
            calendarType = calendarType,
            to = at,
            duration = Defaults.MATCH_DURATION
          )
      yield game
