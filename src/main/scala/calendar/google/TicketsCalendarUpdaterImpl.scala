package calendar.google

import uk.co.unclealex.pog.Sources.*
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.{Done, NotUsed}
import db.PersistedGameDao
import tickets.TicketsFactory

import java.time.Clock

class TicketsCalendarUpdaterImpl(
    persistedGameDao: PersistedGameDao,
    ticketsEventUpdater: TicketsEventUpdater,
    ticketsFactory: TicketsFactory,
    clock: Clock
) extends TicketsCalendarUpdater {

  override def updateFutureTicketSellingDates(
      points: Int
  ): Source[Done, NotUsed] = {
    val games = for {
      candidatePersistedGame <- persistedGameDao.findWithTickets()
      ticketsSaleDate <- Source(
        ticketsFactory.saleDate(candidatePersistedGame.tickets, points).toSeq
      )
      persistedGame <- Source.single(candidatePersistedGame)
      if ticketsSaleDate.isAfter(clock.instant())
      persistedGameWithNewTickets <- Source
        .single(persistedGame)
        .via(ticketsEventUpdater.flow(points))
    } yield {
      persistedGameWithNewTickets
    }
    games.fold(Done)((_, _) => Done)
  }
}
