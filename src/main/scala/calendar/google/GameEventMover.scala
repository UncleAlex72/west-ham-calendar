package calendar.google

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import db.PersistedGame

trait GameEventMover {

  def flow(): Flow[PersistedGame, PersistedGame, NotUsed]
}
