package calendar.google

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow
import db.PersistedGame

trait GameEventDateTimeUpdater {

  def flow(): Flow[PersistedGame, PersistedGame, NotUsed]
}
