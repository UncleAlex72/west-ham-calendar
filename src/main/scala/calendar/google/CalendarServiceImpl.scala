package calendar.google

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import org.apache.pekko.stream.RestartSettings
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.{Done, NotUsed}
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.googleapis.json.GoogleJsonResponseException
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.util.DateTime
import com.google.api.services.calendar.model.{
  AclRule,
  Calendar,
  Event,
  EventDateTime,
  Events
}
import com.google.api.services.calendar.{CalendarScopes, Calendar as GCalendar}
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.GoogleCredentials
import com.typesafe.scalalogging.StrictLogging
import db.{CalendarId, CalendarIdDao}
import uk.co.unclealex.pog.Sources
import uk.co.unclealex.pog.Sources.*
import user.ValidUsersSupplier

import java.nio.file.{Files, Path}
import java.time.{Clock, Duration, Instant, ZonedDateTime}
import java.util.Collections
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters.*

//noinspection LanguageFeature
class CalendarServiceImpl(
    val googleCalendar: GCalendar,
    val calendarIdDao: CalendarIdDao,
    val validUsersSupplier: ValidUsersSupplier,
    val calendarNameSupplier: CalendarNameSupplier,
    val role: CalendarRole,
    val clock: Clock
)(using val ec: ExecutionContext)
    extends CalendarService
    with StrictLogging:

  @FunctionalInterface
  trait NullGuard[A, B] {
    def replace(a: A): B
  }

  given voidNullGuard: NullGuard[Void, Done] = _ => Done
  given eventsNullGuard: NullGuard[Events, Events] = identity[Events]
  given eventNullGuard: NullGuard[Event, Event] = identity[Event]
  given aclRuleNullGuard: NullGuard[AclRule, AclRule] = identity[AclRule]
  given calendarNullGuard: NullGuard[Calendar, Calendar] =
    identity[Calendar]

  given Conversion[Instant, EventDateTime] = (instant: Instant) =>
    val zonedDateTime = ZonedDateTime.ofInstant(instant, clock.getZone)
    val dateTime = new DateTime(
      instant.toEpochMilli,
      zonedDateTime.getOffset.getTotalSeconds / 60
    )
    new EventDateTime().setDateTime(dateTime)

  override def list(calendarType: CalendarType): Source[Event, NotUsed] = {
    for {
      calendarId <- idOf(calendarType)
      event <- listEvents(calendarId)
    } yield {
      event
    }
  }

  private def listEvents(calendarId: String): Source[Event, NotUsed] =
    def listEvents(maybeToken: Option[String]): Source[Event, NotUsed] =
      val pageSource = executeEvents: events =>
        maybeToken.foldLeft(events.list(calendarId)): (list, token) =>
          list.setPageToken(token)
      pageSource.flatMap: events =>
        val maybeNextToken =
          Option(events.getNextPageToken).filterNot(_.isEmpty)
        def nextPageSource(): Source[Event, NotUsed] = maybeNextToken match
          case Some(nextToken) =>
            listEvents(Some(nextToken))
          case None =>
            Source.empty
        Source(events.getItems.asScala.toSeq)
          .concatLazy(Source.lazySource(nextPageSource))
    listEvents(None)

  override def sendInvite(
      calendarType: CalendarType
  )(userFilter: String => Boolean): Source[(String, String), NotUsed] =
    for
      calendarId <- idOf(calendarType)
      emailAndId <- addUsers(calendarId, userFilter, delay = false)
    yield emailAndId

  private def addUsers(
      calendarId: String,
      userFilter: String => Boolean,
      delay: Boolean
  ): Source[(String, String), NotUsed] =
    def forceDelay(): Source[Done, NotUsed] =
      if (delay)
        Source.single(Done).delay(15.seconds)
      else
        Source.single(Done)
    def addAcl(calendarId: String, email: String): Source[String, NotUsed] =
      executeAcl { acl =>
        val aclRule = new AclRule()
          .setRole(role.role)
          .setScope(new AclRule.Scope().setType("user").setValue(email))
        val result = acl.insert(calendarId, aclRule)
        logger.info(s"Added user $email to calendar '$calendarId'")
        result
      }.map(_.getId)

    for
      email <- validUsersSupplier.validUsers() if userFilter(email)
      // Allow the calendar to be properly created
      _ <- forceDelay()
      aclId <- addAcl(calendarId, email)
    yield email -> aclId

  private def executeAcl[A](
      block: GCalendar#Acl => AbstractGoogleClientRequest[A]
  )(using nullGuard: NullGuard[A, A]): Source[A, NotUsed] =
    doExecute(_.acl())(block)

  private def doExecute[A, B, C](serviceFactory: GCalendar => A)(
      block: A => AbstractGoogleClientRequest[B]
  )(using nullGuard: NullGuard[B, C]): Source[C, NotUsed] =
    Sources
      .restartOnFailuresWithBackoff(
        RestartSettings(5.seconds, 2.minutes, 0.2),
        { case t: GoogleJsonResponseException =>
          t.getDetails.getCode == 403
        }
      ) { () =>
        SF:
          val response = block.apply(serviceFactory(googleCalendar)).execute()
          nullGuard.replace(response)
      }

  private def SF[T](block: => T): Source[T, NotUsed] =
    Source.future(Future(block))

  private def idOf(calendarType: CalendarType): Source[String, NotUsed] =
    for calendarId <- calendarIdDao.findByType(calendarType)
    yield calendarId.calendarId

  override def sendInvites(
      userFilter: String => Boolean
  ): Source[(String, String), NotUsed] =
    for
      calendarId <- calendarIdDao.findAll()
      emailAndId <- addUsers(calendarId.calendarId, userFilter, delay = false)
    yield emailAndId

  override def clearOrCreate(
      calendarType: CalendarType
  ): Source[Option[String], NotUsed] =
    def clear(calendarId: CalendarId): Source[Option[String], NotUsed] =
      for
        event <- listEvents(calendarId.calendarId)
        _ <- executeEvents { events =>
          logger.info(
            s"Deleting event '${event.getId}' from calendar ${calendarId.calendarType} (${calendarId.calendarId})'"
          )
          events.delete(calendarId.calendarId, event.getId)
        }
      yield None

    def create(): Source[String, NotUsed] =
      val calendarName = calendarNameSupplier.calendarName(calendarType)
      val calendar: Calendar = new Calendar()
        .setSummary(calendarName)
        .setTimeZone(clock.getZone.getId)
      for newCalendar <- executeCalendars(_.insert(calendar))
      yield
        logger.info(
          s"Created calendar '$calendarName' with id '${newCalendar.getId}"
        )
        newCalendar.getId

    def persist(
        calendarType: CalendarType,
        calendarId: String
    ): Source[CalendarId, NotUsed] =
      calendarIdDao.store(
        CalendarId(calendarType = calendarType, calendarId = calendarId)
      )

    calendarIdDao.findByType(calendarType).headOption.flatMapConcat {
      case Some(calendarId) =>
        clear(calendarId)
      case None =>
        for {
          id <- create()
          _ <- addUsers(id, _ => true, delay = true)
          _ <- persist(calendarType, id)
        } yield {
          Option(id)
        }
    }

  /** Create an event in a calendar
    *
    * @param description
    *   The event's description.
    * @param location
    *   The location of the event.
    * @param at
    *   When the event is.
    * @param duration
    *   The duration of the event.
    * @param calendarType
    *   The Calendar in which to store the event.
    * @return
    *   The ID of the newly created event.
    */
  override def create(
      description: String,
      location: Option[String],
      at: Instant,
      duration: Duration,
      calendarType: CalendarType
  ): Source[String, NotUsed] = {
    val event = new Event()
      .setStart(at)
      .setEnd(at.plus(duration))
      .setLocation(location.orNull)
      .setSummary(description)
    for
      calendarId <- idOf(calendarType)
      evt <- executeEvents(_.insert(calendarId, event))
    yield
      logger.info(
        s"Added event '$description' (${evt.getId}) to calendar $calendarType ($calendarId) at $at"
      )
      evt.getId
  }

  private def executeCalendars[A](
      block: GCalendar#Calendars => AbstractGoogleClientRequest[A]
  )(using nullGuard: NullGuard[A, A]): Source[A, NotUsed] =
    doExecute(_.calendars())(block)

  /** Move an event from one calendar to another.
    *
    * @param eventId
    *   The ID of the event.
    * @param source
    *   The source calendar.
    * @param target
    *   The target calendar.
    * @return
    *   Done.
    */
  override def move(
      eventId: String,
      source: CalendarType,
      target: CalendarType
  ): Source[Done, NotUsed] =
    for
      sourceId <- idOf(source)
      targetId <- idOf(target)
      _ <- executeEvents(_.move(sourceId, eventId, targetId))
    yield
      logger.info(
        s"Moved event $eventId from calendar $source ($sourceId) to $target ($targetId)"
      )
      Done

  private def executeEvents[A, B](
      block: GCalendar#Events => AbstractGoogleClientRequest[A]
  )(using nullGuard: NullGuard[A, B]): Source[B, NotUsed] =
    doExecute(_.events())(block)

  /** Change the time of an event.
    *
    * @param eventId
    *   The ID of the event.
    * @param calendarType
    *   The calendar containing the event.
    * @param to
    *   The new time of the event.
    * @return
    *   Done.
    */
  override def changeWhen(
      eventId: String,
      calendarType: CalendarType,
      to: Instant,
      duration: Duration
  ): Source[Done, NotUsed] =
    for
      calendarId <- idOf(calendarType)
      _ <- executeEvents { events =>
        val updatedEvent = new Event().setStart(to).setEnd(to.plus(duration))
        val result = events.patch(calendarId, eventId, updatedEvent)
        logger.info(
          s"Moved event '$eventId' in calendar $calendarType ($calendarId) to $to"
        )
        result
      }
    yield Done

  override def remove(
      calendarType: CalendarType,
      eventId: String
  ): Source[Done, NotUsed] =
    for
      calendarId <- idOf(calendarType)
      _ <- executeEvents: events =>
        events.delete(calendarId, eventId)
    yield
      logger.info(s"Removed event '$eventId' from calendar '$calendarId'")
      Done

object CalendarServiceImpl {
  def apply(
      calendarIdDao: CalendarIdDao,
      credentialsPath: Path,
      validUsersSupplier: ValidUsersSupplier,
      clock: Clock,
      role: CalendarRole,
      calendarNameSupplier: CalendarNameSupplier,
      executionContext: ExecutionContext
  ): CalendarServiceImpl = {
    val credentials =
      GoogleCredentials
        .fromStream(Files.newInputStream(credentialsPath))
        .createScoped(Collections.singleton(CalendarScopes.CALENDAR))
    val HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport
    val JSON_FACTORY = GsonFactory.getDefaultInstance
    val calendar = new GCalendar.Builder(
      HTTP_TRANSPORT,
      JSON_FACTORY,
      new HttpCredentialsAdapter(credentials)
    ).setApplicationName("West Ham Calendar").build
    new CalendarServiceImpl(
      googleCalendar = calendar,
      calendarIdDao = calendarIdDao,
      clock = clock,
      role = role,
      validUsersSupplier = validUsersSupplier,
      calendarNameSupplier = calendarNameSupplier
    )(using executionContext)
  }
}
