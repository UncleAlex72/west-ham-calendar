package calendar.google

trait CalendarNameSupplier {

  def calendarName(calendarType: CalendarType): String
}
