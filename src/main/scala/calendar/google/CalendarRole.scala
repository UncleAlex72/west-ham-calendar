package calendar.google

import enumeratum.*
import net.ceedubs.ficus.readers.ValueReader
import uk.co.unclealex.stringlike.StringLike

sealed abstract class CalendarRole(val role: String) extends EnumEntry

object CalendarRole extends Enum[CalendarRole]:

  import net.ceedubs.ficus.*

  case object READER extends CalendarRole("reader")
  case object WRITER extends CalendarRole("writer")

  override def values: IndexedSeq[CalendarRole] = findValues

  given StringLike[CalendarRole] =
    StringLike.fromValues(values, _.role)

  given calendarRoleValueReader(using
      stringValueReader: ValueReader[String]
  ): ValueReader[CalendarRole] = stringValueReader.map: str =>
    summon[StringLike[CalendarRole]].tryDecode(str.toLowerCase).get
