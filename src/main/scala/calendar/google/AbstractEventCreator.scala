package calendar.google

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import calendar.Defaults
import db.{PersistedGame, PersistedGameDao}

import java.time.{Clock, Duration, Instant, ZoneId}

class AbstractEventCreator(
    val persistedGameDao: PersistedGameDao,
    val calendarService: CalendarService,
    val clock: Clock
) {

  val zoneId: ZoneId = clock.getZone

  def createFlow(
      calendarProvider: PersistedGame => CalendarType,
      dateTimeProvider: PersistedGame => Option[Instant],
      locationProvider: PersistedGame => Option[String],
      duration: Duration,
      persistGame: PersistedGameDao => (
          PersistedGame,
          String
      ) => Source[PersistedGame, NotUsed]
  ): Flow[PersistedGame, PersistedGame, NotUsed] = {
    Flow[PersistedGame].flatMapConcat { persistedGame =>
      dateTimeProvider(persistedGame) match {
        case Some(when) =>
          val description = Defaults.summary(
            opponents = persistedGame.opponents,
            location = persistedGame.location,
            competition = persistedGame.competition
          )
          calendarService
            .create(
              description = description,
              location = locationProvider(persistedGame),
              calendarType = calendarProvider(persistedGame),
              at = when,
              duration = duration
            )
            .flatMapConcat { id =>
              persistGame(persistedGameDao)(persistedGame, id)
            }
        case None => Source.empty
      }
    }

  }
}
