package calendar.google

import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.{Done, NotUsed}
import uk.co.unclealex.pog.Sources.*
import db.{PersistedGame, PersistedGameDao, PointsDao}
import tickets.TicketsFactory

class CalendarSeederImpl(
    val persistedGameDao: PersistedGameDao,
    val pointsDao: PointsDao,
    val calendarService: CalendarService,
    val gameEventCreator: GameEventCreator,
    val ticketsEventCreator: TicketsEventCreator,
    val ticketsFactory: TicketsFactory
) extends CalendarSeeder {

  override def generate(): Source[Done, NotUsed] = {
    clearCalendars()
      .fold(Done)((_, _) => Done)
      .flatMapConcat(_ => createGames())
  }

  def clearCalendars(): Source[Done, NotUsed] = {
    for {
      calendarType <- Source(CalendarType.values)
      _ <- calendarService.clearOrCreate(calendarType)
    } yield {
      Done
    }
  }

  def createGames(): Source[Done, NotUsed] = {
    pointsDao
      .points()
      .flatMapConcat { points =>
        persistedGameDao
          .findAll()
          .fold(Seq.empty[PersistedGame])(_ :+ _)
          .flatMapConcat { games =>
            Source(games)
          }
          .via(gameEventCreator.flow())
          .via(ticketsEventCreator.flow(points))
      }
      .map(_ => Done)
  }
}
