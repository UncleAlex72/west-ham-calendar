package calendar.docker

import cats.effect.IO

trait ContainerManager:

  def start: IO[Unit]
  def stop: IO[Unit]
