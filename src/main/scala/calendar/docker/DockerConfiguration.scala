package calendar.docker

import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ValueReader

case class DockerConfiguration(
    containerName: String,
    image: String,
    environment: Seq[String],
    ports: Seq[String],
    shmSize: String,
    networks: Seq[String],
    volumes: Seq[String]
)

object DockerConfiguration:
  given dockerConfigurationValueReader: ValueReader[DockerConfiguration] =
    ValueReader.relative[DockerConfiguration]: conf =>
      given stringSeqConfigReader: ValueReader[Seq[String]] =
        implicitly[ValueReader[String]].map: str =>
          str.split(",").map(_.trim).filterNot(_.isEmpty).toSeq
      val containerName = conf.as[String]("containerName")
      val image = conf.as[String]("image")
      val environment = conf.as[Seq[String]]("environment")
      val ports = conf.as[Seq[String]]("ports")
      val shmSize = conf.as[String]("shmSize")
      val networks = conf.as[Seq[String]]("networks")
      val volumes = conf.as[Seq[String]]("volumes")
      DockerConfiguration(
        containerName = containerName,
        image = image,
        environment = environment,
        ports = ports,
        shmSize = shmSize,
        networks = networks,
        volumes = volumes
      )
