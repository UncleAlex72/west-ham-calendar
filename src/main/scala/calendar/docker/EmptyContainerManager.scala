package calendar.docker

import cats.effect.IO

class EmptyContainerManager extends ContainerManager {
  override def start: IO[Unit] = IO.unit

  override def stop: IO[Unit] = IO.unit
}
