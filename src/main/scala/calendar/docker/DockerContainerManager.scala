package calendar.docker

import cats.effect.IO
import com.typesafe.scalalogging.StrictLogging
import io.circe._
import retry.Retry

import scala.concurrent.duration.DurationInt
import scala.sys.process._

class DockerContainerManager(val dockerConfiguration: DockerConfiguration)
    extends ContainerManager
    with StrictLogging {

  private val containerName = dockerConfiguration.containerName

  private val fullyQualifiedContainerName: String = {
    val suppliedName = dockerConfiguration.containerName
    if (suppliedName.contains("/")) suppliedName else s"/$suppliedName"
  }

  override def start: IO[Unit] = {
    for {
      _ <- stop
      _ <- doStart()
      _ <- waitForContainer
    } yield {
      {}
    }
  }

  private def doStart(): IO[Unit] = IO {
    val command = Seq(
      "docker",
      "run",
      "-d",
      "--name",
      containerName,
      "-h",
      containerName,
      "--shm-size",
      dockerConfiguration.shmSize
    ) ++
      options("-e", dockerConfiguration.environment) ++
      options("-p", dockerConfiguration.ports) ++
      options("-v", dockerConfiguration.volumes) ++
      options(
        "--network",
        dockerConfiguration.networks
      ) :+ dockerConfiguration.image
    logger.info(s"Starting container $containerName")
    logger.info(command.mkString(" "))
    command.!
  }

  private class DockerError(message: String) extends Exception(message)

  private def waitForContainer: IO[Unit] = {
    sealed trait ContainerStatus
    case object Starting extends ContainerStatus
    case object Ready extends ContainerStatus
    case class Errored(error: String) extends ContainerStatus

    case class StatusJson(status: Option[String], error: Option[String])
    implicit val containerStatusDecoder: Decoder[ContainerStatus] = {
      val statusJsonDecoder =
        Decoder.forProduct2[StatusJson, Option[String], Option[String]](
          "Status",
          "Error"
        )(StatusJson.apply)
      statusJsonDecoder.map { statusJson =>
        statusJson.error match {
          case Some(error) if error.nonEmpty => Errored(error)
          case _ =>
            if (statusJson.status.contains("running")) Ready else Starting
        }
      }
    }
    val statusTask: IO[Unit] = IO {
      val containerStatus = Seq(
        "docker",
        "inspect",
        "--format='{{json .State}}'",
        containerName
      ).lazyLines_!.headOption match {
        case Some(line) =>
          parser.parse(line).flatMap(_.as[ContainerStatus]).getOrElse(Starting)
        case _ => Starting
      }
      containerStatus match {
        case Errored(error) =>
          throw new DockerError(s"Could not start the docker container: $error")
        case status =>
          logger.info(s"The container has status $status")
      }
    }
    Retry.retry(
      20,
      1.second,
      statusTask,
      { case _: DockerError =>
        Retry.Failed
      }
    ) >> IO {
      logger.info(s"Container $containerName has started successfully")
    }
  }

  private def options(suffix: String, options: Seq[String]): Seq[String] = {
    options.flatMap(option => Seq(suffix, option))
  }

  override def stop: IO[Unit] = {
    IO { Seq("docker", "stop", fullyQualifiedContainerName).! } >>
      IO { Seq("docker", "rm", "--volumes", fullyQualifiedContainerName).! } >>
      IO { logger.info(s"Successfully stopped $fullyQualifiedContainerName") }
  }
}
