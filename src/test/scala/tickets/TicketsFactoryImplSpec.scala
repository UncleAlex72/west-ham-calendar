package tickets

import db.PersistedTicket
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import uk.co.unclealex.days.DSL

import java.time.{Instant, ZoneId}
import scala.language.postfixOps
import uk.co.unclealex.days.DSL.*

class TicketsFactoryImplSpec extends AnyWordSpec with DSL with Matchers {

  given zoneId: ZoneId = ZoneId.of("Europe/London")

  val ticketsFactory: TicketsFactory = new TicketsFactoryImpl

  val tickets: Seq[PersistedTicket] = Range(2, 10, 2).map { idx =>
    PersistedTicket(idx, August(11 - idx, 2018) at 9.am)
  }

  "Finding ticket selling dates when there are none" should {
    "return nothing" in {
      ticketsFactory.saleDate(Seq.empty[PersistedTicket], 6) should ===(None)
    }
  }

  def saleDate(points: Int): Option[Instant] = {
    ticketsFactory.saleDate(tickets, points)
  }

  "Finding tickets for when a person does not have enough points" should {
    "return nothing" in {
      saleDate(1) should ===(None)
    }
  }

  "Finding tickets for when a person has more points that is needed for any sale date" should {
    "return the earliest date" in {
      saleDate(19) should contain((August(3, 2018) at 9.am).toInstant)
    }
  }

  "Finding tickets for 6 points" should {
    "return the date for 6 points" in {
      saleDate(6) should contain((August(5, 2018) at 9.am).toInstant)
    }
  }

  "Finding tickets for 5 points" should {
    "return the date for 4 points" in {
      saleDate(5) should contain((August(7, 2018) at 9.am).toInstant)
    }
  }

  "Finding tickets for 4 points" should {
    "return the date for 4 points" in {
      saleDate(4) should contain((August(7, 2018) at 9.am).toInstant)
    }
  }
}
