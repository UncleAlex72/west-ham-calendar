/** Copyright 2013 Alex Jones
  *
  * Licensed to the Apache Software Foundation (ASF) under one or more
  * contributor license agreements. See the NOTICE file distributed with work
  * for additional information regarding copyright ownership. The ASF licenses
  * file to you under the Apache License, Version 2.0 (the "License"); you may
  * not use file except in compliance with the License. You may obtain a copy of
  * the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations
  * under the License.
  */
package calendar.ical

import java.net.URI
import java.time.{Clock, Duration, ZoneId}
import db.Competition.*
import db.Location.{AWAY, HOME}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import uk.co.unclealex.days.DSL
import uk.co.unclealex.days.DSL.*

import scala.collection.SortedSet

/** @author
  *   alex
  */
class IcalCalendarWriterSpec extends AnyWordSpec with DSL with Matchers {

  given zoneId: ZoneId = ZoneId.of("Europe/London")

  "Tottingham at home" should {
    "be outputted correctly" in {
      val tottinghamHome = new Event(
        id = "1",
        gameId = "1",
        competition = PREM,
        location = HOME,
        geoLocation = Some("London Stadium"),
        opponents = "Tottingham",
        zonedDateTime = September(5, 2013) at 3.pm,
        duration = 2.hours,
        result = None,
        attendance = None,
        busy = true,
        dateCreated = September(4, 2013) at 10.am,
        lastUpdated = September(5, 2013) at 11.am
      )
      print(tottinghamHome) should ===(
        expectedTottinghamHome.replace("\n", "\r\n").trim
      )
    }
  }

  "Southampton away" should {
    "be outputted correctly" in {
      val southamptonAway = new Event(
        id = "2",
        gameId = "2",
        competition = FACP,
        location = AWAY,
        geoLocation = Some("St Mary's Stadium"),
        opponents = "Southampton",
        zonedDateTime = October(5, 2013) at 3.pm,
        duration = 2.hours,
        result = None,
        attendance = None,
        busy = false,
        dateCreated = October(4, 2013) at 10.am,
        lastUpdated = October(5, 2013) at 11.am
      )
      print(southamptonAway) should ===(
        expectedSouthamptonAway.replace("\n", "\r\n").trim
      )
    }
  }

  "Liverpool away" should {
    "be outputted correctly" in {
      val liverpoolAway = new Event(
        id = "2",
        gameId = "2",
        competition = PREM,
        location = AWAY,
        geoLocation = Some("Anfield"),
        opponents = "Liverpool",
        zonedDateTime = October(5, 2013) at 3.pm,
        duration = 1.hour,
        result = Some("0-3"),
        attendance = Some(25000),
        busy = false,
        dateCreated = October(4, 2013) at 10.am,
        lastUpdated = October(5, 2013) at 11.am
      )
      print(liverpoolAway) should ===(
        expectedLiverpoolAway.replace("\n", "\r\n").trim
      )
    }
  }

  def print(event: Event): String = {
    val clock: Clock = Clock.fixed(June(11, 2013) at 19._03, zoneId)
    val icalCalendarWriter = new IcalCalendarWriter(
      clock,
      (venue: String) => new URI(s"search $venue".replace(" ", "+"))
    )
    icalCalendarWriter
      .write(Calendar("id", "title", SortedSet(event).toSeq))
      .trim
  }

  def expectedTottinghamHome = """
BEGIN:VCALENDAR
PRODID:-//unclealex.co.uk//West Ham Calendar 6.0//EN
VERSION:2.0
METHOD:PUBLISH
X-WR-CALNAME:title
X-WR-CALDESC:title
X-WR-TIMEZONE:Europe/London
CALSCALE:GREGORIAN
BEGIN:VEVENT
DTSTART:20130905T140000Z
DTEND:20130905T160000Z
DTSTAMP:20130611T180300Z
UID:1@calendar.unclealex.co.uk
CREATED:20130904T090000Z
DESCRIPTION:
LAST-MODIFIED:20130905T100000Z
LOCATION:London Stadium
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:West Ham vs Tottingham (Premiership)
ATTACH:search+London+Stadium
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR"""

  def expectedSouthamptonAway = """
BEGIN:VCALENDAR
PRODID:-//unclealex.co.uk//West Ham Calendar 6.0//EN
VERSION:2.0
METHOD:PUBLISH
X-WR-CALNAME:title
X-WR-CALDESC:title
X-WR-TIMEZONE:Europe/London
CALSCALE:GREGORIAN
BEGIN:VEVENT
DTSTART:20131005T140000Z
DTEND:20131005T160000Z
DTSTAMP:20130611T180300Z
UID:2@calendar.unclealex.co.uk
CREATED:20131004T090000Z
DESCRIPTION:
LAST-MODIFIED:20131005T100000Z
LOCATION:St Mary's Stadium
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Southampton vs West Ham (FA Cup)
ATTACH:search+St+Mary's+Stadium
TRANSP:TRANSPARENT
END:VEVENT
END:VCALENDAR
"""

  def expectedLiverpoolAway = """
BEGIN:VCALENDAR
PRODID:-//unclealex.co.uk//West Ham Calendar 6.0//EN
VERSION:2.0
METHOD:PUBLISH
X-WR-CALNAME:title
X-WR-CALDESC:title
X-WR-TIMEZONE:Europe/London
CALSCALE:GREGORIAN
BEGIN:VEVENT
DTSTART:20131005T140000Z
DTEND:20131005T150000Z
DTSTAMP:20130611T180300Z
UID:2@calendar.unclealex.co.uk
CREATED:20131004T090000Z
DESCRIPTION:Result: 0-3\nAttendence: 25000
LAST-MODIFIED:20131005T100000Z
LOCATION:Anfield
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Liverpool vs West Ham (Premiership)
ATTACH:search+Anfield
TRANSP:TRANSPARENT
END:VEVENT
END:VCALENDAR
"""

  implicit class IntegerImplicits(hrs: Int) {
    def hour: Duration = Duration.ofHours(hrs)
    def hours: Duration = Duration.ofHours(hrs)
  }
}
