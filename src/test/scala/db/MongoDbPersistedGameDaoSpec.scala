package db

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Sink, Source}
import db.Competition.PREM
import db.Location.HOME
import org.scalatest.matchers.should
import uk.co.unclealex.mongodb.bson.BsonDocument.~>

import java.time.{Clock, ZoneId}
import scala.concurrent.Future
import uk.co.unclealex.days.DSL.*
import uk.co.unclealex.days.{DSL, DateAndTime}
import uk.co.unclealex.mongodb.bson.{BsonDateTime, BsonDocument}
import uk.co.unclealex.mongodb.dao.Database.Database
import uk.co.unclealex.mongodb.test.MongoDbDaoBaseSpec

class MongoDbPersistedGameDaoSpec
    extends MongoDbDaoBaseSpec[PersistedGame, MongoDbPersistedGameDao]("game")
    with should.Matchers
    with DSL {

  given zoneId: ZoneId = ZoneId.of("Europe/London")
  given dateAndTimeToMillis: Conversion[DateAndTime, Long] = dt =>
    dt.toInstant.toEpochMilli

  override def createDao(
      eventualDb: Future[Database],
      clock: Clock
  ): MongoDbPersistedGameDao = {
    new MongoDbPersistedGameDao(eventualDb, clock)
  }

  "The persisted game dao " should {
    "find games by game key" in { f =>
      val gameKey = GameKey(
        competition = PREM,
        location = HOME,
        opponents = "Chelsea",
        season = 2021
      )
      listOpponents(f.dao.findByBusinessKey(gameKey)).map { opponents =>
        opponents should contain theSameElementsAs Seq("Chelsea")
      }
    }

    "find attended games" in { f =>
      listOpponents(f.dao.findByAttended(true)).map { opponents =>
        opponents should contain theSameElementsAs Seq("Chelsea", "Brentford")
      }
    }

    "find unattended games" in { f =>
      listOpponents(f.dao.findByAttended(false)).map { opponents =>
        opponents should contain theSameElementsAs Seq("Arsenal")
      }
    }

    "find games with tickets" in { f =>
      listOpponents(f.dao.findWithTickets()).map { opponents =>
        opponents should contain theSameElementsAs Seq("Brentford")
      }
    }

    "find a game on a specific day" in { f =>
      listOpponents(f.dao.findByDatePlayed(September(12, 2021).toLocalDate))
        .map { opponents =>
          opponents should contain theSameElementsAs Seq("Arsenal")
        }
    }
  }

  def listOpponents(
      games: Source[PersistedGame, NotUsed]
  ): Future[Seq[String]] = {
    games.map(_.opponents).runWith(Sink.seq[String])
  }

  override def initialData(): Seq[BsonDocument] = Seq(
    BsonDocument(
      "season" ~> 2021,
      "opponents" ~> "Chelsea",
      "location" ~> "HOME",
      "competition" ~> "PREM",
      "attended" ~> true,
      "tickets" ~> Seq.empty[BsonDocument]
    ),
    BsonDocument(
      "season" ~> 2021,
      "opponents" ~> "Arsenal",
      "location" ~> "HOME",
      "competition" ~> "PREM",
      "attended" ~> false,
      "at" -> BsonDateTime(September(12, 2021).at(3.pm)),
      "day" ~> 12,
      "month" ~> 9,
      "year" ~> 2021,
      "tickets" ~> Seq.empty[BsonDocument]
    ),
    BsonDocument(
      "season" ~> 2021,
      "opponents" ~> "Brentford",
      "location" ~> "HOME",
      "competition" ~> "PREM",
      "attended" ~> true,
      "at" ~> September(19, 2021).at(3.pm).toInstant,
      "day" ~> 19,
      "month" ~> 9,
      "year" ~> 2021,
      "tickets" ~> Seq(
        BsonDocument(
          "points" ~> 20,
          "at" ~> August(19, 2021).at(9.am).toInstant
        )
      )
    )
  )
}
