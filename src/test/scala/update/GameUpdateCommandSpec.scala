/** Copyright 2010-2012 Alex Jones
  *
  * Licensed to the Apache Software Foundation (ASF) under one or more
  * contributor license agreements. See the NOTICE file distributed with work
  * for additional information regarding copyright ownership. The ASF licenses
  * file to you under the Apache License, Version 2.0 (the "License"); you may
  * not use file except in compliance with the License. You may obtain a copy of
  * the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  * License for the specific language governing permissions and limitations
  * under the License.
  */

package update

import db.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import uk.co.unclealex.days.DSL
import uk.co.unclealex.mongodb.bson.ID
import update.GameUpdateCommand.*

import java.time.{OffsetDateTime, ZoneId, ZonedDateTime}

/** The Class GameUpdateCommandSpec.
  *
  * @author
  *   alex
  */
class GameUpdateCommandSpec extends AnyWordSpec with DSL with Matchers {

  given zoneId: ZoneId = ZoneId.of("Europe/London")

  val DEFAULT_COMPETITION: Competition = Competition.FACP
  val DEFAULT_LOCATION: Location = Location.HOME
  val DEFAULT_OPPONENTS = "Them"
  val DEFAULT_SEASON = 2012
  val DEFAULT_DATE_PLAYED: ZonedDateTime = September(5, 1972) at 3.pm
  val DEFAULT_DAY: Int = DEFAULT_DATE_PLAYED.getDayOfMonth
  val DEFAULT_MONTH: Int = DEFAULT_DATE_PLAYED.getMonthValue
  val DEFAULT_YEAR: Int = DEFAULT_DATE_PLAYED.getYear
  val DEFAULT_TICKETS_AVAILABLE: ZonedDateTime = September(5, 1973) at 15._30
  val DEFAULT_UPDATE_DATE: ZonedDateTime = September(5, 2013) at 9._12
  val DEFAULT_RESULT: GameResult = GameResult(Score(1, 0))
  val DEFAULT_ATTENDANCE = 100000
  val DEFAULT_MATCH_REPORT = "Good"
  val DEFAULT_ATTENDED = false

  "Updating the date played" should {
    val newDatePlayed = March(12, 1973).at(2.pm).toOffsetDateTime
    testGameUpdateCommand[OffsetDateTime](
      DatePlayedUpdateCommand(March(12, 1973).at(2.pm).toOffsetDateTime),
      DatePlayedUpdateCommand(DEFAULT_DATE_PLAYED.toOffsetDateTime),
      _.copy(
        at = Some(newDatePlayed.toInstant),
        day = Some(12),
        month = Some(3),
        year = Some(1973)
      )
    )
  }

  "Updating the result" should {
    val newResult: GameResult =
      DEFAULT_RESULT.copy(shootoutScore = Some(Score(2, 0)))
    testGameUpdateCommand(
      ResultUpdateCommand(newResult),
      ResultUpdateCommand(DEFAULT_RESULT),
      _.copy(result = Some(newResult))
    )
  }

  "Updating the attendance" should {
    testGameUpdateCommand(
      AttendanceUpdateCommand(DEFAULT_ATTENDANCE + 2),
      AttendanceUpdateCommand(DEFAULT_ATTENDANCE),
      _.copy(attendance = Some(DEFAULT_ATTENDANCE + 2))
    )
  }

  "Updating the ticket sale date for a points value that does not exist" should {
    "add the new selling date" in {
      val game: PersistedGame = createFullyPopulatedGame()
      val gameUpdateCommand = TicketsUpdateCommand(
        (2, DEFAULT_TICKETS_AVAILABLE.plusHours(1).toOffsetDateTime)
      )
      gameUpdateCommand.update(game) should contain(
        game.copy(tickets =
          Seq(
            PersistedTicket(
              2,
              DEFAULT_TICKETS_AVAILABLE.plusHours(1).toInstant
            ),
            PersistedTicket(3, DEFAULT_TICKETS_AVAILABLE.toInstant)
          )
        )
      )
    }
  }

  "Updating the ticket sale date for a points value that does exist" should {
    "add the override selling date if it is different to before" in {
      val game: PersistedGame = createFullyPopulatedGame()
      val gameUpdateCommand = TicketsUpdateCommand(
        (3, DEFAULT_TICKETS_AVAILABLE.plusHours(1).toOffsetDateTime)
      )
      gameUpdateCommand.update(game) should contain(
        game.copy(tickets =
          Seq(
            PersistedTicket(3, DEFAULT_TICKETS_AVAILABLE.plusHours(1).toInstant)
          )
        )
      )
    }
    "do nothing if it is no different from before" in {
      val game: PersistedGame = createFullyPopulatedGame()
      val gameUpdateCommand =
        TicketsUpdateCommand((3, DEFAULT_TICKETS_AVAILABLE.toOffsetDateTime))
      gameUpdateCommand.update(game) shouldBe None
    }
  }

  def testGameUpdateCommand[E](
      changingGameUpdateCommand: GameUpdateCommand,
      noopGameUpdateCommand: GameUpdateCommand,
      expectedChange: PersistedGame => PersistedGame
  ): Unit = {
    "not change for equal values" in {
      val game: PersistedGame = createFullyPopulatedGame()
      noopGameUpdateCommand.update(game) shouldBe None
    }
    "change for different values" in {
      val game: PersistedGame = createFullyPopulatedGame()
      changingGameUpdateCommand.update(game) should contain(
        expectedChange(game)
      )
    }
  }

  /** Creates the fully populated game.
    *
    * @return
    *   the game
    */
  def createFullyPopulatedGame(): PersistedGame = {
    new PersistedGame(
      _id = Some(ID()),
      location = DEFAULT_LOCATION,
      season = DEFAULT_SEASON,
      competition = DEFAULT_COMPETITION,
      opponents = DEFAULT_OPPONENTS,
      at = Some(DEFAULT_DATE_PLAYED.toInstant),
      day = Some(DEFAULT_DAY),
      month = Some(DEFAULT_MONTH),
      year = Some(DEFAULT_YEAR),
      attended = DEFAULT_ATTENDED,
      result = Some(DEFAULT_RESULT),
      attendance = Some(DEFAULT_ATTENDANCE),
      tickets = Seq(PersistedTicket(3, DEFAULT_TICKETS_AVAILABLE.toInstant)),
      lastUpdated = Some(DEFAULT_UPDATE_DATE.toInstant),
      dateCreated = Some(DEFAULT_UPDATE_DATE.toInstant)
    )
  }

}
