package update

import org.apache.pekko.stream.scaladsl.{Sink, Source}
import db.Competition.PREM
import db.Location.HOME
import db.{GameKey, PersistedGame, PersistedGameDao}
import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.mockito.Mockito.{times, verify, when}
import org.scalatest.FutureOutcome
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.FixtureAsyncWordSpec
import org.scalatestplus.mockito.MockitoSugar
import uk.co.unclealex.days.DSL.*
import uk.co.unclealex.days.{DSL, DateAndTime}
import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.test.TestActorSystem
import update.GameUpdateCommand.DatePlayedUpdateCommand

import java.time.{Instant, ZoneOffset}

class FixtureUpdatesProcessorSpec
    extends FixtureAsyncWordSpec
    with Matchers
    with TestActorSystem
    with MockitoSugar
    with DSL {

  given zoneOffset: ZoneOffset = ZoneOffset.UTC

  type FixtureParam =
    (FixtureUpdatesProcessor, PersistedGameDao)

  val TOTTENHAM_GAME_KEY: GameKey = GameKey(
    competition = PREM,
    location = HOME,
    opponents = "Tottenham",
    season = 2021
  )

  val TOTTENHAM_NEW_GAME: PersistedGame = PersistedGame
    .gameKey(TOTTENHAM_GAME_KEY)
    .copy(
      at = Some(October(15, 2021).at(3.pm)),
      day = Some(15),
      month = Some(10),
      year = Some(2021),
      attended = true
    )

  val TOTTENHAM_EXISTING_GAME: PersistedGame =
    TOTTENHAM_NEW_GAME.copy(_id = Some(ID()))

  val TOTTENHAM_UPDATED_GAME: PersistedGame =
    TOTTENHAM_EXISTING_GAME.copy(
      at = Some(October(16, 2021).at(3.pm)),
      day = Some(16),
      month = Some(10),
      year = Some(2021)
    )

  "The fixture updates processor" should {
    "create a new game if it does not exist and notify" in {
      case (
            fixtureUpdatesProcessor,
            persistedGameDao
          ) =>
        when(persistedGameDao.findByBusinessKey(TOTTENHAM_GAME_KEY))
          .thenReturn(Source.empty)
        when(persistedGameDao.store(TOTTENHAM_NEW_GAME))
          .thenReturn(Source.single(TOTTENHAM_EXISTING_GAME))
        val source: Source[(GameKey, Set[GameUpdateCommand]), NotUsed] = Source
          .single(
            TOTTENHAM_GAME_KEY -> Set(
              DatePlayedUpdateCommand(October(15, 2021).at(3.pm))
            )
          )
        source
          .via(fixtureUpdatesProcessor.update())
          .runWith(Sink.seq)
          .map { notableUpdates =>
            verify(persistedGameDao).findByBusinessKey(TOTTENHAM_GAME_KEY)
            verify(persistedGameDao).store(TOTTENHAM_NEW_GAME)
            notableUpdates should contain theSameElementsAs Set(
              NewGameNotableUpdate(TOTTENHAM_EXISTING_GAME)
            )
          }
    }
    "update an existing game and notify if the date changes" in {
      case (
            fixtureUpdatesProcessor,
            persistedGameDao
          ) =>
        when(persistedGameDao.findByBusinessKey(TOTTENHAM_GAME_KEY))
          .thenReturn(Source.single(TOTTENHAM_EXISTING_GAME))
        when(persistedGameDao.store(TOTTENHAM_UPDATED_GAME))
          .thenReturn(Source.single(TOTTENHAM_UPDATED_GAME))
        val source: Source[(GameKey, Set[GameUpdateCommand]), NotUsed] =
          Source.single(
            TOTTENHAM_GAME_KEY -> Set(
              DatePlayedUpdateCommand(October(16, 2021).at(3.pm))
            )
          )
        source
          .via(fixtureUpdatesProcessor.update())
          .runWith(Sink.seq)
          .map { notableUpdates =>
            verify(persistedGameDao).findByBusinessKey(TOTTENHAM_GAME_KEY)
            verify(persistedGameDao).store(TOTTENHAM_UPDATED_GAME)
            notableUpdates should contain theSameElementsAs Set(
              DateChangedNotableUpdate(
                TOTTENHAM_UPDATED_GAME,
                October(15, 2021).at(3.pm)
              )
            )
          }
    }
  }

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    val persistedGameDao: PersistedGameDao = mock[PersistedGameDao]
    val fixtureUpdatesProcessor: FixtureUpdatesProcessorImpl =
      new FixtureUpdatesProcessorImpl(
        persistedGameDao = persistedGameDao
      )

    withFixture(
      test.toNoArgAsyncTest(
        fixtureUpdatesProcessor,
        persistedGameDao
      )
    )
  }

}
