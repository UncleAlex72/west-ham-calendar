package update

import db.Competition.PREM
import db.Location.HOME
import db.{GameKey, PersistedGame, PersistedGameDao, PersistedTicket}
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Sink, Source}
import org.mockito.Mockito.{verify, verifyNoMoreInteractions, when}
import org.scalatest.FutureOutcome
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.FixtureAsyncWordSpec
import org.scalatestplus.mockito.MockitoSugar
import uk.co.unclealex.days.DSL
import uk.co.unclealex.days.DSL.*
import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.test.TestActorSystem
import update.GameUpdateCommand.TicketsUpdateCommand

import java.time.{Instant, LocalDate, ZoneId, ZoneOffset}

class TicketUpdatesProcessorSpec
    extends FixtureAsyncWordSpec
    with Matchers
    with DSL
    with TestActorSystem
    with MockitoSugar {

  given ZoneId = ZoneId.of("Europe/London")

  type FixtureParam =
    (TicketUpdatesProcessor, PersistedGameDao)

  val TOTTENHAM_GAME_KEY: GameKey = GameKey(
    competition = PREM,
    location = HOME,
    opponents = "Tottenham",
    season = 2021
  )

  val TOTTENHAM_NEW_GAME: PersistedGame = PersistedGame
    .gameKey(TOTTENHAM_GAME_KEY)
    .copy(
      at = Some(October(15, 2021).at(3.pm)),
      day = Some(15),
      month = Some(10),
      year = Some(2021)
    )

  val TOTTENHAM_EXISTING_GAME: PersistedGame =
    TOTTENHAM_NEW_GAME.copy(_id = Some(ID()))

  val TOTTENHAM_UPDATED_GAME: PersistedGame =
    TOTTENHAM_EXISTING_GAME.copy(
      at = Some(October(16, 2021).at(3.pm)),
      day = Some(16),
      month = Some(10),
      year = Some(2021)
    )

  val TOTTENHAM_TICKETS_GAME: PersistedGame =
    TOTTENHAM_EXISTING_GAME.copy(tickets =
      Seq(PersistedTicket(10, October(9, 2021).at(3.pm)))
    )

  "The ticket updates processor" should {
    "not create a new game if it does not exist" in {
      case (
            ticketUpdatesProcessor,
            persistedGameDao
          ) =>
        when(persistedGameDao.findByDatePlayed(October(10, 2021).toLocalDate))
          .thenReturn(Source.empty)
        val source: Source[(LocalDate, Set[GameUpdateCommand]), NotUsed] =
          Source.single(
            October(10, 2021).toLocalDate -> Set(
              TicketsUpdateCommand(10 -> October(9, 2021).at(3.pm))
            )
          )
        source.via(ticketUpdatesProcessor.update()).runWith(Sink.seq).map {
          notableUpdates =>
            verify(persistedGameDao)
              .findByDatePlayed(October(10, 2021).toLocalDate)
            verifyNoMoreInteractions(persistedGameDao)
            notableUpdates shouldBe empty
        }
    }
    "update an existing game and notify if the date changes" in {
      case (
            ticketUpdatesProcessor,
            persistedGameDao
          ) =>
        when(persistedGameDao.findByDatePlayed(October(15, 2021).toLocalDate))
          .thenReturn(Source.single(TOTTENHAM_EXISTING_GAME))
        when(persistedGameDao.store(TOTTENHAM_TICKETS_GAME))
          .thenReturn(Source.single(TOTTENHAM_TICKETS_GAME))
        val source: Source[(LocalDate, Set[GameUpdateCommand]), NotUsed] =
          Source.single(
            October(15, 2021).toLocalDate -> Set(
              TicketsUpdateCommand(10 -> October(9, 2021).at(3.pm))
            )
          )
        source.via(ticketUpdatesProcessor.update()).runWith(Sink.seq).map {
          notableUpdates =>
            verify(persistedGameDao)
              .findByDatePlayed(October(15, 2021).toLocalDate)
            verify(persistedGameDao).store(TOTTENHAM_TICKETS_GAME)
            notableUpdates should contain theSameElementsAs Set(
              TicketsOnSaleNotableUpdate(TOTTENHAM_TICKETS_GAME)
            )
        }
    }
  }

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    val persistedGameDao: PersistedGameDao = mock[PersistedGameDao]
    val ticketUpdatesProcessor: TicketUpdatesProcessor =
      new TicketUpdatesProcessorImpl(
        persistedGameDao = persistedGameDao
      )

    withFixture(
      test.toNoArgAsyncTest(
        ticketUpdatesProcessor,
        persistedGameDao
      )
    )
  }

}
