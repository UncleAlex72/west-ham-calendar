package update

import org.apache.pekko.stream.scaladsl.Sink
import client.HttpClientPageLoader
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import org.apache.pekko.actor.ActorSystem
import org.scalatest.FutureOutcome
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.FixtureAsyncWordSpec
import uk.co.unclealex.days.DSL
import uk.co.unclealex.days.DateAndTime
import uk.co.unclealex.mongodb.test.TestActorSystem
import update.GameUpdateCommand.TicketsUpdateCommand

import java.net.URI
import java.nio.file.Paths
import java.time.{LocalDate, ZoneOffset}

class HtmlTicketUpdatesSupplierSpec
    extends FixtureAsyncWordSpec
    with DSL
    with TestActorSystem
    with Matchers {

  "The HTML tickets updates supplier" should {
    "find all the ticket updates on the website" in {
      htmlTicketUpdatesSupplier =>
        htmlTicketUpdatesSupplier
          .ticketUpdates()
          .runWith(Sink.seq[(LocalDate, Set[GameUpdateCommand])])
          .map { localDatesAndUpdates =>
            val updatesByDate = localDatesAndUpdates
              .groupMap(_._1)(_._2)
              .view
              .mapValues(_.flatten)
            def update(points: Int, dateAndTime: DateAndTime)
                : GameUpdateCommand = {
              implicit val zoneOffset: ZoneOffset = ZoneOffset.ofHours(1)
              TicketsUpdateCommand(points -> dateAndTime.toOffsetDateTime)
            }
            val BRENTFORD_AWAY_DATE: LocalDate = April(10, 2022)
            val CHELSEA_AWAY_DATE: LocalDate = April(24, 2022)
            updatesByDate.keys should contain theSameElementsAs Seq(
              BRENTFORD_AWAY_DATE,
              CHELSEA_AWAY_DATE
            )
            updatesByDate(BRENTFORD_AWAY_DATE) shouldBe empty
            updatesByDate(
              CHELSEA_AWAY_DATE
            ) should contain theSameElementsAs Seq(
              update(42, March(31, 2022).at(11.am)),
              update(43, March(31, 2022).at(9.am)),
              update(44, March(30, 2022).at(5.pm)),
              update(45, March(30, 2022).at(3.pm)),
              update(46, March(30, 2022).at(1.pm)),
              update(47, March(30, 2022).at(11.am)),
              update(48, March(30, 2022).at(9.am)),
              update(49, March(29, 2022).at(5.pm)),
              update(50, March(29, 2022).at(3.pm)),
              update(51, March(29, 2022).at(1.pm)),
              update(52, March(29, 2022).at(11.am)),
              update(53, March(29, 2022).at(9.am))
            )
          }

    }
  }

  type FixtureParam = HtmlTicketUpdatesSupplier

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    val markerFile =
      Paths.get(
        classOf[HtmlTicketUpdatesSupplierSpec].getClassLoader
          .getResource("wiremock.txt")
          .toURI
      )
    val rootDir = markerFile.getParent.toString
    val wireMockServer = new WireMockServer(
      options().withRootDirectory(rootDir)
    )
    wireMockServer.stubFor(
      get(urlEqualTo("/tickets/away-matches")).willReturn(
        aResponse().withBodyFile("tickets/away-matches.html")
      )
    )
    wireMockServer.stubFor(
      get(urlEqualTo("/tickets/away-matches/brentford-v-west-ham-united"))
        .willReturn(
          aResponse().withBodyFile("tickets/brentford.html")
        )
    )
    wireMockServer.stubFor(
      get(urlEqualTo("/chelsea-v-west-ham-united")).willReturn(
        aResponse().withBodyFile("tickets/chelsea.html")
      )
    )
    wireMockServer.start()
    val port = wireMockServer.port()
    val uri = new URI(s"http://localhost:$port")
    val pageLoader = new HttpClientPageLoader
    val htmlTicketUpdatesSupplier =
      new HtmlTicketUpdatesSupplier(pageLoader = pageLoader, ticketsUri = uri)
    withFixture(test.toNoArgAsyncTest(htmlTicketUpdatesSupplier))
      .onCompletedThen { _ =>
        wireMockServer.shutdown()
      }
  }

}
