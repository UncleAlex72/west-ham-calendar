package update
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import cats.effect.IO
import io.circe.parser

import scala.concurrent.Future
import scala.io.{Source => IoSource}

class ResourceBrowser(val resourceName: String)(implicit
    actorSystem: ActorSystem
) extends Browser {
  override def browse(maybeSeason: Option[Int]): IO[Seq[Match]] = {
    IO.fromFuture {
      IO {

        val url =
          classOf[ResourceBrowser].getClassLoader.getResource(resourceName)
        Source
          .single({})
          .mapWithResource(() => IoSource.fromURL(url))(
            { case (source, _) =>
              for {
                json <- parser
                  .parse(
                    source
                      .getLines()
                      .mkString("\n")
                  )
                  .toTry
                matches <- json.as[Seq[Matches]].toTry
              } yield {
                matches
              }
            },
            source => {
              source.close()
              None
            }
          )
          .mapConcat(_.get)
          .mapConcat(_.`match`)
          .runFold(Seq.empty[Match])(_ :+ _)
      }
    }
  }
}
