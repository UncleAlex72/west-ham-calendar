package update

import org.apache.pekko.stream.scaladsl.Sink
import db.Competition.{EUROPA_GROUP, FACP, PREM}
import db.Location.{AWAY, HOME}
import db.{GameKey, GameResult, Score}
import org.apache.pekko.actor.ActorSystem
import org.scalactic.Equality
import org.scalatest.FutureOutcome
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.FixtureAsyncWordSpec
import uk.co.unclealex.days.{DSL, DateAndTime}
import uk.co.unclealex.mongodb.test.TestActorSystem
import update.GameUpdateCommand.{
  AttendanceUpdateCommand,
  DatePlayedUpdateCommand,
  ResultUpdateCommand,
  VenueUpdateCommand
}

import java.time.{OffsetDateTime, ZoneId, ZoneOffset}

class BrowserFixtureUpdatesSupplierSpec
    extends FixtureAsyncWordSpec
    with Matchers
    with TestActorSystem
    with DSL {

  given zoneId: ZoneId = ZoneId.of("Europe/London")

  given Equality[OffsetDateTime] with
    override def areEqual(a: OffsetDateTime, b: Any): Boolean =
      b.isInstanceOf[OffsetDateTime] && a.toInstant == b
        .asInstanceOf[OffsetDateTime]
        .toInstant

  "The browser fixture updates supplier" should {
    "find all the fixture updates on the website" in {
      browserFixtureUpdatesSupplier =>
        browserFixtureUpdatesSupplier
          .fixtureUpdates(None)
          .runWith(Sink.seq[(GameKey, Set[GameUpdateCommand])])
          .map { gameKeysAndUpdates =>
            val updatesByGameKey = gameKeysAndUpdates
              .groupMap(_._1)(_._2)
              .view
              .mapValues(_.flatten)
            val CITEH_HOME = GameKey(PREM, HOME, "Manchester City", 2023)
            val TSC_HOME = GameKey(EUROPA_GROUP, HOME, "TSC", 2023)
            val LIVERPOOL_AWAY = GameKey(PREM, AWAY, "Liverpool", 2023)
            val VILLA_AWAY = GameKey(PREM, AWAY, "Aston Villa", 2020)
            val FULHAM_AWAY = GameKey(PREM, AWAY, "Fulham", 2020)
            val MANURE_AWAY = GameKey(FACP, AWAY, "Manchester United", 2020)
            val BLADES_HOME = GameKey(PREM, HOME, "Sheffield United", 2020)
            val SPUDS_HOME = GameKey(PREM, HOME, "Tottenham Hotspur", 2020)
            val CITEH_AWAY =
              GameKey(PREM, AWAY, "Manchester City", 2020)

            updatesByGameKey.keys.toSeq should contain theSameElementsAs Seq(
              CITEH_HOME,
              TSC_HOME,
              LIVERPOOL_AWAY,
              VILLA_AWAY,
              FULHAM_AWAY,
              MANURE_AWAY,
              BLADES_HOME,
              SPUDS_HOME,
              CITEH_AWAY
            )
            updatesByGameKey(CITEH_HOME) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(September(16, 2023).at(3.pm)),
              VenueUpdateCommand("London Stadium"),
              ResultUpdateCommand(GameResult(Score(1, 3), None)),
              AttendanceUpdateCommand(62475)
            )

            updatesByGameKey(TSC_HOME) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(September(21, 2023).at(8.pm)),
              VenueUpdateCommand("London Stadium")
            )

            updatesByGameKey(
              LIVERPOOL_AWAY
            ) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(September(24, 2023).at(2.pm)),
              VenueUpdateCommand("Anfield")
            )

            updatesByGameKey(VILLA_AWAY) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(February(3, 2021).at(8._15.pm)),
              VenueUpdateCommand("Villa Park"),
              ResultUpdateCommand(GameResult(Score(1, 3), None))
            )

            updatesByGameKey(FULHAM_AWAY) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(February(6, 2021).at(5._30.pm)),
              VenueUpdateCommand("Craven Cottage"),
              ResultUpdateCommand(GameResult(Score(0, 0), None))
            )

            updatesByGameKey(MANURE_AWAY) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(February(9, 2021).at(7._30.pm)),
              VenueUpdateCommand("Old Trafford"),
              ResultUpdateCommand(GameResult(Score(0, 0), None))
            )

            updatesByGameKey(
              BLADES_HOME
            ) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(February(15, 2021).at(6.pm)),
              VenueUpdateCommand("London Stadium"),
              ResultUpdateCommand(GameResult(Score(3, 0), None))
            )

            updatesByGameKey(
              SPUDS_HOME
            ) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(February(21, 2021).at(12.pm)),
              VenueUpdateCommand("London Stadium"),
              ResultUpdateCommand(GameResult(Score(2, 1), None))
            )

            updatesByGameKey(
              CITEH_AWAY
            ) should contain theSameElementsAs Seq(
              DatePlayedUpdateCommand(February(27, 2021).at(12._30.pm)),
              VenueUpdateCommand("Etihad Stadium"),
              ResultUpdateCommand(GameResult(Score(2, 1), None))
            )
          }
    }
  }
  type FixtureParam = BrowserFixtureUpdatesSupplier

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    val browser = new ResourceBrowser("browser.json")
    val browserFixtureUpdatesSupplier = new BrowserFixtureUpdatesSupplier(
      browser = browser
    )

    withFixture(test.toNoArgAsyncTest(browserFixtureUpdatesSupplier))
  }

}
