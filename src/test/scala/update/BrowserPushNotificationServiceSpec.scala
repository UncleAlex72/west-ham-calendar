package update

import org.apache.pekko.stream.scaladsl.{Sink, Source}
import db.{Competition, Location, PersistedGame, PointsDao}
import org.apache.pekko.actor.ActorSystem
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.{verify, when}
import org.scalatest.FutureOutcome
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.FixtureAsyncWordSpec
import org.scalatestplus.mockito.MockitoSugar
import tickets.TicketsFactory
import uk.co.unclealex.days.DSL
import uk.co.unclealex.days.Syntax.*
import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.test.TestActorSystem
import uk.co.unclealex.push.PushSubscription.{Keys, User}
import uk.co.unclealex.push.{BrowserPushService, PushResponse, PushSubscription}

import java.time.{Instant, ZoneId}
import scala.concurrent.Future

class BrowserPushNotificationServiceSpec
    extends FixtureAsyncWordSpec
    with Matchers
    with DSL
    with TestActorSystem
    with MockitoSugar {

  given ZoneId = ZoneId.of("Europe/London")

  "The browser push notification service" should {
    "send new game updates" in { f =>
      val newGameNotableUpdate = NewGameNotableUpdate(
        PersistedGame(
          _id = Some(ID()),
          location = Location.AWAY,
          competition = Competition.PREM,
          opponents = "Chelsea",
          season = 2021,
          at = Some(May(14, 2022).at(3.pm))
        )
      )
      val eventualMessages: Future[Seq[String]] = Source
        .single(newGameNotableUpdate)
        .via(f.browserPushNotificationService.sendNotifications())
        .map(_._2)
        .runWith(Sink.seq)
      eventualMessages.map { messages =>
        messages should have size 1
        val message = messages.head
        val expectedMessage =
          """{
            |"notificationType":"new",
            |"competition":"Premiership",
            |"location":"AWAY",
            |"opponents":"Chelsea",
            |"season":2021,
            |"when":"2022-05-14T14:00:00Z"
            |}""".stripMargin.replace("\n", "")
        verify(f.browserPushService).notifyAll(expectedMessage)
        message shouldBe expectedMessage
      }
    }
  }

  it should {
    "send date change updates" in { f =>
      val dateChangedNotableUpdate = DateChangedNotableUpdate(
        game = PersistedGame(
          _id = Some(ID()),
          location = Location.AWAY,
          competition = Competition.PREM,
          opponents = "Chelsea",
          season = 2021,
          at = Some(May(14, 2022).at(3.pm))
        ),
        previousDatePlayed = May(7, 2022).at(3.pm)
      )
      val eventualMessages: Future[Seq[String]] = Source
        .single(dateChangedNotableUpdate)
        .via(f.browserPushNotificationService.sendNotifications())
        .map(_._2)
        .runWith(Sink.seq)
      eventualMessages.map { messages =>
        messages should have size 1
        val message = messages.head
        val expectedMessage =
          """{
            |"notificationType":"change",
            |"competition":"Premiership",
            |"location":"AWAY",
            |"opponents":"Chelsea",
            |"season":2021,
            |"previousDateTimePlayed":"2022-05-07T14:00:00Z",
            |"newDateTimePlayed":"2022-05-14T14:00:00Z"
            |}""".stripMargin.replace("\n", "")
        verify(f.browserPushService).notifyAll(expectedMessage)
        message shouldBe expectedMessage
      }
    }
  }

  it should {
    "send ticket sale updates" in { f =>
      val ticketsOnSaleNotableUpdate = TicketsOnSaleNotableUpdate(
        game = PersistedGame(
          _id = Some(ID()),
          location = Location.AWAY,
          competition = Competition.PREM,
          opponents = "Chelsea",
          season = 2021,
          at = Some(May(14, 2022).at(3.pm).toInstant)
        )
      )
      when(f.pointsDao.points()).thenReturn(Source.single(5))
      when(f.ticketsFactory.saleDate(Seq.empty, 5))
        .thenReturn(Some(March(12, 2022).at(9.am).toInstant))
      val eventualMessages: Future[Seq[String]] = Source
        .single(ticketsOnSaleNotableUpdate)
        .via(f.browserPushNotificationService.sendNotifications())
        .map(_._2)
        .runWith(Sink.seq)
      eventualMessages.map { messages =>
        messages should have size 1
        val message = messages.head
        val expectedMessage =
          """{
            |"notificationType":"tickets",
            |"competition":"Premiership",
            |"location":"AWAY",
            |"opponents":"Chelsea",
            |"season":2021,
            |"dateTimePlayed":"2022-05-14T14:00:00Z",
            |"ticketsAvailableAt":"2022-03-12T09:00:00Z"
            |}""".stripMargin
            .replace("\n", "")
        verify(f.browserPushService).notifyAll(expectedMessage)
        message shouldBe expectedMessage
      }
    }
  }

  type FixtureParam = Services

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {
    val browserPushService = mock[BrowserPushService]
    when(browserPushService.notifyAll(any[String])).thenReturn(
      Source.single(
        PushResponse.Success(
          PushSubscription(
            _id = None,
            endpoint = "",
            keys = Keys("", ""),
            user = User(None, None),
            lastUpdated = None,
            dateCreated = None
          )
        )
      )
    )
    val ticketsFactory = mock[TicketsFactory]
    val pointsDao = mock[PointsDao]
    val services = Services(
      browserPushNotificationService = new BrowserPushNotificationService(
        browserPushService = browserPushService,
        ticketsFactory = ticketsFactory,
        pointsDao = pointsDao
      ),
      browserPushService = browserPushService,
      pointsDao = pointsDao,
      ticketsFactory = ticketsFactory
    )
    test.toNoArgAsyncTest(services)()
  }

  case class Services(
      browserPushNotificationService: BrowserPushNotificationService,
      browserPushService: BrowserPushService,
      pointsDao: PointsDao,
      ticketsFactory: TicketsFactory
  )

}
