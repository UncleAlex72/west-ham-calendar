import com.typesafe.sbt.packager.docker.Cmd
import sbtrelease.ReleaseStateTransformations._
import scala.sys.process._

val _scalaVersion = "3.6.2"
val mongoDbVersion = "5.0.1"

name := "west-ham-calendar"
scalaVersion := _scalaVersion
resolvers += "Atlassian public repository" at "https://packages.atlassian.com/maven-public/"
libraryDependencies ++= Seq(
  "com.google.api-client" % "google-api-client" % "2.7.2",
  "com.google.oauth-client" % "google-oauth-client-jetty" % "1.37.0",
  "com.google.auth" % "google-auth-library-oauth2-http" % "1.31.0",
  "com.google.apis" % "google-api-services-calendar" % "v3-rev20250115-2.0.0",
  "uk.co.unclealex" %% "mongodb-scala" % mongoDbVersion,
  "uk.co.unclealex" %% "browser-push-notifications" % "4.0.1",
  "uk.co.unclealex" %% "string-like" % "2.0.4",
  "uk.co.unclealex" %% "futures" % "2.0.0",
  "com.beachape" %% "enumeratum" % "1.7.5",
  "ch.qos.logback" % "logback-classic" % "1.5.16",
  "uk.co.unclealex" %% "pekko-oidc-google" % "5.1.0",
  "org.mnode.ical4j" % "ical4j" % "3.2.1",
  "commons-net" % "commons-net" % "3.11.1",
  "org.jsoup" % "jsoup" % "1.18.3",
  "org.apache.xmlgraphics" % "batik-transcoder" % "1.18",
  "org.apache.xmlgraphics" % "batik-codec" % "1.18",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
  "org.apache.shiro" % "shiro-core" % "2.0.2",
  "org.seleniumhq.selenium" % "selenium-remote-driver" % "4.12.1",
  "org.seleniumhq.selenium" % "selenium-devtools-v116" % "4.12.1",
  "org.seleniumhq.selenium" % "selenium-chrome-driver" % "4.12.1",
  "org.scala-lang.modules" %% "scala-collection-contrib" % "0.4.0",
  "commons-io" % "commons-io" % "2.18.0"
) ++ Seq(
  "org.scalatestplus" %% "mockito-4-2" % "3.2.11.0",
  "uk.co.unclealex" %% "mongodb-scala-test" % mongoDbVersion,
  "uk.co.unclealex" %% "thank-you-for-the-days" % "2.0.0",
  "com.github.tomakehurst" % "wiremock-jre8" % "2.33.0",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.13.0",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.0"
).map(_ % "test")
dockerBaseImage := "adoptopenjdk/openjdk11-openj9:latest"
dockerExposedPorts := Seq(9000)
maintainer := "Alex Jones <alex.jones@unclealex.co.uk>"
dockerRepository := Some("unclealex72")
packageName := "west-ham-calendar"
dockerUpdateLatest := true
// Installing packages requires the run commands to be put high up in the list of docker commands
dockerCommands := {
  val apkAddCommand: Seq[String] = Seq(
  )
  val allCommands: Seq[Seq[String]] = Seq(
    Seq("groupadd", "-r", "-g", "998", "docker"),
    Seq("usermod", "demiourgos728", "-a", "-G", "docker"),
    Seq("apt", "update"),
    Seq("apt", "install", "-y", "curl", "docker.io")
  )
  val run: Seq[String] = allCommands.flatMap(cmd => "&&" +: cmd).drop(1)
  Docker.removeUserGroup(
    Docker.insertCommands(
      dockerCommands.value,
      Cmd("RUN", run: _*)
    )
  )
}
Universal / javaOptions ++= Seq(
  "pidfile.path" -> "/dev/null",
  "java.awt.headless" -> "true"
).map { case (k, v) =>
  s"-D$k=$v"
}

enablePlugins(DockerPlugin, JavaAppPackaging)

Compile / sourceGenerators += Def.task {
  val file = (Compile / sourceManaged).value / "version" / "Version.scala"
  IO.write(file,
    s"""
       |package version
       |
       |object Version:
       |  val version: String = "${version.value}"
       |""".stripMargin.trim)
  Seq(file)
}.taskValue

// Releases

lazy val deploy = taskKey[Unit]("Execute the shell script")

deploy := {
  Seq("ssh", "cole", "cd git/docker-environment; ./update-stack west-ham-calendar").!
}

releaseProcess := Seq[ReleaseStep](
  releaseStepCommand("scalafmtCheckAll"),
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand(
    "stage"
  ), // : ReleaseStep, build server docker image.
  releaseStepCommand(
    "docker:publish"
  ), // : ReleaseStep, build server docker image.
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges, // : ReleaseStep, also checks that an upstream branch is properly configured
  releaseStepCommand("deploy")
)
Global / onChangedBuildSource := ReloadOnSourceChanges
