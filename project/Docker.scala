import com.typesafe.sbt.packager.docker.{Cmd, CmdLike}

object Docker {

  def insertCommands(
      dockerCommands: Seq[CmdLike],
      extraCommands: CmdLike*
  ): Seq[CmdLike] = {
    val (prefixCommands, suffixCommands) = dockerCommands.splitAt(
      1 +
        dockerCommands.indexWhere { cmd =>
          cmd.makeContent.startsWith("RUN id -u demiourgos728")
        }
    )
    prefixCommands ++ extraCommands ++ suffixCommands
  }

  def removeUserGroup(dockerCommands: Seq[CmdLike]): Seq[CmdLike] = {
    dockerCommands.map {
      case Cmd(cmd, args @ _*) if cmd == "USER" =>
        Cmd("USER", args.map(arg => arg.split(":")(0)): _*)
      case cmdLike: CmdLike => cmdLike
    }
  }

}
